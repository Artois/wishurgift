# Documentation Wishurgift Serveur

Dans ce document se trouve toutes les informations sur le fonctionnement et l'utilisation du serveur de Wishurgift.

## API

L'API du serveur est au format REST et est disponible sur le port 8080. Les parametres dans les URL seront mis entre accolade (exemple : /test/{param}, ici param est un parametre dans l'URL) si ils sont obligatoire, entre crochet si ils sont optionnels. Tous les points d'entrée documenté sont disponibles dans un [serveur de mock](https://gitlab.univ-artois.fr/arthur_brandao/wishurgift-mock-server) pour tester (:warning: Le serveur de mock ne prend pas en compte l'authentification).

Toutes les requetes sont encapsuler dans le json suivant :

```json
{
    "success": true,
    "data": {}
}
```

Dans le cas ou la requete ne retourne pas d'erreur le boolean succes seras à `true` et les données retourné seront dans data. En cas d'erreur ou de probleme success seras à `false` et data contiendra les informations sur l'erreur. Ces informations sont sous forme d'un objet JSON avec un code et un message d'erreur comme dans l'exemple ci-dessous.

```json
{
    "success": false,
    "data": {
        "code": 404,
        "message": "Not found",
        "trace": []
    }
}
```

Le code est lié au message dans erreur (tous les codes sont décrit ci-dessous dans la section erreurs). L'attribut trace est un tableau de String (qui peut être vide) qui contient des informations sur la raison de l'erreur.

Il possible de tester ce retour avec l'URL `/error` sur le serveur de mock (:warning: Cette URL ne fais pas partie de l'API du serveur réel)

Les retour indiqué dans la documentation ci-dessous ne prennent pas compte de l'encapsulation mais **ils seront toujours encapsuler de la façon décrite ci-dessus**. Le serveur de mock tient en compte de l'encapsulation et retournera toutes les réponse encapsuler.

Si il n'y a pas d'indication sur le retour pour un point d'entrée cela signifie que seul l'encapsulation est retourné avec data qui prend la valeur d'un objet vide (comme dans l'exemple de retour sans erreur ci-dessus).

Les informations sur les points d'entrés de l'API sont trouvable dans les documents suivants :
- [Commentaire (`/comments`)](./api/comments.md) 
- [Contact (`/contacts`)](./api/contacts.md)
- [Element (`/elements`)](./api/elements.md)
- [Image (`/images`)](./api/images.md)
- [Statistique (`/stats`)](./api/stats.md)   
- [Utilisateur (`/users`)](./api/users.md) 
- [Liste de souhait (`/wishlists`)](./api/wishlists.md) 

## Authentification

L'authentification est gérée par un JSON Web Token (ou JWT). Le principe est simple, l'utilisateur envoie son login et son mot de passe au serveur qui lui renvoie un token si l'authentification réussit. Ensuite le client envoie dans l'entête HTTP Authorization le token reçu pour prouver son authentification.

Pour plus d'information vous pouvez consulter les sites suivants :

- https://jwt.io
- https://medium.com/dev-bits/a-guide-for-adding-jwt-token-based-authentication-to-your-single-page-nodejs-applications-c403f7cf04f4
- https://github.com/jwtk/jjwt

Chaque token est valide 24h00 après sa génération. Pour être authentifier il faut envoyer la token dans la clef Authorization du header de la requete HTTP sous la forme suivante `Bearer <token>`

Il est possible de générer un nouveau token à partir d'un token existant encore valide, pour prolongé la connexion sans redemander les informations d'authentification. Pour cela référez vous à la documentation de Utilisateur.

## Erreurs

Voici la liste des erreurs et leurs codes pouvant être retournées par l'application. Si le code d'erreur correspond à un code de retour HTTP alors il sera utilisé, sinon c'est la code de retour 400 qui sera retourné.

- 00 : Erreur inconnue
- 01 : Impossible de se connecter
- 02 : Utilisateur déjà existant
- 03 : Impossible de récupèrer les données
- 400 : Requete invalide
- 401 : Non autorisé
- 403 : Interdit
- 404 : Ressource non trouvé

## Base de données

La base de données utilise le SGBD PostgreSQL dans un docker. Ci-dessous se trouve le MCD de la base de données. D'autre documents peuvent être trouvé dans le dossier zip `/db/Mocodo.zip`

Les scripts SQL pour générer la base se trouve dans `/src/main/resources/liquibase/changelog`

![MCD](../db/MCD.svg)

## Configuration et gestion du projet

Pour les développeurs des informations sur la configuration et la gestion du projet sont trouvable dans le [document dev.md](./dev.md)