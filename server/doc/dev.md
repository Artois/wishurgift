# Configuration et gestion du projet

Dans ce document se trouve quelques conseille pour les développeur reprenant le projet. Le projet a été développé avec IntellIj, et il est conseiller d'utiliser cet IDE (les fichiers de configuration de l'IDE sont présent dans le projet).

## Parametrage IntellIJ

Pour faire fonctionner correctement le projet dans IntellIj il faut installer deux plugins (Ctrl+Alt+S -> Plugin -> Marketplace) :

- Save actions : Cocher les deux premieres cases dans `General` et cocher toutes les cases sauf la 3éme dans `Formatting action` (en théorie les cases dont déjà cochées grace à la présence des fichiers de configuration du projet)
- Lombok (pour utiliser lombok dans le projet)

## Fichier de configuration du projet

Voici quelques informations sur les fichiers de configuration du projet.

### Passer les tests sur maven

Pour ne plus éxecuter les tests lors de l'utilisation de maven il suffit de décommenter le dernier plugin dans le fichier `pom.xml`

### Changer les informations de la base de données

Pour changer les informations de la base de données il faut modifier le fichier `/src/main/resources/META-INF/resources.xml`, soit pour ajouter une nouvelle resource, soit pour modifier l'existante. Si une nouvelle resource est crée il faut aussi modifier le fichier `/src/main/resources/META-INF/persistence.xml` et mettre l'id de la ressource dans la balise jta-data-source.

Enfin il faut aussi indiquer à Liquibase les nouvelles informations, pour cela il faut modifier les 3 propriétés commencant par db dans le fichier `pom.xml`.

### Ajouter un script SQL Liquibase

Il suffit d'ajouter un fichier SQL dans le dossier `/src/main/resources/liquibase/changelog/`, il devra commencer par les lignes suivantes :

```text
--liquibase formatted sql
--changeset {user}:{id}
```

{user} doit être remplacé par le nom de l'utilisateur et {id} par une chaine de caractère qui sert d'identifiant unique pour liquibase. Par convention le fichier est nommé {id}-{action}.sql, ou {id} est l'id dans le fichier et {action} le role du fichier sql (créer la table user, ou ajouter des données par exemple).

Ensuite il faut ajouter dans le fichier `/src/main/resources/liquibase/changelog-master.xml` la ligne ci-dessous (en remplaçant {filename} par le nom du fichier SQL) à la suite des autres balises include :

```xml
<include file="changelog/{filename}.sql" relativeToChangelogFile="true"/>
```

Le script seras joué lors du prochain maven package (la base de données doit donc être accessible lors du maven package) si il n'a jamais été joué auparavant. Liquibase verifie à chaque maven package l'état de la base de données.