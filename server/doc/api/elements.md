# Element

Liste de tous les les points d'entrée lié à la gestion des éléments, ils commencent tous par `/elements`.

### GET

- `GET /elements/{id}` : Récupère un élément (l'élément doit être dans une liste accessible à l'utilisateur connecté, sinon une erreur est retorunée)

  - Parametre dans l'URL :

    - `id` : L'id de l'élément

  - Retour :

    ```json
    {
        "id": 48,
        "status": "idea",
        "name": "Chaise",
        "link": "https://vendeur-de-chause.fr",
        "image": [
            {
                "id": 1,
                "ext": "png",
                "data": "data:image/png;base64,imageenbase64"
            },
            {
                "id": 2,
                "ext": "jpeg",
                "data": "data:image/png;base64,imageenbase64"
            }
        ],
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```
    
- `GET /elements/{id}/comments` : Récupère tous les commentaires d'un élément (La liste de l'élement doit être accessible à l'utilisateur connecté, sinon une erreur est retournée)
        
    - Parametre dans l'URL :
            
        - `id` : L'id de l'élément
        
    - Retour :
        
        ```json
        {
            "total": 3,
            "list": [
                {
                    "id": 1,
                    "text": "je suis un commentaire",
                    "date": "21/08/2020",
                    "user": {
                      "id": 8,
                      "name": "John",
                      "tag": "John#12345"
                    }
                },
                {
                    "id": 2,
                    "text": "je suis un commentaire",
                    "date": "24/08/2020",
                    "user": {
                      "id": 8,
                      "name": "John",
                      "tag": "John#12345"
                    }
                },
                {
                    "id": 3,
                    "text": "je suis un commentaire",
                    "date": "02/09/2020",
                    "user": {
                      "id": 8,
                      "name": "John",
                      "tag": "John#12345"
                    }
                }
            ]
        }    
        ```
      
- `GET /elements/{id}/comments/page/{num}/{nb}` : Récupère une liste paginée des commentaires d'un élément (La liste de l'élement doit être accessible à l'utilisateur connecté, sinon une erreur est retournée)
        
    - Parametre dans l'URL :
            
        - `id` : L'id de l'élément
        - `num` : Le numéro de la page
        - `nb` : Le nombre d'élément par page
        
    - Retour :
        
        ```json
        {
            "total": 3,
            "page": 1,
            "size": 2,
            "list": [
                {
                    "id": 1,
                    "text": "je suis un commentaire",
                    "date": "21/08/2020",
                    "user": {
                      "id": 8,
                      "name": "John",
                      "tag": "John#12345"
                    }
                },
                {
                    "id": 2,
                    "text": "je suis un commentaire",
                    "date": "24/08/2020",
                    "user": {
                      "id": 8,
                      "name": "John",
                      "tag": "John#12345"
                    }
                }
            ]
        }
        ```
      
       Le retour ci-dessus correspond à l'URL /elements/{id}/comments/page/1/2
        
### POST

- `POST /elements/{id}/image` : Ajoute une image sur l'élément (la liste de l'élément doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)

  - Parametre dans l'URL :

    - `id` : L'id de l'élément à modifier

  - Parametre dans la requete HTTP :

    - `ext` : L'extension du fichier
    - `data` : L'image encoder en base64

    ```json
    {
        "ext": "png",
        "data": "data:image/png;base64,imageenbase64"
    }
    ```

  - Retour :

    ```json
    {
        "id": 68,
        "ext": "png",
        "data": "data:image/png;base64,imageenbase64"
    }
    ```

- `POST /elements/{id}/comment` : Ajoute un commentaire sur l'élément (la liste doit être accessible à l'utilisateur connecté, sinon une erreur est retourné)

  - Parametre dans l'URL :

    - `id` : L'id de l'élément

  - Parametre dans la requete HTTP :

    - `text` : Le texte du commentaire

    ```json
    {
        "text": "je suis un commentaire"
    }
    ```

  - Retour :

    ```json
    {
        "id": 1,
        "text": "je suis un commentaire",
        "date": "21/08/2020",
        "user": {
          "id": 8,
          "name": "John",
          "tag": "John#12345"
        }
    }
    ```

### PUT

- `PUT /elements/{id}` : Modifie un élément (la liste de l'élément doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)

  - Parametre dans l'URL :

    - `id` : L'id de l'élément à modifier

  - Parametre dans la requete HTTP :

    - `name (optionnel)` : Le nom de l'élément
    - `link (optionnel)` : Lien vers l'élément
    - `address (optionnel)` : L'adresse pour trouver l'element
    - `price (optionnel)` : Prix de l'objet
    - `description (optionnel)` : Description de l'élément

    ```json
    {
        "name": "Chaise",
        "link": "https://vendeur-de-chause.fr",
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```

  - Retour :

    ```json
    {
        "id": 48,
        "status": "purchased",
        "name": "Chaise",
        "link": "https://vendeur-de-chause.fr",
        "image": [
            {
                "id": 1,
                "ext": "png",
                "data": "data:image/png;base64,imageenbase64"
            },
            {
                "id": 2,
                "ext": "jpeg",
                "data": "data:image/png;base64,imageenbase64"
            }
        ],
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```
  
- `PUT /elements/{id}/status/{status}` : Permet de modifier le status d'un élément d'une liste dont l'utilisateur connecté peut accèder. Il n'est possible que d'avancer dans la liste des status (idea,crush -> reserved -> purchased). Si l'utilisateur connecté n'a pas accès à l'élément ou qu'il essaye de faire reculer le status une erreur est retournée.

  - Parametre dans l'URL :

    - id : L'id de l'élément à modifier
    - status : Le nouveau statut (reserved ou purchased)

  - Retour :

    ```json
    {
        "id": 4,
        "status": "reserved",
        "name": "Chaise",
        "link": "https://vendeur-de-chaise.fr",
        "image": [
            {
                "id": 1,
                "ext": "png",
                "data": "data:image/png;base64,imageenbase64"
            },
            {
                "id": 2,
                "ext": "jpeg",
                "data": "data:image/png;base64,imageenbase64"
            }
        ],
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```

### DELETE

- `DELETE /elements/{id}` : Supprime un élément d'une liste (la liste doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)
  - Parametre dans l'URL :
    - `id` : L'id de l'élément
