# Image

Liste de tous les les points d'entrée lié aux images, ils commencent tous par `/images`.

### GET

- `GET /images/{id}` : Récupère les données d'une image (l'image doit appartenir à un élément d'une liste dont l'utilisateur connecté à accès)

  - Parametre dans l'URL :

    - `id` : L'id de l'image

  - Retour :

    ```json
    {
        "id": 1,
        "ext": "png",
        "data": "data:image/png;base64,imageenbase64"
    }
    ```

### DELETE

- `DELETE /images/{id}` : Supprime une image d'un élément (l'image doit être rattachée à un élément d'une liste de l'utiliseur connecté, sinon une erreur est retounée)
  - Parametre dans l'URL :
    - `id` : L'id de l'image