# Utilisateur

Liste de tous les les points d'entrée lié à la gestion d'un utilisateur, ils commencent tous par `/users`.

### GET

- `GET /users/{id}` : Retourne les informations d'un utilisateur

  - Parametre dans l'URL :

    - `id` : L'id de l'utilisateur

  - Retour :

    ```json
    {
        "id": 6,
        "name": "John Doe",
        "email": "john.doe@email.com",
        "avatar": "http://gravatar.url",
        "public": false,
        "tag": "John_Doe#08520"
    }
    ```

    L'utilisateur ne seras retourné que si son profil est publique ou que l'utilisateur connecté et l'utilisateur recherché soit ami ou bien que ce soit le profil de l'utilisateur actuellement connecté. Dans les autres cas une erreur est retournée

- `GET /users/search/{request}/[limit]` : Retourne une liste d'utilisateur correspondant  à la recherche

  - Parametre dans l'URL :

    - `request` : Chaine de caractere pour la recherche. La recherche est effectuée sur le l'email, le nom et la partie id du tag et n'est pas sensible à la casse.
    - `limit (optionnel)` : Nombre maximum de résultat à retourner.

  - Retour :

    ```json
    {
        "total": 3,
        "list": [
            {
                "id": 1,
                "name": "janeDoe",
                "email": "jd@mail.com",
                "avatar": "http://gravatar.url",
                "public": true,
                "tag": "janeDoe#04578"
            },
            {
                "id": 2,
                "name": "John Doe",
                "email": "john.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": true,
                "tag": "John_Doe#08520"
            },
            {
                "id": 3,
                "name": "Doeris",
                "email": "alice@email.com",
                "avatar": "http://gravatar.url",
                "public": true,
                "tag": "Doeris#08520"
            }
        ]
    }
    ```

    Dans les résultat de la recherche seul les profils publique apparaissent.
    
    Dans l'exemple ci-dessus la recherche a été faite sur la chaine "do"

- `GET /users/{id}/wishlists` : Retourne toutes les listes de souhaits d'un utilisateur

  - Parametre dans l'URL : 

    - `id` : L'id de l'utilisateur

  - Retour :

    ```json
    {
        "total": 2,
        "list": [
            {
                "id": 1,
                "name": "Ma liste de noel",
                "public": true,
                "size": {
                    "total": 8,
                    "unbuy": 4
                }
            },
            {
                "id": 2,
                "name": "Amazon",
                "public": false,
              	"size": {
                    "total": 24,
                    "unbuy": 21
                }
            }
    	]
    }
    ```
    
    Les données dans le retour diffère en fonction de l'authentification : si on récupère les listes de l'utilisateur connecté alors toutes les listes sont retournées, si on récupère les listes d'un autre utilisateur seul ces listes publiques et celles qui sont partagées avec l'utilisateur connecté sont retournées, enfin si aucun utilisateur n'est connecté alors seul les listes publiques sont retournées

- `GET /users/{id}/wishlists/page/{num}/{nb}` : Retourne une liste paginée des listes de souhaits d'un utilisateur

  - Parametre dans l'URL :

    - `id` : L'id de l'utilisateur
    - `num` : Le numéro de la page
    - `nb` : Le nombre d'élément par page

  - Retour

    ```json
    {
        "total": 4,
        "page": 1,
        "size": 3,
        "list": [
            {
                "id": 1,
                "name": "Ma liste de noel",
                "public": true,
                "size": {
                    "total": 8,
                    "unbuy": 4
                }
            },
            {
                "id": 2,
                "name": "Amazon",
                "public": false,
                "size": {
                    "total": 24,
                    "unbuy": 21
                }
            },
            {
                "id": 6,
                "name": "Jeux vidéo",
                "public": true,
                "size": {
                    "total": 6,
                    "unbuy": 0
                }
            }
      ]
    }
    ```
    
    Les données dans le retour diffère en fonction de l'authentification : si on récupère les listes de l'utilisateur connecté alors toutes les listes sont retournées, si on récupère les listes d'un autre utilisateur seul ces listes publiques et celles qui sont partagées avec l'utilisateur connecté sont retournées, enfin si aucun utilisateur n'est connecté alors seul les listes publiques sont retournées.
    
    Le retour ci-dessus correspond à l'URL /user/{id}/wishlists/page/1/3
    
- `GET /users/wishlists/shared` : Retourne toutes les listes de souhaits partagées avec l'utilisateur connecté

  - Retour :

    ```json
    {
        "total": 2,
        "list": [
            {
                "id": 1,
                "name": "Ma liste de noel",
                "public": true,
                "size": {
                    "total": 8,
                    "unbuy": 4
                },
                "user": {
                    "id": 1,
                    "name": "John",
                    "tag": "John#12345"
                }
            },
            {
                "id": 2,
                "name": "Amazon",
                "public": false,
              	"size": {
                    "total": 24,
                    "unbuy": 21
                },
                "user": {
                    "id": 2,
                    "name": "Jane",
                    "tag": "Jane#98765"
                }
            }
        ]
    }
    ```

- `GET /users/wishlists/shared/page/{num}/{nb}` : Retourne une liste paginée des listes de souhaits partagées avec l'utilisateur connecté

  - Parametre dans l'URL :
  
    - `num` : Le numéro de la page
    - `nb` : Le nombre d'élément par page

  - Retour

    ```json
    {
        "total": 5,
        "page": 1,
        "size": 2,
        "list": [
            {
                "id": 1,
                "name": "Ma liste de noel",
                "public": true,
                "size": {
                    "total": 8,
                    "unbuy": 4
                },
                "user": {
                    "id": 1,
                    "name": "John",
                    "tag": "John#12345"
                }
            },
            {
                "id": 2,
                "name": "Amazon",
                "public": false,
              	"size": {
                    "total": 24,
                    "unbuy": 21
                },
                "user": {
                    "id": 2,
                    "name": "Jane",
                    "tag": "Jane#98765"
                }
            }
        ]
    }
    ```
    
    Le retour ci-dessus correspond à l'URL /user/wishlists/shared/page/1/2

### POST

- `POST /users/login` : Connexion d'un utilisateur

  - Parametre dans la requete HTTP :

    - `login` : L'email de l'utilisateur (doit être unique), :question: possibilité plus tard de prendre un autre identifiant unique.
    - `password` : Le mot de passe de l'utilisateur 

    ```json
    {
        "login": "test@email.com",
        "password": "azerty"
    }
    ```

  - Retour :

    ```json
    {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.e06MJM0W2IGKwyOzwQEgNhrXgSi3envVExGR9uoKplQ",
        "user": {
            "id": 6,
            "name": "John Doe",
            "email": "test@email.com",
            "avatar": "http://gravatar.url",
            "public": false,
            "tag": "John_Doe#08520"
        }
    }
    ```

- `POST /users` : Inscription d'un utilisateur

  - Parametre dans la requete HTTP :

    - `name` : Le nom de l'utilisateur
    - `email` : L'email de l'utilisateur (doit être unique, est vérifié sur le serveur)
    - `password` : Le mot de passe

    ```json
    {
        "name": "John Doe",
        "email": "test@email.com",
        "password": "azerty"
    }
    ```

    Le tag est générer automatiquement par le serveur, et le profil de l'utilisateur est par défaut en privé (public à false). L'inscription ne connecte pas automatiquement utilisateur, il faut ensuite qu'il se connecte

### PUT

- `PUT /users/refresh` : Actualise le token de l'utilisateur connecté pour prolonger sa validité en générant un nouveau token

    - Retour :
      
          ```json
          {
              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.e06MJM0W2IGKwyOzwQEgNhrXgSi3envVExGR9uoKplQ",
          }
          ```

- `PUT /users` : Modification de l'utilisateur connecté (unn erreur si aucun utilisateur n'est connecté)

  - Parametre dans la requete HTTP :

    - `name (optionnel)` : Le nouveau nom de l'utilisateur
    - `email (optionnel)` : Le nouvelle email de l'utilisateur (doit être unique, est vérifié sur le serveur)
    - `password (optionnel)` : Le nouveau mot de passe
    - `public (optionnel)` : La visibilité du profil de l'utilisateur

    ```json
    {
        "name": "John Doe",
        "email": "test@email.com",
        "password": "azerty",
        "public": true
    }
    ```

  - Retour :

    ```json
    {
    	"id": 6,
        "name": "John Doe",
        "email": "test@email.com",
        "avatar": "http://gravatar.url",
        "public": true,
        "tag": "John_Doe#08520"
    }
    ```
