# Statistique

Liste de tous les les points d'entrée lié aux images, ils commencent tous par `/stats`.

### GET

- `GET /stats/user/{id}` : Statistique sur un utilisateur (l'utilisateur connecté peut uniquement accèder au statistique de ses contacts ou de lui même)

  - Parametre dans l'URL :

    - id : L'id de l'utilisateur

  - Retour :

    ```json
    {
        "wishlist": 12,
        "element": 68,
        "contact": 25,
        "idea": 14,
        "crush": 25,
        "reserved": 8,
        "purchased": 21,
        "averagePrice": 48.2,
        "lowestPrice": 12,
        "highestPrice": 1832
    }
    ```
