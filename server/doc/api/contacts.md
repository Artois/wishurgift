# Contact

Liste de tous les les points d'entrée lié aux contacts, ils commencent tous par `/contacts`. Tous les points d'entrée de contact nécéssite d'être connecté, sinon une erreur est retournée.

### GET

- `GET /contacts` : Retourne tous les contacts de l'utilisateur connecté

  - Retour :

    ```json
    {
        "total": 2,
        "list": [
            {
                "id": 48,
                "name": "John Doe",
                "email": "john.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "John_Doe#08546"
            },
            {
                "id": 84,
                "name": "Jane Doe",
                "email": "jane.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "Jane_Doe#08457"
            }
        ]
    }
    ```

- `GET /contacts/page/{num}/{nb}` : Retourne les contacts de l'utilisateur connecté sous forme paginée

  - Parametre dans l'URL :

    - `num` : Le numéro de la page
    - `nb` : Le nombre d'élément par page

  - Retour

    ```json
    {
        "total": 3,
        "page": 1,
        "size": 2,
        "list": [
            {
                "id": 48,
                "name": "John Doe",
                "email": "john.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "John_Doe#08546"
            },
            {
                "id": 84,
                "name": "Jane Doe",
                "email": "jane.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "Jane_Doe#08457"
            }
        ]
    }
    ```

    Le retour ci-dessus correspond à un appel à l'URL /contact/page/1/2

- `GET /contacts/pending` : Retourne la liste des utilisateurs ayant ajouté l'utilisateur connecté mais qui ne sont pas dans ses contacts.

  - Retour :

    ```json
    {
        "total": 2,
        "list": [
            {
                "id": 48,
                "name": "John Doe",
                "email": "john.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "John_Doe#08546"
            },
            {
                "id": 84,
                "name": "Jane Doe",
                "email": "jane.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "Jane_Doe#08457"
            }
        ]
    }
    ```

- `GET /contacts/pending/page/{num}/{nb}` : Retourne la liste paginée des utilisateurs ayant ajouté l'utilisateur connecté mais qui ne sont pas dans ses contacts.

  - Parametre dans l'URL :

    - `num` : Le numéro de la page
    - `nb` : Le nombre d'élément par page

  - Retour

    ```json
    {
        "total": 3,
        "page": 1,
        "size": 2,
        "list": [
            {
                "id": 48,
                "name": "John Doe",
                "email": "john.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "John_Doe#08546"
            },
            {
                "id": 84,
                "name": "Jane Doe",
                "email": "jane.doe@email.com",
                "avatar": "http://gravatar.url",
                "public": false,
                "tag": "Jane_Doe#08457"
            }
        ]
    }
    ```

    Le retour ci-dessus correspond à un appel à l'URL /contact/pending/page/1/2
    
 - `GET /contacts/request` : Retourne la liste des utilisateurs ajoutés par l'utilisateur connecté mais qui ne sont pas dans ses contacts.
 
   - Retour :
 
     ```json
     {
         "total": 2,
         "list": [
             {
                 "id": 48,
                 "name": "John Doe",
                 "email": "john.doe@email.com",
                 "avatar": "http://gravatar.url",
                 "public": false,
                 "tag": "John_Doe#08546"
             },
             {
                 "id": 84,
                 "name": "Jane Doe",
                 "email": "jane.doe@email.com",
                 "avatar": "http://gravatar.url",
                 "public": false,
                 "tag": "Jane_Doe#08457"
             }
         ]
     }
     ```
 
 - `GET /contacts/request/page/{num}/{nb}` : Retourne la liste paginée des utilisateurs ajoutés par l'utilisateur connecté mais qui ne sont pas dans ses contacts.
 
   - Parametre dans l'URL :
 
     - `num` : Le numéro de la page
     - `nb` : Le nombre d'élément par page
 
   - Retour
 
     ```json
     {
         "total": 3,
         "page": 1,
         "size": 2,
         "list": [
             {
                 "id": 48,
                 "name": "John Doe",
                 "email": "john.doe@email.com",
                 "avatar": "http://gravatar.url",
                 "public": false,
                 "tag": "John_Doe#08546"
             },
             {
                 "id": 84,
                 "name": "Jane Doe",
                 "email": "jane.doe@email.com",
                 "avatar": "http://gravatar.url",
                 "public": false,
                 "tag": "Jane_Doe#08457"
             }
         ]
     }
     ```
 
     Le retour ci-dessus correspond à un appel à l'URL /contact/request/page/1/2

### POST

- `POST /contacts` : Ajoute un utilisateur dans les contacts de l'utilisateur connecté. Pour que l'ajout fonctionne les deux utilisateurs doivent s'ajouter mutuellement.

  - Parametre dans la requete HTTP :

    - `tag` : Le tag de l'utilisateur à ajouter

    ```json
    {
        "tag": "John_Doe#08520"
    }
    ```

  - Retour :

    ```json
    {
        "id": 48,
        "name": "John Doe",
        "email": "john.doe@email.com",
        "avatar": "http://gravatar.url",
        "public": false,
        "tag": "John_Doe#08520"
    }
    ```

### DELETE

- `DELETE /contacts/{user}` : Supprime un utilisateur des contacts. Si la demande est en attente (pending) alors cela supprime la demande
  - Parametre dans l'URL :
    - `user` : L'id de l'utilisateur à supprimer de la liste de contact
