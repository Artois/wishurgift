# Commentaire

Liste de tous les les points d'entrée lié aux commentaires, ils commencent tous par `/comments`.

### GET

- `GET /comments/{id}` : Récupère les données d'un commentaire (il doit appartenir à un élément d'une liste dont l'utilisateur connecté à accès)

  - Parametre dans l'URL :

    - `id` : L'id du commentaire

  - Retour :

    ```json
    {
        "id": 1,
        "text": "je suis un commentaire",
        "date": "21/08/2020",
        "user": {
          "id": 8,
          "name": "John",
          "tag": "John#12345"
        }
    }
    ```
    
### PUT

- `PUT /comments/{id}` : Modifie un commentaire (le commentaire doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)

  - Parametre dans l'URL :

    - `id` : L'id du commentaire

  - Parametre dans la requete HTTP :

    - `text` : Le texte du commentaire

    ```json
    {
        "text": "je suis un autre commentaire"
    }
    ```

  - Retour :

    ```json
    {
        "id": 1,
        "text": "je suis un autre commentaire",
        "date": "24/08/2020",
        "user": {
          "id": 8,
          "name": "John",
          "tag": "John#12345"
        }
    }
    ```

### DELETE

- `DELETE /comments/{id}` : Supprime un commentaire (il doit appartenir à l'utiliseur connecté, sinon une erreur est retounée)
  - Parametre dans l'URL :
    - `id` : L'id du commentaire