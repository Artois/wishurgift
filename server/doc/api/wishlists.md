# Liste de souhait

Liste de tous les les points d'entrée lié à la gestion des listes de souhaits, ils commencent tous par `/wishlists`.

### GET

- `GET /wishlists/{id}` : Récupère le contenue d'une liste, les éléments achetés sont mis en dernier (Uniquement si c'est une liste de l'utilisateur, partagé ave l'utilisateur ou une liste publique. Une erreur est retournée sinon)

  - Parametre dans l'URL :

    - `id` : L'id de la liste

  - Retour :

    ```json
    {
        "id": 4,
        "name": "Ikea",
        "public": true,
        "size": {
            "total": 2,
            "unbuy": 1
        },
        "element": [
            {
                "id": 48,
                "status": "idea",
                "name": "Chaise",
                "link": "https://vendeur-de-chause.fr",
                "image": [
                    {
                        "id": 1,
                        "ext": "png",
                        "data": "data:image/png;base64,imageenbase64"
                    },
                    {
                        "id": 2,
                        "ext": "jpeg",
                        "data": "data:image/png;base64,imageenbase64"
                    }
                ],
                "address": "1 rue des rues",
                "price": 42.21,
                "description": "La meilleur chaise du monde"
            },
            {
                "id": 12,
                "status": "reserved",
                "name": "Table",
                "link": null,
                "image": [
                    {
                        "id": 3,
                        "ext": "png",
                        "data": "data:image/png;base64,imageenbase64"
                    }
                ],
                "address": null,
                "price": null,
                "description": null
            }
        ]
    }
    ```

    Le status possède 4 valeurs possibles : `idea` (quand c'est une idée d'achat), `crush` (quand l'achat est urgent), `reserved` (quand l'objet va être acheté), `purchased` (quand l'objet est acheté)
    
    De plus si c'est l'utilisateur qui possède la liste qui la récupére, alors un attribut shared est ajouté avec la liste des utilisateurs à qui il à partagé la liste (exemple en dessous)
    
    ```json
    {
      "shared": [
        {
          "id": 1,
          "name": "janeDoe",
          "tag": "janeDoe#04578"
        },
        {
          "id": 2,
          "name": "John doe",
          "tag": "John_doe#45123"
        }
      ]
    }
    ```

- `GET /wishlists/{id}/shared` : Récupére la liste des utilisateurs avec qui une liste est partagée

  - Parametre dans l'URL :

    - `id` : L'id de la liste

  - Retour :

    ```json
    {
        "list": [
            {
                "id": 1,
                "name": "janeDoe",
                "tag": "janeDoe#04578"
            },
            {
                "id": 2,
                "name": "John doe",
                "tag": "John_doe#45123"
            }
        ],
        "total": 2
    }
    ```

### POST

- `POST /wishlists` : Ajoute une liste à l'utilisateur actuellement connecté (une erreur est retourné sinon)

  - Parametre dans la requete HTTP :

    - `name` : Le nom de la liste
    - `public` : Liste publique (true) ou privé (false)

    ```json
    {
        "name": "Ikea",
        "public": true
    }
    ```

  - Retour :

    ```json
    {
        "id": 12,
        "name": "Ikea",
        "public": true,
        "size": {
        	"total": 0,
            "unbuy": 0
        }
    }
    ```

- `POST /wishlists/{id}/element` : Ajoute un element dans une liste

  - Parametre dans l'URL :
  
    - `id` : L'id de la liste à modifier, la liste doit appartenir à l'utilisateur connecté, une erreur est retournée sinon
  
  - Parametre dans la requete HTTP :
  
    - `name` : Le nom de l'élément
    - `link (optionnel)` : Lien vers l'élément
    - `image (optionnel)` : Les images de l'élément. C'est un tableau d'objets. Chaque objet composé de deux attributs, ext qui corresponf a l'extension du fichier et data les données de l'image en base64
    - `address (optionnel)` : L'adresse pour trouver l'element
    - `price (optionnel)` : Prix de l'objet
    - `description (optionnel)` : Description de l'élément
  
    ```json
    {
        "name": "Chaise",
        "link": "https://vendeur-de-chause.fr",
        "image": [
            {
                "ext": "png",
                "data": "data:image/png;base64,imageenbase64"
            },
            {
                "ext": "jpeg",
                "data": "data:image/png;base64,imageenbase64"
            }
        ],
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```
  
  - Retour :
  
    ```json
    {
        "id": 48,
        "status": "idea",
        "name": "Chaise",
        "link": "https://vendeur-de-chause.fr",
        "image": [
            {
                "id": 1,
                "ext": "png",
                "data": "data:image/png;base64,imageenbase64"
            },
            {
                "id": 2,
                "ext": "jpeg",
                "data": "data:image/png;base64,imageenbase64"
            }
        ],
        "address": "1 rue des rues",
        "price": 42.21,
        "description": "La meilleur chaise du monde"
    }
    ```
  
  Le status possède 4 valeurs possibles : `idea` (quand c'est une idée d'achat), `crush` (quand c'est un coup de coeur), `reserved` (quand l'objet va être acheté), `purchased` (quand l'objet est acheté). Par défaut l'objet est crée avec le statut idée.

### PUT

- `PUT /wishlists/{id}` : Modification d'une liste (la liste doit appartenir à l'utilisateur, sinon une erreur est retournée)

  - Parametre dans l'URL :

    - `id` : L'id de la liste à modifier

  - Parametre dans la requete HTTP :

    - `name (optionnel)` : Le nouveau nom de la liste
    - `public (optionnel)` : La visibilité de la liste

    ```json
    {
        "name": "Alinea",
        "public": false
    }
    ```

  - Retour :

    ```json
    {
        "id": 12,
        "name": "Alinea",
        "public": false,
        "size": {
            "total": 6,
            "unbuy": 1
        } 
    }
    ```

- `PUT /wishlists/{id}/share/{user}` : Partage une liste à un utilisateur (la liste doit appartenir à l'utilisateur connecté, sinon une erreur est retournée).

  - Parametre dans l'URL :
    - `id` : L'id de la liste à partager
    - `user` : L'id de l'utilisateur avec lequel on partage la liste

### DELETE

- `DELETE /wishlists/{id}` : Supprime une liste (la liste doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)
  - Parametre dans l'URL :
    - `id` : L'id de la liste à supprimer
- `DELETE /wishlists/{id}/share/{user}` : Annule le partage d'une liste à un utilisateur (la liste doit appartenir à l'utilisateur connecté, sinon une erreur est retourné)
  - Parametre dans l'URL :
    - `id` : L'id de la liste 
    - `user` : L'id de l'utilisateur
