#!/bin/bash

sudo echo "Starting..."
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
mkdir -p $DIR/../web/apps/
mkdir -p $DIR/../web/shared/logs/
sudo docker-compose up &> /dev/null &
echo "Done"