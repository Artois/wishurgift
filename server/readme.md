# Wishurgift Serveur

Serveur de l'application Wishurgift, dans le cadre du projet du second semestre de l'année 2019-2020 du M2 ILI à l'université d'Artois.

Les membres du groupe sont :
- Arthur Brandao
- Maxence Bacquet
- Ouahab Fenniche
- Aymeric Vandewoorde

## Prérequis

Pour pouvoir compiler et lancer le serveur depuis les sources les logiciels suivants sont nécessaire :
- Java (testé avec la version 13 d'Oracle et l'Open JDK 11, doit normalement fonctionner avec la version 8)
- Docker (testé avec la version 18.09.7)
- Docker-compose (testé avec la version 1.25.1 et 1.17.1)
- Maven (testé avec la version 3.6.0)

Pour information le serveur et son installation on été testé sur les OS suivant : KDE Neon 5.17 et Ubuntu 18.04.4 LTS, tous deux basé sur Debian 10 (Buster)

## Installation & Utilisation

Pour gérer le serveur des scripts bash ont été écrit, l'explication pour lancer le serveur seras donné avec et sans l'utilisation des scripts. L'utilité des différents scripts est expliquer à la fin de cette partie. De plus les commandes données sont toutes lancées depuis le dossier racine du serveur.

### Avec les scripts

Pour un 1er lancement il est conseillé d'utiliser run plutot que start (pour voir ce qui ce passe), mais les deux fonctionnent. La différence est décrite plus bas dans la section sur les scripts.

```shell
# Lance les dockers
./script/run.sh

# Compile et deploie le serveur
mvn package
```

:warning: Le docker de base de données doit absolument être lancé lors de la compilation et du déploiement du war. Si la base de données n'est pas accessible le maven package va échouer. Ceci est provoqué par la vérification (et la mise à jour) de la base de données si besoins.

Normalement après ces étapes le serveur est disponible à l'adresse http://localhost:8080

### Sans les scripts

```shell
# Création du dossier pour copier le war
mkdir ./web/apps

# Lancement des dockers
docker-compose up

# Compile et deploie le serveur
mvn package
```

La création du dossier `apps` n'est pas obligatoire, il seras de toute façon créer par le docker. Cependant en laissant le docker créer le dossier il possible que maven n'arrive pas à copier le war dedans (problème de droit), le créer avant de lancer le docker évite ce problème. De plus la remarque sur le docker de base de données (dans Avec les scripts) est toujours valable.

Normallement après ces étapes le serveur est disponible à l'adresse http://localhost:8080

### Autres informations
 
Pour installer une nouvelle version de l'application à partir des sources quand une 1er installation à déjà eu lieu, il suffit de lancer un `mvn package` alors que les dockers sont lancés. Le nouveau war va automatiquement être deployé à la place de l'ancien.

Il se peut parfois que le redeployement à chaud de Tomee entraine l'apparition de bugs (comme le fait qu'il ne trouve plus les class de PostgreSQL). Dans ce cas il suffit de couper et relancer les dockers pour résoudre les problèmes.

### Les scripts

Voici les différents scripts présent dans le projet, ce sont tous des scripts shell :
- clear-docker : Reset docker
- connect-db : Connexion à la base de données avec le client CLI postgres (doit être installé sur la machine)
- init : Création des dossiers manquants pour l'application
- postgres-cli : Connexion en mode CLI au docker PostgreSQL (le docker doit être lancé)
- run : Initialise (voir init) et lance les dockers (docker-compose up)
- server : Connexion SSH au serveur utilisé par l'application mobile
- start : Initialise (voir init) et lance les dockers (docker-compose up) en tache de fond
- stop : Stop les dockers (docker-compose stop)
- tomee-cli : Connexion en mode CLI au docker Tomee (le docker doit être lancé)

## Technologies

Voici les principales technologies et framework utilisé dans le projet :
- Jave 8 : Langage de programmation
- Maven : Outil de gestion et d'automatisation de production des projets logiciels Java
- Docker & Docker-compose : Lanceur d'applications dans des conteneurs logiciels
- Postgres 12.1 : SGBD
- Liquibase : Suivi, gestion et application des changements de schéma de base de données indépendament du SGBD
- OpenJPA : Implementation JPA (ORM)
- JUnit 4 : Test unitaire en Java
- Mockito : Framework pour les tests unitaires
- Lombok : Simplification du code
- JWT : Token pour l'authentification
- Gitlab CI : Intégration continue
- Tomee + : Serveur d'application
- [Bcrypt : Hash des mots de passe](https://github.com/patrickfav/bcrypt)

## Documenation

Les informations sur le fonctionnement et l'utilisation de l'API du serveur sont trouvable dans le [dossier /doc](./doc) du projet.

De plus quelques informations pour la configuration et la gestion du projet pour les développeur sont trouvable dans le [fichier /doc/dev.md](./doc/dev.md).

## Autre projet Wishurgift

Voici la liste des differents projets Wishurgift :
- [L'application Android](https://git.loquico.me/Artois/Wishurgift/src/master/android)
- [Le mock server](https://git.loquico.me/Artois/Wishurgift/src/master/mock-server)