package com.wishurgift.entry.controller;

import com.wishurgift.entry.validator.ElementAddValidator;
import com.wishurgift.entry.validator.WishlistAddValidator;
import com.wishurgift.entry.validator.WishlistUpdateValidator;
import com.wishurgift.helper.ControllerHelper;
import com.wishurgift.helper.ElementMaker;
import com.wishurgift.helper.UserMaker;
import com.wishurgift.helper.WishlistMaker;
import com.wishurgift.service.WishlistService;
import com.wishurgift.service.dto.*;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import com.wishurgift.service.dto.request.WishlistRequestDTO;
import com.wishurgift.utils.Constant;
import com.wishurgift.wrapper.Page;
import com.wishurgift.wrapper.Result;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WishlistControllerTest {

    @Mock
    private WishlistService wishlistService;

    @Mock
    private WishlistAddValidator wishlistAddValidator;

    @Mock
    private WishlistUpdateValidator wishlistUpdateValidator;

    @Mock
    private ElementAddValidator elementAddValidator;

    @InjectMocks
    private WishlistController controller;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTest() {
        WishlistRequestDTO request = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        WishlistHeaderDTO expected = WishlistMaker.makeWishlistHeaderDTO();

        Mockito.when(wishlistAddValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(wishlistService.add(principal, request)).thenReturn(expected);

        Response response = controller.add(request, securityContext);
        WishlistHeaderDTO actual = ControllerHelper.extractSuccessResult(response, WishlistHeaderDTO.class);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isPublic(), actual.isPublic());
        WishlistSizeDTO expectedSize = expected.getSize();
        WishlistSizeDTO actualSize = actual.getSize();
        assertEquals(expectedSize.getTotal(), actualSize.getTotal());
        assertEquals(expectedSize.getUnbuy(), actualSize.getUnbuy());
    }

    @Test
    public void invalidRequestAddTest() {
        WishlistRequestDTO request = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        List<String> trace = new ArrayList<>();
        trace.add("trace1");
        trace.add("trace2");

        Mockito.when(wishlistAddValidator.isNotValid(request)).thenReturn(true);
        Mockito.when(wishlistAddValidator.getAllErrors()).thenReturn(trace);

        Response response = controller.add(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        trace.forEach(elt -> assertTrue(error.getTrace().contains(elt)));
    }

    @Test
    public void invalidTokenAddTest() {
        WishlistRequestDTO request = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();
        List<String> trace = new ArrayList<>();
        trace.add("trace1");
        trace.add("trace2");

        Mockito.when(wishlistAddValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(wishlistAddValidator.getAllErrors()).thenReturn(trace);

        Response response = controller.add(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void failAddTest() {
        WishlistRequestDTO request = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(wishlistAddValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(wishlistService.add(principal, request)).thenReturn(null);

        Response response = controller.add(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void updateTest() {
        long id = 8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        WishlistHeaderDTO wishlistHeaderDTO = new WishlistHeaderDTO();
        wishlistHeaderDTO.setId(id);
        wishlistHeaderDTO.setName(wishlistRequestDTO.getName());
        wishlistHeaderDTO.setPublic(wishlistRequestDTO.isPublic());
        Result<WishlistHeaderDTO> result = Result.success(WishlistHeaderDTO.class).data(wishlistHeaderDTO).build();

        Mockito.when(wishlistUpdateValidator.isNotValid(wishlistRequestDTO)).thenReturn(false);
        Mockito.when(wishlistService.update(principal, id, wishlistRequestDTO)).thenReturn(result);

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        WishlistHeaderDTO actual = ControllerHelper.extractSuccessResult(response, WishlistHeaderDTO.class);
        assertEquals(id, actual.getId());
        assertEquals(wishlistRequestDTO.getName(), actual.getName());
        assertEquals(wishlistRequestDTO.isPublic(), actual.isPublic());
    }

    @Test
    public void errorUpdateTest() {
        long id = 8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        String trace = "trace";
        Result<ErrorDTO> result = Result.error().data(Result.Error.UNKNOWN).trace(trace).build();

        Mockito.when(wishlistUpdateValidator.isNotValid(wishlistRequestDTO)).thenReturn(false);
        Mockito.when(wishlistService.update(principal, id, wishlistRequestDTO)).thenReturn(result);

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNKNOWN.getCode(), actual.getCode());
        assertEquals(Result.Error.UNKNOWN.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(trace));
    }

    @Test
    public void invalidIdUpdateTest() {
        long id = -8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), actual.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void invalidRequestUpdateTest() {
        long id = 8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        List<String> trace = new ArrayList<>();
        trace.add("trace1");
        trace.add("trace2");

        Mockito.when(wishlistUpdateValidator.isNotValid(wishlistRequestDTO)).thenReturn(true);
        Mockito.when(wishlistUpdateValidator.getAllErrors()).thenReturn(trace);

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), actual.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), actual.getMessage());
        assertEquals(trace.size(), actual.getTrace().size());
        trace.forEach(elt -> assertTrue(actual.getTrace().contains(elt)));
    }

    @Test
    public void userNotFoundUpdateTest() {
        long id = 8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Mockito.when(wishlistUpdateValidator.isNotValid(wishlistRequestDTO)).thenReturn(false);

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), actual.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void removeTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        Result<Object> result = Result.success().build();

        Mockito.when(wishlistService.remove(principal, id)).thenReturn(result);

        Response response = controller.remove(id, securityContext);
        ControllerHelper.successResult(response);
    }

    @Test
    public void errorRemoveTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        String trace = "trace";
        Result<ErrorDTO> result = Result.error().data(Result.Error.UNKNOWN).trace(trace).build();

        Mockito.when(wishlistService.remove(principal, id)).thenReturn(result);

        Response response = controller.remove(id, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNKNOWN.getCode(), actual.getCode());
        assertEquals(Result.Error.UNKNOWN.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(trace));
    }

    @Test
    public void invalidIdRemoveTest() {
        long id = -8;
        WishlistRequestDTO wishlistRequestDTO = WishlistMaker.makeWishlistRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.update(id, wishlistRequestDTO, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), actual.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void userNotFoundRemoveTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.remove(id, securityContext);
        ErrorDTO actual = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), actual.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), actual.getMessage());
        assertEquals(1, actual.getTrace().size());
        assertTrue(actual.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void shareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO userDTO = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.share(userDTO, id, userId)).thenReturn(true);

        Response response = controller.share(id, userId, securityContext);
        ControllerHelper.successResult(response);
    }

    @Test
    public void invalidRequest1ShareTest() {
        long id = -8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.share(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void invalidRequest2ShareTest() {
        long id = 8;
        long userId = -4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.share(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void userNotFoundShareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.share(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void unauthorizedShareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO userDTO = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.share(userDTO, id, userId)).thenReturn(false);

        Response response = controller.share(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    @Test
    public void unshareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO userDTO = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.unshare(userDTO, id, userId)).thenReturn(true);

        Response response = controller.unshare(id, userId, securityContext);
        ControllerHelper.successResult(response);
    }

    @Test
    public void invalidRequest1UnshareTest() {
        long id = -8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.unshare(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void invalidRequest2UnshareTest() {
        long id = 8;
        long userId = -4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.unshare(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void userNotFoundUnshareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.unshare(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void unauthorizedUnshareTest() {
        long id = 8;
        long userId = 4;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO userDTO = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.unshare(userDTO, id, userId)).thenReturn(false);

        Response response = controller.unshare(id, userId, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    @Test
    public void getWishlistTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        WishlistDTO expected = WishlistMaker.makeWishlistDTO();

        Mockito.when(wishlistService.getWishlist(id, principal)).thenReturn(expected);

        Response response = controller.getWishlist(id, securityContext);
        WishlistDTO actual = ControllerHelper.extractSuccessResult(response, WishlistDTO.class);
        assertEquals(expected, actual);
    }

    @Test
    public void invalidRequestGetWishlistTest() {
        long id = -8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.getWishlist(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void unableToGetDataGetWishlistTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.getWishlist(id, principal)).thenReturn(null);

        Response response = controller.getWishlist(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    @Test
    public void elementTest() {
        long id = 8;
        ElementRequestDTO request = ElementMaker.makeElementRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        ElementDTO expected = ElementMaker.makeElementDTO();

        Mockito.when(elementAddValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(wishlistService.addElement(principal, id, request)).thenReturn(expected);

        Response response = controller.element(id, request, securityContext);
        ElementDTO actual = ControllerHelper.extractSuccessResult(response, ElementDTO.class);
        assertEquals(expected, actual);
    }

    @Test
    public void invalidIdElementTest() {
        long id = -8;
        ElementRequestDTO request = ElementMaker.makeElementRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.element(id, request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void invalidRequestElementTest() {
        long id = 8;
        ElementRequestDTO request = ElementMaker.makeElementRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        List<String> trace = new ArrayList<>();
        trace.add("trace1");
        trace.add("trace2");

        Mockito.when(elementAddValidator.isNotValid(request)).thenReturn(true);
        Mockito.when(elementAddValidator.getAllErrors()).thenReturn(trace);

        Response response = controller.element(id, request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(trace.size(), error.getTrace().size());
        trace.forEach(elt -> assertTrue(error.getTrace().contains(elt)));
    }

    @Test
    public void userNotFoundElementTest() {
        long id = 8;
        ElementRequestDTO request = ElementMaker.makeElementRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Mockito.when(elementAddValidator.isNotValid(request)).thenReturn(false);

        Response response = controller.element(id, request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void failElementTest() {
        long id = 8;
        ElementRequestDTO request = ElementMaker.makeElementRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(elementAddValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(wishlistService.addElement(principal, id, request)).thenReturn(null);

        Response response = controller.element(id, request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    @Test
    public void getSharedTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        List<UserLightDTO> list = new ArrayList<>();
        list.add(UserMaker.makeUserLightDTO());
        list.add(UserMaker.makeUserLightDTO());
        list.get(1).setId(2);
        Page<UserLightDTO> expected = Page.unpaged(list);

        Mockito.when(wishlistService.sharedWith(principal, id)).thenReturn(expected);

        Response response = controller.getShared(id, securityContext);
        Page<UserLightDTO> actual = ControllerHelper.extractSuccessResult(response, Page.class);
        assertNull(actual.getSize());
        assertNull(actual.getPage());
        assertEquals(expected.getTotal(), actual.getTotal());
        assertEquals(list, actual.getList());
    }

    @Test
    public void invalidIdGetSharedTest() {
        long id = -8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.getShared(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void userNotFoundGetSharedTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.getShared(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void failGetSharedTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(wishlistService.sharedWith(principal, id)).thenReturn(null);

        Response response = controller.getShared(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

}
