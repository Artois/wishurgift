package com.wishurgift.entry.controller;

import com.wishurgift.entry.validator.LoginValidator;
import com.wishurgift.entry.validator.RegisterValidator;
import com.wishurgift.entry.validator.UpdateUserValidator;
import com.wishurgift.helper.ControllerHelper;
import com.wishurgift.helper.UserMaker;
import com.wishurgift.helper.WishlistMaker;
import com.wishurgift.service.UserService;
import com.wishurgift.service.dto.ErrorDTO;
import com.wishurgift.service.dto.LoginDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.WishlistHeaderDTO;
import com.wishurgift.service.dto.request.LoginRequestDTO;
import com.wishurgift.service.dto.request.RegisterRequestDTO;
import com.wishurgift.service.dto.request.UpdateUserRequestDTO;
import com.wishurgift.utils.Constant;
import com.wishurgift.wrapper.Page;
import com.wishurgift.wrapper.Result;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserControllerTest {

    @Mock
    private UserService service;

    @Mock
    private RegisterValidator registerValidator;

    @Mock
    private LoginValidator loginValidator;

    @Mock
    private UpdateUserValidator updateUserValidator;

    @InjectMocks
    private UserController controller;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loginValidTest() {
        LoginRequestDTO loginRequestDTO = UserMaker.makeLoginRequestDTO();
        LoginDTO loginDTO = makeLoginDTO();

        Mockito.when(loginValidator.isNotValid(loginRequestDTO)).thenReturn(false);
        Mockito.when(service.login(loginRequestDTO)).thenReturn(loginDTO);

        Response response = controller.login(loginRequestDTO);
        LoginDTO actual = ControllerHelper.extractSuccessResult(response, LoginDTO.class);
        assertEquals(loginDTO, actual);
    }

    @Test
    public void loginInvalidTest() {
        LoginRequestDTO loginRequestDTO = UserMaker.makeLoginRequestDTO();
        String errorTrace1 = "trace1";
        String errorTrace2 = "trace2";
        List<String> errorList = new ArrayList<>();
        errorList.add(errorTrace1);
        errorList.add(errorTrace2);

        Mockito.when(loginValidator.isNotValid(loginRequestDTO)).thenReturn(true);
        Mockito.when(loginValidator.getAllErrors()).thenReturn(errorList);

        Response response = controller.login(loginRequestDTO);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertTrue(error.getTrace().contains(errorTrace1));
        assertTrue(error.getTrace().contains(errorTrace2));
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
    }

    @Test
    public void loginInvalidUserTest() {
        LoginRequestDTO loginRequestDTO = UserMaker.makeLoginRequestDTO();

        Mockito.when(loginValidator.isNotValid(loginRequestDTO)).thenReturn(false);
        Mockito.when(service.login(loginRequestDTO)).thenReturn(null);

        Response response = controller.login(loginRequestDTO);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
    }

    @Test
    public void registerValidTest() {
        RegisterRequestDTO registerRequestDTO = UserMaker.makeRegisterRequestDTO();
        UserDTO userDTO = UserMaker.makeUserDTO();

        Mockito.when(registerValidator.isNotValid(registerRequestDTO)).thenReturn(false);
        Mockito.when(service.register(registerRequestDTO)).thenReturn(userDTO);

        Response response = controller.register(registerRequestDTO);
        ControllerHelper.successResult(response);
    }

    @Test
    public void registerInvalidTest() {
        RegisterRequestDTO registerRequestDTO = UserMaker.makeRegisterRequestDTO();
        String errorTrace1 = "trace1";
        String errorTrace2 = "trace2";
        List<String> errorList = new ArrayList<>();
        errorList.add(errorTrace1);
        errorList.add(errorTrace2);

        Mockito.when(registerValidator.isNotValid(registerRequestDTO)).thenReturn(true);
        Mockito.when(registerValidator.getAllErrors()).thenReturn(errorList);

        Response response = controller.register(registerRequestDTO);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertTrue(error.getTrace().contains(errorTrace1));
        assertTrue(error.getTrace().contains(errorTrace2));
    }

    @Test
    public void registerAlreadyExistTest() {
        RegisterRequestDTO registerRequestDTO = UserMaker.makeRegisterRequestDTO();

        Mockito.when(registerValidator.isNotValid(registerRequestDTO)).thenReturn(false);
        Mockito.when(service.register(registerRequestDTO)).thenReturn(null);

        Response response = controller.register(registerRequestDTO);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.USER_ALREADY_EXIST.getCode(), error.getCode());
        assertEquals(Result.Error.USER_ALREADY_EXIST.getMessage(), error.getMessage());
    }

    @Test
    public void refreshTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        String newToken = "nouveautoken";

        Mockito.when(service.refresh(Mockito.any(Principal.class))).thenReturn(newToken);

        Response response = controller.refresh(securityContext);
        LoginDTO result = ControllerHelper.extractSuccessResult(response, LoginDTO.class);
        assertEquals(newToken, result.getToken());
        assertNull(result.getUser());
    }

    @Test
    public void refreshFailTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Mockito.when(service.refresh(Mockito.any(Principal.class))).thenReturn(null);

        Response response = controller.refresh(securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
    }

    @Test
    public void getUserTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO user = UserMaker.makeUserDTO();
        Result<UserDTO> res = Result.success(UserDTO.class).data(user).build();
        long id = 8;

        Mockito.when(service.getUser(user, id)).thenReturn(res);

        Response response = controller.getUser(id, securityContext);
        UserDTO actual = ControllerHelper.extractSuccessResult(response, UserDTO.class);
        assertEquals(user, actual);
    }

    @Test
    public void getUserErrorTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO user = UserMaker.makeUserDTO();
        long id = 8;
        List<String> trace = new ArrayList<>();
        trace.add("trace1");
        trace.add("trace2");
        Result<ErrorDTO> expected = Result.error().data(4, "erreurMessage").trace(trace).build();

        Mockito.when(service.getUser(user, id)).thenReturn(expected);

        Response response = controller.getUser(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(expected.getData(), error);
    }

    @Test
    public void searchTest() {
        String request = "search";
        int limit = 12;
        List<UserDTO> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            UserDTO userDTO = UserMaker.makeUserDTO();
            userDTO.setId(i + 1);
            expected.add(userDTO);
        }

        Mockito.when(service.search(request, limit)).thenReturn(Page.unpaged(expected));

        Response response = controller.search(request, limit);
        Page<UserDTO> actual = (Page<UserDTO>) ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(expected.size(), actual.getTotal());
        assertEquals(expected, actual.getList());
        expected.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void searchWithoutLimitTest() {
        String request = "search";
        List<UserDTO> expected = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            UserDTO userDTO = UserMaker.makeUserDTO();
            userDTO.setId(i + 1);
            expected.add(userDTO);
        }

        Mockito.when(service.search(request, 0)).thenReturn(Page.unpaged(expected));

        Response response = controller.search(request);
        Page<UserDTO> actual = (Page<UserDTO>) ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(expected.size(), actual.getTotal());
        assertEquals(expected, actual.getList());
        expected.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void searchInvalidRequestTest() {
        Response response = controller.search("", 2);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.SEARCH_REQUEST_EMPTY));

        response = controller.search("  ", 2);
        error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.SEARCH_REQUEST_EMPTY));

        response = controller.search("\t  \n ", 2);
        error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.SEARCH_REQUEST_EMPTY));

        response = controller.search(null, 2);
        error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.SEARCH_REQUEST_EMPTY));

        response = controller.search("coucou", -2);
        error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.LIMIT_INVALID));

        response = controller.search(" ", -2);
        error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.SEARCH_REQUEST_EMPTY));
    }

    @Test
    public void updateTest() {
        UpdateUserRequestDTO request = UserMaker.makeUpdateUserRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        UserDTO expected = UserMaker.makeUserDTO();
        expected.setId(2);
        expected.setName("newname");

        Mockito.when(updateUserValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(service.update(principal, request)).thenReturn(expected);

        Response response = controller.update(request, securityContext);
        UserDTO actual = ControllerHelper.extractSuccessResult(response, UserDTO.class);
        assertEquals(expected, actual);
    }

    @Test
    public void invalidUserUpdateTest() {
        UpdateUserRequestDTO request = UserMaker.makeUpdateUserRequestDTO();
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.update(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void invalidRequestUpdateTest() {
        UpdateUserRequestDTO request = UserMaker.makeUpdateUserRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        String trace1 = "trace1";
        String trace2 = "trace2";
        List<String> trace = new ArrayList<>();
        trace.add(trace1);
        trace.add(trace2);

        Mockito.when(updateUserValidator.isNotValid(request)).thenReturn(true);
        Mockito.when(updateUserValidator.getAllErrors()).thenReturn(trace);

        Response response = controller.update(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(trace.size(), error.getTrace().size());
        trace.forEach(elt -> assertTrue(error.getTrace().contains(elt)));
    }

    @Test
    public void failUpdateTest() {
        UpdateUserRequestDTO request = UserMaker.makeUpdateUserRequestDTO();
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(updateUserValidator.isNotValid(request)).thenReturn(false);
        Mockito.when(service.update(principal, request)).thenReturn(null);

        Response response = controller.update(request, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNAUTHORIZED.getCode(), error.getCode());
        assertEquals(Result.Error.UNAUTHORIZED.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.EMAIL_ALREADY_USED));
    }

    @Test
    public void wishlistTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        List<WishlistHeaderDTO> list = new ArrayList<>();
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.get(1).setId(2);

        Mockito.when(service.getWishlist(principal, id)).thenReturn(Page.unpaged(list));

        Response response = controller.wishlist(id, securityContext);
        Page<WishlistHeaderDTO> actual = ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(list.size(), actual.getTotal());
        assertNull(actual.getPage());
        assertNull(actual.getSize());
        list.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void invalidRequestWishlistTest() {
        long id = -8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.wishlist(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void unableToGetDataWishlistTest() {
        long id = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(service.getWishlist(principal, id)).thenReturn(null);

        Response response = controller.wishlist(id, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void wishlistPageTest() {
        long id = 8;
        int page = 1;
        int nb = 5;
        int total = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        List<WishlistHeaderDTO> list = new ArrayList<>();
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.get(1).setId(2);

        Mockito.when(service.getWishlist(page, nb, principal, id)).thenReturn(Page.paged(page, total, list));

        Response response = controller.wishlistPage(id, page, nb, securityContext);
        Page<WishlistHeaderDTO> actual = ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(total, actual.getTotal());
        assertEquals(page, (long) actual.getPage());
        assertEquals(list.size(), (long) actual.getSize());
        list.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void invalidRequest1WishlistPageTest() {
        long id = -8;
        int page = 1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.wishlistPage(id, page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.INVALID_ID));
    }

    @Test
    public void invalidRequest2WishlistPageTest() {
        long id = 8;
        int page = -1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.wishlistPage(id, page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.PAGE_NUMBER_INVALID));
    }

    @Test
    public void unableToGetDataWishlistPageTest() {
        long id = 8;
        int page = 1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(service.getWishlist(page, elt, principal, id)).thenReturn(null);

        Response response = controller.wishlistPage(id, page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void sharedTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        List<WishlistHeaderDTO> list = new ArrayList<>();
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.get(1).setId(2);

        Mockito.when(service.getShared(principal)).thenReturn(Page.unpaged(list));

        Response response = controller.shared(securityContext);
        Page<WishlistHeaderDTO> actual = ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(list.size(), actual.getTotal());
        assertNull(actual.getPage());
        assertNull(actual.getSize());
        list.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void userNotFoundSharedTest() {
        long id = -8;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.shared(securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void unableToGetDataSharedTest() {
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(service.getShared(principal)).thenReturn(null);

        Response response = controller.shared(securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    @Test
    public void sharedPageTest() {
        int page = 1;
        int nb = 5;
        int total = 8;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();
        List<WishlistHeaderDTO> list = new ArrayList<>();
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.add(WishlistMaker.makeWishlistHeaderDTO());
        list.get(1).setId(2);

        Mockito.when(service.getShared(page, nb, principal)).thenReturn(Page.paged(page, total, list));

        Response response = controller.sharedPage(page, nb, securityContext);
        Page<WishlistHeaderDTO> actual = ControllerHelper.extractSuccessResult(response, Page.class);
        assertEquals(total, actual.getTotal());
        assertEquals(page, (long) actual.getPage());
        assertEquals(list.size(), (long) actual.getSize());
        list.forEach(elt -> assertTrue(actual.getList().contains(elt)));
    }

    @Test
    public void userNotFoundSharedPageTest() {
        int page = 1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeInvalidSecurityContext();

        Response response = controller.sharedPage(page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_CONNECT.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.USER_NOT_FOUND));
    }

    @Test
    public void invalidRequestWishlistPageTest() {
        int page = -1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeSecurityContext();

        Response response = controller.sharedPage(page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.INVALID_REQUEST.getCode(), error.getCode());
        assertEquals(Result.Error.INVALID_REQUEST.getMessage(), error.getMessage());
        assertEquals(1, error.getTrace().size());
        assertTrue(error.getTrace().contains(Constant.PAGE_NUMBER_INVALID));
    }

    @Test
    public void unableToGetDataSharedPageTest() {
        int page = 1;
        int elt = 5;
        SecurityContext securityContext = UserMaker.makeSecurityContext();
        UserDTO principal = UserMaker.makeUserDTO();

        Mockito.when(service.getShared(page, elt, principal)).thenReturn(null);

        Response response = controller.sharedPage(page, elt, securityContext);
        ErrorDTO error = ControllerHelper.extractErrorResult(response);
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getCode(), error.getCode());
        assertEquals(Result.Error.UNABLE_TO_GET_DATA.getMessage(), error.getMessage());
        assertTrue(error.getTrace().isEmpty());
    }

    private LoginDTO makeLoginDTO() {
        return new LoginDTO("ceciEstUnToken", UserMaker.makeUserDTO());
    }

}
