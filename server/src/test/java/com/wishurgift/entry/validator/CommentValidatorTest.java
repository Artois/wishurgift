package com.wishurgift.entry.validator;

import com.wishurgift.helper.CommentMaker;
import com.wishurgift.service.dto.request.CommentRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class CommentValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    CommentValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validate() {
        CommentRequestDTO data = CommentMaker.makeCommentRequestDTO();
        assertTrue(validator.isValid(data));

        data.setText(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(CommentValidator.COMMENT_REQUIRED));

        data.setText("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(CommentValidator.COMMENT_REQUIRED));

        data.setText("   ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(CommentValidator.COMMENT_REQUIRED));

        data.setText("  \t  \n");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(CommentValidator.COMMENT_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
