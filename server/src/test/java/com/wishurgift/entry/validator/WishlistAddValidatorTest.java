package com.wishurgift.entry.validator;

import com.wishurgift.helper.WishlistMaker;
import com.wishurgift.service.dto.request.WishlistRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class WishlistAddValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    WishlistAddValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validatorTest() {
        WishlistRequestDTO data = WishlistMaker.makeWishlistRequestDTO();
        assertTrue(validator.isValid(data));

        data.setPublic(true);
        assertTrue(validator.isValid(data));

        data.setName(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.NAME_REQUIRED));

        data.setName("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.NAME_REQUIRED));

        data.setName("  \n \t ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.NAME_REQUIRED));

        data.setName("name");
        data.setPublic(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.PUBLIC_REQUIRED));

        data.setName("  \n \t ");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.NAME_REQUIRED));
        assertTrue(validator.getAllErrors().contains(WishlistAddValidator.PUBLIC_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
