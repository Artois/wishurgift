package com.wishurgift.entry.validator;

import com.wishurgift.helper.UserMaker;
import com.wishurgift.service.dto.request.TagRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class TagValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    TagValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validatorTest() {
        TagRequestDTO data = UserMaker.makeTagRequestDTO();
        assertTrue(validator.isValid(data));
        assertEquals(0, validator.getErrorNumber());

        data.setTag(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_REQUIRED));

        data.setTag("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_REQUIRED));

        data.setTag("\n  \t \n");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_REQUIRED));

        data.setTag("#");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_INVALID));

        data.setTag("hello");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_INVALID));

        data.setTag("name#");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_INVALID));

        data.setTag("#12345");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_USERNAME_INVALID));

        data.setTag("name#123456");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_ID_INVALID));

        data.setTag("name#pasunid");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_ID_INVALID));

        data.setTag("#123456");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_USERNAME_INVALID));
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_ID_INVALID));

        data.setTag("#pasunid");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_USERNAME_INVALID));
        assertTrue(validator.getAllErrors().contains(TagValidator.TAG_ID_INVALID));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }
    
}
