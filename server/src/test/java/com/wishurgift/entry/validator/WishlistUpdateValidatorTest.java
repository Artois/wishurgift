package com.wishurgift.entry.validator;

import com.wishurgift.helper.WishlistMaker;
import com.wishurgift.service.dto.request.WishlistRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class WishlistUpdateValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    WishlistUpdateValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validateTest() {
        WishlistRequestDTO data = WishlistMaker.makeWishlistRequestDTO();
        assertTrue(validator.isValid(data));

        data.setName(null);
        assertTrue(validator.isValid(data));

        data.setName("name");
        data.setPublic(null);
        assertTrue(validator.isValid(data));

        data.setName(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistUpdateValidator.ONE_UPDATED_FIELD_REQUIRED));

        data.setName("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistUpdateValidator.ONE_UPDATED_FIELD_REQUIRED));

        data.setName("  \t  \n ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(WishlistUpdateValidator.ONE_UPDATED_FIELD_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
