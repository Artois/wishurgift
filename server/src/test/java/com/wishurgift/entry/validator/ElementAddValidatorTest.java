package com.wishurgift.entry.validator;

import com.wishurgift.helper.ElementMaker;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import java.util.Collections;

import static org.junit.Assert.*;

public class ElementAddValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    ElementAddValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validate() {
        ElementRequestDTO data = ElementMaker.makeElementRequestDTO();
        validator.isValid(data);
        assertTrue(validator.isValid(data));

        data.setName(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.NAME_REQUIRED));

        data.setName("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.NAME_REQUIRED));

        data.setName("\n    \t   ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.NAME_REQUIRED));

        data.setName("name");
        data.setPrice(-12.34);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.PRICE_POSITIVE));

        data.setPrice(8.42);
        data.getImage().get(0).setExt(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains("0: " + ImageValidator.EXT_REQUIRED));

        data.getImage().get(1).setExt("jpeg");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains("0: " + ImageValidator.EXT_REQUIRED));
        assertTrue(validator.getAllErrors().contains("1: " + ImageValidator.DATA_INVALID));

        data.setImage(null);
        assertTrue(validator.isValid(data));

        data.setImage(Collections.emptyList());
        assertTrue(validator.isValid(data));

        data.setLink("https://urlsecured.com");
        assertTrue(validator.isValid(data));

        data.setLink("pas une url");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.URL_INVALID));

        data.setLink("ftp://uneurlftp.fr");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.URL_INVALID));

        data = new ElementRequestDTO();
        data.setName("name");
        assertTrue(validator.isValid(data));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
