package com.wishurgift.entry.validator;

import com.wishurgift.helper.UserMaker;
import com.wishurgift.service.dto.request.LoginRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class LoginValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    LoginValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validatorTest() {
        LoginRequestDTO data = UserMaker.makeLoginRequestDTO();
        assertTrue(validator.isValid(data));
        assertEquals(0, validator.getErrorNumber());

        // Test mot de passe

        data.setPassword(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.PASSWORD_REQUIRED));

        data.setPassword("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.PASSWORD_REQUIRED));

        data.setPassword("\t\n ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.PASSWORD_REQUIRED));

        // Test Login

        data.setPassword("pass");
        data.setLogin(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_REQUIRED));

        data.setLogin("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_REQUIRED));

        data.setLogin(" \t\n");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_REQUIRED));

        data.setLogin("pasunLogin");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_EMAIL_INVALID));

        // Test erreur multiple

        data.setPassword("");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_EMAIL_INVALID));
        assertTrue(validator.getAllErrors().contains(LoginValidator.PASSWORD_REQUIRED));

        data.setLogin("\t\t");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(LoginValidator.LOGIN_REQUIRED));
        assertTrue(validator.getAllErrors().contains(LoginValidator.PASSWORD_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
