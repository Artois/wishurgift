package com.wishurgift.entry.validator;

import com.wishurgift.helper.ElementMaker;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import java.util.Collections;

import static org.junit.Assert.*;

public class ElementUpdateValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    ElementUpdateValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validate() {
        ElementRequestDTO data = ElementMaker.makeElementRequestDTO();
        data.setImage(null);
        validator.isValid(data);
        assertTrue(validator.isValid(data));

        data.setPrice(-12.34);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementUpdateValidator.PRICE_POSITIVE));

        data.setAddress(null);
        data.setPrice(.0);
        assertTrue(validator.isValid(data));

        data.setImage(Collections.singletonList(ElementMaker.makeImageRequestDTO()));
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementUpdateValidator.NO_IMAGE_UPDATE));

        data.setImage(Collections.emptyList());
        assertTrue(validator.isValid(data));

        data.setLink("https://urlsecured.com");
        assertTrue(validator.isValid(data));

        data.setLink("pas une url");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.URL_INVALID));

        data.setLink("ftp://uneurlftp.fr");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.URL_INVALID));

        data.setPrice(-8.88);
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.URL_INVALID));
        assertTrue(validator.getAllErrors().contains(ElementAddValidator.PRICE_POSITIVE));

        data = new ElementRequestDTO();
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ElementUpdateValidator.ONE_UPDATED_FIELD_REQUIRED));

        data.setName("name");
        assertTrue(validator.isValid(data));

        data.setName("  \t \n   ");
        data.setPrice(2.4);
        assertTrue(validator.isValid(data));

        data.setPrice(null);
        data.setLink("http://link.com");
        assertTrue(validator.isValid(data));

        data.setLink("");
        data.setDescription("desc");
        assertTrue(validator.isValid(data));

        data.setDescription(null);
        data.setAddress("1 rue des rues");
        assertTrue(validator.isValid(data));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
