package com.wishurgift.entry.validator;

import com.wishurgift.helper.UserMaker;
import com.wishurgift.service.dto.request.RegisterRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class RegisterValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    RegisterValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validatorTest() {
        RegisterRequestDTO data = UserMaker.makeRegisterRequestDTO();
        assertTrue(validator.isValid(data));
        assertEquals(0, validator.getErrorNumber());

        // Test mot de passe

        data.setPassword(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));

        data.setPassword("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));

        data.setPassword("\t\n ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));

        // Test Nom

        data.setPassword("pass");
        data.setName(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));

        data.setName("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));

        data.setName("\t \n");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));

        // Test Email

        data.setName("aze");
        data.setEmail(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_REQUIRED));

        data.setEmail("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_REQUIRED));

        data.setEmail(" \t\n");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_REQUIRED));

        data.setEmail("pasunemail");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_INVALID));

        // Test erreur multiple

        data.setName("   ");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_INVALID));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));

        data.setPassword("\n\n");
        assertFalse(validator.isValid(data));
        assertEquals(3, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_INVALID));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));

        data.setEmail("\t\t");
        assertFalse(validator.isValid(data));
        assertEquals(3, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.EMAIL_REQUIRED));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));

        data.setEmail("jesuis.unmail@val.id");
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(RegisterValidator.NAME_REQUIRED));
        assertTrue(validator.getAllErrors().contains(RegisterValidator.PASSWORD_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
