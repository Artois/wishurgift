package com.wishurgift.entry.validator;

import com.wishurgift.helper.UserMaker;
import com.wishurgift.service.dto.request.UpdateUserRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class UpdateUserValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    UpdateUserValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validatorTest() {
        UpdateUserRequestDTO data = UserMaker.makeUpdateUserRequestDTO();
        assertTrue(validator.isValid(data));

        data.setPublic(null);
        assertTrue(validator.isValid(data));

        data.setPublic(false);
        data.setPassword(null);
        assertTrue(validator.isValid(data));

        data.setPassword("pass");
        data.setName(null);
        assertTrue(validator.isValid(data));

        data.setName("name");
        data.setEmail(null);
        assertTrue(validator.isValid(data));

        data.setPassword(null);
        data.setName(null);
        assertTrue(validator.isValid(data));

        data.setEmail("email@email.com");
        data.setPublic(null);
        assertTrue(validator.isValid(data));

        data.setName("name");
        data.setEmail(null);
        assertTrue(validator.isValid(data));

        data.setPassword("pass");
        data.setName(null);
        assertTrue(validator.isValid(data));

        data.setPassword(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(UpdateUserValidator.ONE_UPDATED_FIELD_REQUIRED));

        data.setEmail("pasunemail");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(UpdateUserValidator.EMAIL_INVALID));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
