package com.wishurgift.entry.validator;

import com.wishurgift.helper.ElementMaker;
import com.wishurgift.service.dto.request.ImageRequestDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import static org.junit.Assert.*;

public class ImageValidatorTest {

    EJBContainer ejbContainer;

    @EJB
    ImageValidator validator;

    @Before
    public void before() throws NamingException {
        ejbContainer = EJBContainer.createEJBContainer();
        ejbContainer.getContext().bind("inject", this);
    }

    @Test
    public void validate() {
        ImageRequestDTO data = ElementMaker.makeImageRequestDTO();
        assertTrue(validator.isValid(data));

        data.setExt(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.EXT_REQUIRED));

        data.setExt("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.EXT_REQUIRED));

        data.setExt("  \t  \n ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.EXT_REQUIRED));

        data.setExt("jpg");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.DATA_INVALID));

        data.setData(null);
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.DATA_REQUIRED));

        data.setData("");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.DATA_REQUIRED));

        data.setData("  \n  \t      ");
        assertFalse(validator.isValid(data));
        assertEquals(1, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.DATA_REQUIRED));

        data.setExt(null);
        assertFalse(validator.isValid(data));
        assertEquals(2, validator.getErrorNumber());
        assertTrue(validator.getAllErrors().contains(ImageValidator.EXT_REQUIRED));
        assertTrue(validator.getAllErrors().contains(ImageValidator.DATA_REQUIRED));
    }

    @After
    public void after() {
        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

}
