package com.wishurgift.helper;

import com.wishurgift.persistence.data.StatsStatus;
import com.wishurgift.persistence.data.StatsWishlist;
import com.wishurgift.service.dto.StatisticDTO;

public class StatisticMaker {

    public static StatsWishlist makeStatsWishlist() {
        StatsWishlist statsWishlist = new StatsWishlist();
        statsWishlist.setAvgPrice(42.8);
        statsWishlist.setMinPrice(12.46);
        statsWishlist.setMaxPrice(180);
        statsWishlist.setNbElement(18);
        statsWishlist.setNbWishlist(4);
        return statsWishlist;
    }

    public static StatsStatus makeStatsStatus() {
        StatsStatus statsStatus = new StatsStatus();
        statsStatus.setNbIdea(8);
        statsStatus.setNbCrush(2);
        statsStatus.setNbReserved(5);
        statsStatus.setNbPurchased(3);
        return statsStatus;
    }

    public static StatisticDTO makeStatisticDTO() {
        return new StatisticDTO(makeStatsWishlist(), makeStatsStatus(), 9);
    }

}
