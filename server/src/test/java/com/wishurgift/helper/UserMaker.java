package com.wishurgift.helper;

import com.wishurgift.persistence.entity.Contact;
import com.wishurgift.persistence.entity.User;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.UserLightDTO;
import com.wishurgift.service.dto.request.LoginRequestDTO;
import com.wishurgift.service.dto.request.RegisterRequestDTO;
import com.wishurgift.service.dto.request.TagRequestDTO;
import com.wishurgift.service.dto.request.UpdateUserRequestDTO;
import com.wishurgift.utils.JwtUtils;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

/**
 * Class d'aide pour générer des objets lié à un utilisateur utilisé par les tests
 */
public class UserMaker {

    /* --- Entité et DTO --- */

    /**
     * Génére un utilisateur
     *
     * @return
     */
    public static User makeUser() {
        User user = new User();
        user.setTag("12345");
        user.setPassword("hashpass");
        user.setName("name");
        user.setEmail("email@email.com");
        user.setPublic(false);
        user.setId(1);
        return user;
    }

    /**
     * Génére un utilisateur DTO
     *
     * @return
     */
    public static UserDTO makeUserDTO() {
        UserDTO user = new UserDTO();
        user.setTag("name#12345");
        user.setName("name");
        user.setEmail("email@email.com");
        user.setPublic(false);
        user.setId(1);
        return user;
    }

    /**
     * Genere un UserLightDTO
     *
     * @return
     */
    public static UserLightDTO makeUserLightDTO() {
        UserLightDTO user = new UserLightDTO();
        user.setTag("name#12345");
        user.setName("name");
        user.setId(1);
        return user;
    }

    /**
     * Création d'un contact entre deux utilisateur
     *
     * @return
     */
    public static Contact makeContact() {
        User requester = makeUser();
        User other = makeUser();
        other.setId(2);
        Contact contact = new Contact();
        contact.setId(1);
        contact.setRequester(requester);
        contact.setContact(other);
        return contact;
    }

    /* --- Request DTO --- */

    /**
     * Genere un LoginRequestDTO
     *
     * @return
     */
    public static LoginRequestDTO makeLoginRequestDTO() {
        LoginRequestDTO login = new LoginRequestDTO();
        login.setLogin("email@email.com");
        login.setPassword("pass");
        return login;
    }

    /**
     * Genere RegisterRequestDTO
     *
     * @return
     */
    public static RegisterRequestDTO makeRegisterRequestDTO() {
        RegisterRequestDTO register = new RegisterRequestDTO();
        register.setEmail("email@email.com");
        register.setPassword("pass");
        register.setName("name");
        return register;
    }

    /**
     * Genere un TagRequestDTO
     *
     * @return
     */
    public static TagRequestDTO makeTagRequestDTO() {
        TagRequestDTO tag = new TagRequestDTO();
        tag.setTag("azerty#24680");
        return tag;
    }

    public static UpdateUserRequestDTO makeUpdateUserRequestDTO() {
        UpdateUserRequestDTO update = new UpdateUserRequestDTO();
        update.setEmail("new.email@email.com");
        update.setName("newname");
        update.setPassword("newpass");
        update.setPublic(true);
        return update;
    }

    /* --- Autre --- */

    /**
     * Genere un contexte de sécurité avec un utilisateur connecté
     *
     * @return
     */
    public static SecurityContext makeSecurityContext() {
        return new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return new Principal() {
                    @Override
                    public String getName() {
                        return JwtUtils.generate(makeUserDTO());
                    }
                };
            }

            @Override
            public boolean isUserInRole(String s) {
                return false;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return null;
            }
        };
    }

    /**
     * Genere un contexte de sécurité invalide
     *
     * @return
     */
    public static SecurityContext makeInvalidSecurityContext() {
        return new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return new Principal() {
                    @Override
                    public String getName() {
                        return "tokenpasterrible";
                    }
                };
            }

            @Override
            public boolean isUserInRole(String s) {
                return false;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return null;
            }
        };
    }

}
