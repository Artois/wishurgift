package com.wishurgift.helper;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.entity.Image;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.service.dto.ImageDTO;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import com.wishurgift.service.dto.request.ImageRequestDTO;
import com.wishurgift.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class ElementMaker {

    /* --- Entité et DTO --- */

    public static Element makeElement() {
        Element entity = new Element();
        entity.setId(1);
        entity.setName("element");
        entity.setLink("http://link.com");
        entity.setDescription("description");
        entity.setAddress("1 rue des rues");
        entity.setPrice(12.4);
        entity.setStatus(Constant.Status.IDEA.getName());
        entity.setBuyer(null);

        List<Image> images = new ArrayList<>();
        images.add(makeImage());
        images.get(0).setElement(entity);
        images.add(makeImage());
        images.get(1).setId(2);
        images.get(1).setElement(entity);
        entity.setImages(images);

        return entity;
    }

    public static Image makeImage() {
        Image entity = new Image();
        entity.setId(1);
        entity.setExt("png");
        entity.setData("data:image/png;base64,jesuisuneimageencodeenbase64==");
        return entity;
    }

    public static ElementDTO makeElementDTO() {
        List<ImageDTO> images = new ArrayList<>();
        images.add(makeImageDTO());
        images.add(makeImageDTO());
        images.get(1).setId(2);

        ElementDTO dto = new ElementDTO();
        dto.setId(1);
        dto.setName("element");
        dto.setLink("http://link.com");
        dto.setDescription("description");
        dto.setAddress("1 rue des rues");
        dto.setPrice(12.4);
        dto.setStatus(Constant.Status.IDEA.getName());
        dto.setImage(images);
        return dto;
    }

    public static ImageDTO makeImageDTO() {
        ImageDTO dto = new ImageDTO();
        dto.setId(1);
        dto.setExt("png");
        dto.setData("data:image/png;base64,jesuisuneimageencodeenbase64==");
        return dto;
    }

    /* --- Request DTO --- */

    public static ElementRequestDTO makeElementRequestDTO() {
        List<ImageRequestDTO> images = new ArrayList<>();
        images.add(makeImageRequestDTO());
        images.add(makeImageRequestDTO());
        images.get(1).setData("data:image/png;base64,jesuisuneautreimageencodeenbase64==");

        ElementRequestDTO dto = new ElementRequestDTO();
        dto.setName("elt");
        dto.setLink("http://link.net");
        dto.setDescription("desc");
        dto.setAddress("1 rue des rues");
        dto.setPrice(12.8);
        dto.setImage(images);
        return dto;
    }

    public static ImageRequestDTO makeImageRequestDTO() {
        ImageRequestDTO dto = new ImageRequestDTO();
        dto.setExt("png");
        dto.setData("data:image/png;base64,jesuisuneimageencodeenbase64==");
        return dto;
    }

}
