package com.wishurgift.helper;

import com.wishurgift.service.dto.ErrorDTO;
import com.wishurgift.wrapper.Result;
import org.junit.Assert;

import javax.ws.rs.core.Response;

public class ControllerHelper {

    public static Object extractEntity(Response response) {
        return extractEntity(response, Object.class);
    }

    public static <T> T extractEntity(Response response, Class<T> clazz) {
        Object entity = response.getEntity();
        return clazz.cast(entity);
    }

    public static <T> T extractSuccessResult(Result<T> result) {
        Assert.assertTrue(result.isSuccess());
        return result.getData();
    }

    public static ErrorDTO extractErrorResult(Result<ErrorDTO> result) {
        Assert.assertFalse(result.isSuccess());
        return result.getData();
    }

    public static <T> T extractSuccessResult(Response response, Class<T> clazz) {
        Result<T> result = (Result<T>) extractEntity(response, Result.class);
        return extractSuccessResult(result);
    }

    public static ErrorDTO extractErrorResult(Response response) {
        Result<ErrorDTO> result = (Result<ErrorDTO>) extractEntity(response, Result.class);
        return extractErrorResult(result);
    }

    public static void successResult(Response response) {
        Result result = extractEntity(response, Result.class);
        Assert.assertTrue(result.isSuccess());
    }

}
