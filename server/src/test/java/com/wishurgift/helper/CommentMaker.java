package com.wishurgift.helper;

import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.service.dto.CommentDTO;
import com.wishurgift.service.dto.request.CommentRequestDTO;

import java.util.Date;

public class CommentMaker {

    public static Comment makeComment() {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setText("comment");
        comment.setDate(new Date());
        comment.setUser(UserMaker.makeUser());
        comment.setElement(WishlistMaker.makeWishlist().getElements().get(0));
        return comment;
    }

    public static CommentDTO makeCommentDTO() {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(1);
        commentDTO.setText("comment");
        commentDTO.setDate(new Date());
        commentDTO.setUser(UserMaker.makeUserLightDTO());
        return commentDTO;
    }

    public static CommentRequestDTO makeCommentRequestDTO() {
        CommentRequestDTO commentRequestDTO = new CommentRequestDTO();
        commentRequestDTO.setText("comment");
        return commentRequestDTO;
    }

}
