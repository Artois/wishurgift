package com.wishurgift.helper;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.entity.Shared;
import com.wishurgift.persistence.entity.Wishlist;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.service.dto.WishlistDTO;
import com.wishurgift.service.dto.WishlistHeaderDTO;
import com.wishurgift.service.dto.WishlistSizeDTO;
import com.wishurgift.service.dto.request.WishlistRequestDTO;
import com.wishurgift.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class WishlistMaker {

    /* --- Entité et DTO --- */

    public static Wishlist makeWishlist() {
        Wishlist entity = new Wishlist();
        entity.setId(1);
        entity.setName("name");
        entity.setPublic(false);
        entity.setUser(UserMaker.makeUser());

        List<Element> elements = new ArrayList<>();
        Element element = ElementMaker.makeElement();
        element.setWishlist(entity);
        elements.add(element);
        element = ElementMaker.makeElement();
        element.setId(2);
        element.setStatus(Constant.Status.PURCHASED.getName());
        element.setWishlist(entity);
        elements.add(element);
        entity.setElements(elements);

        return entity;
    }

    public static Shared makeShared() {
        Shared shared = new Shared();
        shared.setUser(UserMaker.makeUser());
        shared.setWishlist(makeWishlist());
        return shared;
    }

    public static WishlistSizeDTO makeWishlistSizeDTO() {
        WishlistSizeDTO dto = new WishlistSizeDTO();
        dto.setTotal(2);
        dto.setUnbuy(1);
        return dto;
    }

    public static WishlistHeaderDTO makeWishlistHeaderDTO() {
        WishlistHeaderDTO dto = new WishlistHeaderDTO();
        dto.setId(1);
        dto.setName("name");
        dto.setPublic(false);
        dto.setSize(makeWishlistSizeDTO());
        return dto;
    }

    public static WishlistDTO makeWishlistDTO() {
        WishlistDTO dto = new WishlistDTO();
        dto.setId(1);
        dto.setName("name");
        dto.setPublic(false);
        dto.setSize(makeWishlistSizeDTO());

        List<ElementDTO> elements = new ArrayList<>();
        elements.add(ElementMaker.makeElementDTO());
        ElementDTO element = ElementMaker.makeElementDTO();
        element.setId(2);
        element.setStatus(Constant.Status.PURCHASED.getName());
        elements.add(element);
        dto.setElement(elements);

        return dto;
    }

    /* --- Request DTO --- */

    public static WishlistRequestDTO makeWishlistRequestDTO() {
        WishlistRequestDTO request = new WishlistRequestDTO();
        request.setName("name");
        request.setPublic(false);
        return request;
    }

    /* --- Autre --- */

}
