package com.wishurgift.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {

    @Test
    public void isNullTest() {
        assertTrue(StringUtils.isNull(null));
        assertFalse(StringUtils.isNull(""));
        assertFalse(StringUtils.isNull(" "));
        assertFalse(StringUtils.isNull("\t"));
        assertFalse(StringUtils.isNull("\n"));
        assertFalse(StringUtils.isNull("\t  \n "));
        assertFalse(StringUtils.isNull("Bonjour"));
    }

    @Test
    public void isEmptyTest() {
        assertTrue(StringUtils.isEmpty(null));
        assertTrue(StringUtils.isEmpty(""));
        assertFalse(StringUtils.isEmpty(" "));
        assertFalse(StringUtils.isEmpty("\t"));
        assertFalse(StringUtils.isEmpty("\n"));
        assertFalse(StringUtils.isEmpty("\t  \n "));
        assertFalse(StringUtils.isEmpty("Bonjour"));
    }

    @Test
    public void isBlankTest() {
        assertTrue(StringUtils.isBlank(null));
        assertTrue(StringUtils.isBlank(""));
        assertTrue(StringUtils.isBlank(" "));
        assertTrue(StringUtils.isBlank("\t"));
        assertTrue(StringUtils.isBlank("\n"));
        assertTrue(StringUtils.isBlank("\t  \n "));
        assertFalse(StringUtils.isBlank("Bonjour"));
    }

    @Test
    public void isNotNullTest() {
        assertFalse(StringUtils.isNotNull(null));
        assertTrue(StringUtils.isNotNull(""));
        assertTrue(StringUtils.isNotNull(" "));
        assertTrue(StringUtils.isNotNull("\t"));
        assertTrue(StringUtils.isNotNull("\n"));
        assertTrue(StringUtils.isNotNull("\t  \n "));
        assertTrue(StringUtils.isNotNull("Bonjour"));
    }

    @Test
    public void isNotEmptyTest() {
        assertFalse(StringUtils.isNotEmpty(null));
        assertFalse(StringUtils.isNotEmpty(""));
        assertTrue(StringUtils.isNotEmpty(" "));
        assertTrue(StringUtils.isNotEmpty("\t"));
        assertTrue(StringUtils.isNotEmpty("\n"));
        assertTrue(StringUtils.isNotEmpty("\t  \n "));
        assertTrue(StringUtils.isNotEmpty("Bonjour"));
    }

    @Test
    public void isNotBlankTest() {
        assertFalse(StringUtils.isNotBlank(null));
        assertFalse(StringUtils.isNotBlank(""));
        assertFalse(StringUtils.isNotBlank(" "));
        assertFalse(StringUtils.isNotBlank("\t"));
        assertFalse(StringUtils.isNotBlank("\n"));
        assertFalse(StringUtils.isNotBlank("\t  \n "));
        assertTrue(StringUtils.isNotBlank("Bonjour"));
    }

    @Test
    public void capitalizeTest() {
        assertEquals("Test", StringUtils.capitalize("test"));
        assertEquals("Test", StringUtils.capitalize("TEST"));
        assertEquals("Test", StringUtils.capitalize("TeSt"));
        assertEquals("", StringUtils.capitalize(""));
        assertEquals("A", StringUtils.capitalize("a"));
        assertEquals("A", StringUtils.capitalize("A"));
        assertNull(StringUtils.capitalize(null));
    }

    @Test
    public void lowerTest() {
        assertEquals("test", StringUtils.lower("test"));
        assertEquals("test", StringUtils.lower("TEST"));
        assertEquals("test", StringUtils.lower("TeSt"));
        assertEquals("", StringUtils.lower(""));
        assertNull(StringUtils.capitalize(null));
    }

}
