package com.wishurgift.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordUtilsTest {

    @Test
    public void hashTest() {
        String pass1 = "jesuisunmotdepasse";
        String result1 = PasswordUtils.hash(pass1);
        assertNotEquals(pass1, result1);

        String pass2 = "jesuisunemotdepasse";
        String result2 = PasswordUtils.hash(pass2);
        assertNotEquals(pass2, result2);

        assertNotEquals(result1, result2);
        assertNotEquals(PasswordUtils.hash(pass1), PasswordUtils.hash(pass2));
    }

    @Test
    public void verifyTest() {
        String goodPass = "jesuislebonmotdepasse";
        String badPass = "jesuislemauvaismotdepasse";
        String badGoodPass = "jesuislebonmotdepassf";
        String hash = PasswordUtils.hash(goodPass);

        assertTrue(PasswordUtils.verify(goodPass, hash));

        assertFalse(PasswordUtils.verify(badPass, hash));
        assertFalse(PasswordUtils.verify(badGoodPass, hash));
        assertFalse(PasswordUtils.verify("\t \n ", hash));
        assertFalse(PasswordUtils.verify(null, hash));

        assertFalse(PasswordUtils.verify(goodPass, "hashpasbon"));
        assertFalse(PasswordUtils.verify(goodPass, "\n \t "));
        assertFalse(PasswordUtils.verify(goodPass, null));
    }

    @Test
    public void longPasswordTest() {
        String longPass = "azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbn";
        String otherLongPass = "azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxdiff";
        assertTrue(longPass.getBytes().length > 72);
        assertTrue(otherLongPass.getBytes().length > 72);

        String hash = PasswordUtils.hash(longPass);
        String otherHash = PasswordUtils.hash(otherLongPass);
        assertNotEquals(hash, otherHash);
        assertTrue(PasswordUtils.verify(longPass, hash));
        assertTrue(PasswordUtils.verify(otherLongPass, otherHash));
        assertFalse(PasswordUtils.verify(longPass, otherHash));
        assertFalse(PasswordUtils.verify(otherLongPass, hash));
    }

}
