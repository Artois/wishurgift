package com.wishurgift.service.dto.transformer;

import com.wishurgift.helper.ElementMaker;
import com.wishurgift.persistence.entity.Element;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.utils.Constant;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ElementTransformerTest {

    @Test
    public void entityToDtoTest() {
        Element expected = ElementMaker.makeElement();

        ElementDTO actual = ElementTransformer.entityToDto(expected);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getLink(), actual.getLink());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getAddress(), actual.getAddress());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getPrice(), actual.getPrice(), 0);
        assertEquals(ImageTransformer.entityToDto(expected.getImages()), actual.getImage());

        assertNull(ElementTransformer.entityToDto((Element) null));
    }

    @Test
    public void listEntityToDtoTest() {
        List<Element> expected = new ArrayList<>();
        expected.add(ElementMaker.makeElement());
        Element element = ElementMaker.makeElement();
        element.setId(2);
        element.setStatus(Constant.Status.RESERVED.getName());
        element.setPrice(8.8);
        expected.add(element);
        element = ElementMaker.makeElement();
        element.setId(3);
        element.setDescription("je ne suis pas vide");
        element.getImages().remove(0);
        expected.add(element);

        List<ElementDTO> actual = ElementTransformer.entityToDto(expected);
        assertEquals(expected.size(), actual.size());
        actual.forEach(dto -> {
            Element entity = expected.get((int) dto.getId() - 1);
            assertEquals(entity.getId(), dto.getId());
            assertEquals(entity.getName(), dto.getName());
            assertEquals(entity.getLink(), dto.getLink());
            assertEquals(entity.getDescription(), dto.getDescription());
            assertEquals(entity.getAddress(), dto.getAddress());
            assertEquals(entity.getStatus(), dto.getStatus());
            assertEquals(entity.getPrice(), dto.getPrice(), 0);
            assertEquals(ImageTransformer.entityToDto(entity.getImages()), dto.getImage());
        });

        actual = ElementTransformer.entityToDto(Collections.emptyList());
        assertTrue(actual.isEmpty());

        assertNull(ElementTransformer.entityToDto((List<Element>) null));
    }

}
