package com.wishurgift.service.dto.transformer;

import com.wishurgift.helper.ElementMaker;
import com.wishurgift.persistence.entity.Image;
import com.wishurgift.service.dto.ImageDTO;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ImageTransformerTest {

    @Test
    public void entityToDtoTest() {
        Image expected = ElementMaker.makeImage();

        ImageDTO actual = ImageTransformer.entityToDto(expected);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getExt(), actual.getExt());
        assertEquals(expected.getData(), actual.getData());

        assertNull(ImageTransformer.entityToDto((Image) null));
    }

    @Test
    public void listEntityToDtoTest() {
        List<Image> expected = new ArrayList<>();
        expected.add(ElementMaker.makeImage());
        Image image = ElementMaker.makeImage();
        image.setId(2);
        expected.add(image);
        image = ElementMaker.makeImage();
        image.setId(3);
        image.setExt("jpeg");
        image.setData("data:image/jpeg;base64,jesuisuneautreimageencodeenbase64==");
        expected.add(image);

        List<ImageDTO> actual = ImageTransformer.entityToDto(expected);
        assertEquals(expected.size(), actual.size());
        actual.forEach(dto -> {
            Image entity = expected.get((int) dto.getId() - 1);
            assertEquals(entity.getId(), dto.getId());
            assertEquals(entity.getExt(), dto.getExt());
            assertEquals(entity.getData(), dto.getData());
        });

        actual = ImageTransformer.entityToDto(Collections.emptyList());
        assertTrue(actual.isEmpty());

        assertNull(ImageTransformer.entityToDto((List<Image>) null));
    }

}
