package com.wishurgift.service.dto.transformer;

import com.wishurgift.helper.CommentMaker;
import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.service.dto.CommentDTO;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class CommentTransformerTest {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void entityToDtoTest() {
        Comment expected = CommentMaker.makeComment();

        CommentDTO actual = CommentTransformer.entityToDto(expected);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getText(), actual.getText());
        assertEquals(simpleDateFormat.format(expected.getDate()), actual.getDate());
        assertEquals(UserTransformer.entityToLightDto(expected.getUser()), actual.getUser());

        assertNull(CommentTransformer.entityToDto((Comment) null));
    }

    @Test
    public void listEntityToDtoTest() {
        List<Comment> expected = new ArrayList<>();
        expected.add(CommentMaker.makeComment());
        expected.add(CommentMaker.makeComment());
        expected.get(1).setId(2);
        expected.get(1).setText("text");

        List<CommentDTO> actual = CommentTransformer.entityToDto(expected);
        assertEquals(expected.size(), actual.size());
        actual.forEach(dto -> {
            Comment entity = expected.get((int) dto.getId() - 1);
            assertEquals(entity.getId(), dto.getId());
            assertEquals(entity.getText(), dto.getText());
            assertEquals(simpleDateFormat.format(entity.getDate()), dto.getDate());
            assertEquals(UserTransformer.entityToLightDto(entity.getUser()), dto.getUser());
        });

        actual = CommentTransformer.entityToDto(Collections.emptyList());
        assertTrue(actual.isEmpty());

        assertNull(CommentTransformer.entityToDto((List<Comment>) null));
    }

}
