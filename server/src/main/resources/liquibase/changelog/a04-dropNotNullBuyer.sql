--liquibase formatted sql
--changeset abrandao:a04

ALTER TABLE elements
    ALTER COLUMN us_id DROP NOT NULL;