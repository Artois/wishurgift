--liquibase formatted sql
--changeset abrandao:02

INSERT INTO users (name, email, password, public, tag)
VALUES ('Vipewesh', 'vip@wesh.com', '$2b$08$wI8eU9yPMsuKrhMJisbhauDXT.ya7nX1YGeCsMjxOnw7ervj6Y8ty', false, '50617');

INSERT INTO users (name, email, password, public, tag)
VALUES ('Emonga', 'emonga@gmail.co.jp', '$2b$08$XXJ7SpZgex0hTlFyBhiQQOl5tP.Edrkx5REEcW76p95ukFE0cCQya', false, '10832');