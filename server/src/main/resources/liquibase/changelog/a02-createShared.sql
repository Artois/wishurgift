--liquibase formatted sql
--changeset abrandao:a02

CREATE TABLE SHARED
(
    sh_id SERIAL,
    us_id SERIAL,
    wi_id SERIAL,
    PRIMARY KEY (sh_id)
);

ALTER TABLE SHARED
    ADD FOREIGN KEY (us_id) REFERENCES USERS (us_id);
ALTER TABLE SHARED
    ADD FOREIGN KEY (wi_id) REFERENCES WISHLISTS (wi_id);