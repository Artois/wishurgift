--liquibase formatted sql
--changeset abrandao:a05

ALTER TABLE elements
    DROP COLUMN latitude;

ALTER TABLE elements
    DROP COLUMN longitude;

ALTER TABLE elements
    ADD COLUMN address TEXT;