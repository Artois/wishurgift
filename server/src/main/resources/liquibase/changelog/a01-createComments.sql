--liquibase formatted sql
--changeset abrandao:a01

CREATE TABLE COMMENTS
(
    co_id SERIAL,
    text  TEXT,
    date  DATE,
    us_id SERIAL,
    el_id SERIAL,
    PRIMARY KEY (co_id)
);

ALTER TABLE COMMENTS
    ADD FOREIGN KEY (us_id) REFERENCES USERS (us_id);
ALTER TABLE COMMENTS
    ADD FOREIGN KEY (el_id) REFERENCES ELEMENTS (el_id);