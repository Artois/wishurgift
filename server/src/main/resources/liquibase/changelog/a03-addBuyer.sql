--liquibase formatted sql
--changeset abrandao:a03

ALTER TABLE ELEMENTS
    ADD COLUMN us_id SERIAL;

ALTER TABLE ELEMENTS
    ADD FOREIGN KEY (us_id) REFERENCES USERS (us_id);