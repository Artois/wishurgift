--liquibase formatted sql
--changeset abrandao:01

CREATE TABLE WISHLISTS (
  wi_id SERIAL,
  name VARCHAR(500),
  public BOOLEAN,
  us_id SERIAL,
  PRIMARY KEY (wi_id)
);

CREATE TABLE USERS (
  us_id SERIAL,
  name VARCHAR(500),
  email VARCHAR(500),
  password VARCHAR(255),
  public BOOLEAN,
  tag VARCHAR(5),
  PRIMARY KEY (us_id)
);

CREATE TABLE CONTACT (
  co_id SERIAL,
  us_id SERIAL,
  us_id_demandeur SERIAL,
  PRIMARY KEY (co_id)
);

CREATE TABLE ELEMENTS (
  el_id SERIAL,
  status VARCHAR(100),
  name VARCHAR(500),
  link VARCHAR(500),
  latitude VARCHAR(100),
  longitude VARCHAR(100),
  price REAL,
  description TEXT,
  wi_id SERIAL,
  PRIMARY KEY (el_id)
);

CREATE TABLE IMAGES (
  im_id SERIAL,
  ext VARCHAR(5),
  data TEXT,
  el_id SERIAL,
  PRIMARY KEY (im_id)
);

ALTER TABLE WISHLISTS ADD FOREIGN KEY (us_id) REFERENCES USERS (us_id);
ALTER TABLE CONTACT ADD FOREIGN KEY (us_id_demandeur) REFERENCES USERS (us_id);
ALTER TABLE CONTACT ADD FOREIGN KEY (us_id) REFERENCES USERS (us_id);
ALTER TABLE ELEMENTS ADD FOREIGN KEY (wi_id) REFERENCES WISHLISTS (wi_id);
ALTER TABLE IMAGES ADD FOREIGN KEY (el_id) REFERENCES ELEMENTS (el_id);