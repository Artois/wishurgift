package com.wishurgift.service;

import com.wishurgift.service.dto.LoginDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.WishlistHeaderDTO;
import com.wishurgift.service.dto.request.LoginRequestDTO;
import com.wishurgift.service.dto.request.RegisterRequestDTO;
import com.wishurgift.service.dto.request.UpdateUserRequestDTO;
import com.wishurgift.wrapper.Page;
import com.wishurgift.wrapper.Result;

import java.security.Principal;

public interface UserService {

    /**
     * Met à jour un utilisateur
     *
     * @param user    L'utilisateur à mettre à jour
     * @param request La requete de mise à jour
     * @return L'utilisateur avec les données mise à jours, ou null si l'email à modifier est deja utilisé ou si l'utilisateur à modifier est introuvable
     */
    public UserDTO update(UserDTO user, UpdateUserRequestDTO request);

    /**
     * Recupere un utilisateur par son id
     *
     * @param id
     * @return
     */
    public UserDTO getById(long id);

    /**
     * Récupère un utilisateur en fonction de l'utilisateur connecté
     * Récupère un utilisateur si son profil est publique, qu'il est amis avec l'utilisateur connecté ou que ce soit
     * lui meme.
     *
     * @param principal L'utilisateur connecté (peut etre null si aucun utilisateur n'est connecté)
     * @param id        L'id de l'utilisateur a récupèrer
     * @return L'utilisateur ou null si ruen n'est trouvé
     */
    public Result getUser(UserDTO principal, long id);

    /**
     * Récupère les listes d'un utilisateur
     *
     * @param principal L'utilisateur connecté (peut etre null si aucun utilisateur n'est connecté)
     * @param id        L'id de l'utilisateur dont on souhaite récupèrer les listes
     * @return
     */
    public Page<WishlistHeaderDTO> getWishlist(UserDTO principal, long id);

    /**
     * Récupère une liste pagniée des listes d'un utilisateur
     *
     * @param principal L'utilisateur connecté (peut etre null si aucun utilisateur n'est connecté)
     * @param id        L'id de l'utilisateur dont on souhaite récupèrer les listes
     * @return
     */
    public Page<WishlistHeaderDTO> getWishlist(int page, int size, UserDTO principal, long id);

    /**
     * Connexion d'un utilisateur
     *
     * @param login L'email et le password de l'utilisateur
     * @return
     */
    public LoginDTO login(LoginRequestDTO login);

    /**
     * Création d'un nouveau token pour l'utilisateur principal
     *
     * @param principal L'utilisateur
     * @return Le nouveau token
     */
    public String refresh(Principal principal);

    /**
     * Inscrit un utilisateur
     *
     * @param register Les données pour l'inscription
     * @return L'utilisateur inscrit ou null si il est déjà inscrit
     */
    public UserDTO register(RegisterRequestDTO register);

    /**
     * Recherche des utilisateurs par leur email, leur nom ou leur tag
     *
     * @param request La recherche
     * @param limit   Le nombre maximum de résultat (0 = pas de limite)
     * @return Une liste d'utilisateur correspondant à la recherche
     */
    public Page<UserDTO> search(String request, int limit);

    /**
     * Récupère toutes les listes partagées avec un utilisateur
     *
     * @param principal L'utilisateur
     * @return
     */
    public Page<WishlistHeaderDTO> getShared(UserDTO principal);

    /**
     * Récupère une liste pagniée de toutes les listes partagées avec un utilisateur
     *
     * @param principal L'utilisateur
     * @return
     */
    public Page<WishlistHeaderDTO> getShared(int page, int size, UserDTO principal);

}
