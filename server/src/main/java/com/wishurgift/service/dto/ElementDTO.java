package com.wishurgift.service.dto;

import com.wishurgift.utils.Constant;
import lombok.Data;
import org.decimal4j.util.DoubleRounder;

import java.util.Collections;
import java.util.List;

@Data
public class ElementDTO {

    private long id;

    private Constant.Status status;

    private String name;

    private String link = "";

    private List<ImageDTO> image = Collections.emptyList();

    private String address = "";

    private double price;

    private String description = "";

    public void setLink(String link) {
        this.link = link == null ? "" : link;
    }

    public String getStatus() {
        return status.getName();
    }

    public void setStatus(String status) {
        this.status = Constant.Status.fromName(status);
    }

    public void setImage(List<ImageDTO> image) {
        this.image = image == null ? Collections.emptyList() : image;
    }

    public void setAddress(String address) {
        this.address = address == null ? "" : address;
    }

    public void setPrice(double price) {
        this.price = DoubleRounder.round(price, 2);
    }

    public void setDescription(String description) {
        this.description = description == null ? "" : description;
    }
}
