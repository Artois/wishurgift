package com.wishurgift.service.dto;

import lombok.Data;

@Data
public class ImageDTO {

    private long id;

    private String ext;

    private String data;

}
