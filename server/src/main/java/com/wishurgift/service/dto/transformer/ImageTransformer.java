package com.wishurgift.service.dto.transformer;

import com.wishurgift.persistence.entity.Image;
import com.wishurgift.service.dto.ImageDTO;

import java.util.ArrayList;
import java.util.List;

public class ImageTransformer {

    public static ImageDTO entityToDto(Image entity) {
        if (entity == null) {
            return null;
        }

        ImageDTO dto = new ImageDTO();
        dto.setId(entity.getId());
        dto.setExt(entity.getExt());
        dto.setData(entity.getData());
        return dto;
    }

    public static List<ImageDTO> entityToDto(List<Image> entity) {
        if (entity == null) {
            return null;
        }

        List<ImageDTO> dto = new ArrayList<>();
        entity.forEach(elt -> dto.add(entityToDto(elt)));
        return dto;
    }

}
