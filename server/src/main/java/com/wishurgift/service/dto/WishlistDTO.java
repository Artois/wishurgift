package com.wishurgift.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class WishlistDTO extends WishlistHeaderDTO {

    private List<ElementDTO> element;

    private List<UserLightDTO> shared;

}
