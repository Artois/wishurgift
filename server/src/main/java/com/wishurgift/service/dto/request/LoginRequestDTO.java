package com.wishurgift.service.dto.request;

import lombok.Data;

@Data
public class LoginRequestDTO {

    private String login;
    
    private String password;
}
