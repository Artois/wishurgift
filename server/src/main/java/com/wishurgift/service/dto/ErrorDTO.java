package com.wishurgift.service.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorDTO {

    private int code;

    private String message;

    private List<String> trace = new ArrayList<>();

    public void setTrace(List<String> trace) {
        this.trace = new ArrayList<>(trace);
    }

}
