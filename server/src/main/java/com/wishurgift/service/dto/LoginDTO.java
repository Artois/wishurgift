package com.wishurgift.service.dto;

import lombok.Data;

@Data
public class LoginDTO {

    private String token;

    private UserDTO user;

    public LoginDTO(String token) {
        this(token, null);
    }

    public LoginDTO(String token, UserDTO user) {
        this.token = token;
        this.user = user;
    }
}
