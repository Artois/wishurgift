package com.wishurgift.service.dto.request;

import lombok.Data;

@Data
public class CommentRequestDTO {

    private String text;

}
