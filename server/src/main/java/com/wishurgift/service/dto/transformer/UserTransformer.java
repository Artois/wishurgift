package com.wishurgift.service.dto.transformer;

import com.wishurgift.persistence.entity.User;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.UserLightDTO;
import com.wishurgift.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class UserTransformer {

    public static UserDTO entityToDto(User entity) {
        if (entity == null) {
            return null;
        }

        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(StringUtils.capitalize(entity.getName()));
        dto.setPublic(entity.isPublic());
        dto.setTag(convertTag(entity.getName(), entity.getTag()));

        return dto;
    }

    public static List<UserDTO> entityToDto(List<User> entity) {
        if (entity == null) {
            return null;
        }

        List<UserDTO> dto = new ArrayList<>();
        entity.forEach(elt -> dto.add(entityToDto(elt)));
        return dto;
    }

    public static User dtoToEntity(UserDTO dto) {
        if (dto == null) {
            return null;
        }

        User entity = new User();
        entity.setId(dto.getId());
        entity.setEmail(dto.getEmail());
        entity.setName(dto.getName().toLowerCase());
        entity.setPublic(dto.isPublic());
        entity.setTag(convertTag(dto.getTag()));

        return entity;
    }

    public static List<User> dtoToEntity(List<UserDTO> dto) {
        if (dto == null) {
            return null;
        }

        List<User> entity = new ArrayList<>();
        dto.forEach(elt -> entity.add(dtoToEntity(elt)));
        return entity;
    }

    public static UserLightDTO entityToLightDto(User entity) {
        if (entity == null) {
            return null;
        }

        UserLightDTO dto = new UserLightDTO();
        dto.setId(entity.getId());
        dto.setName(StringUtils.capitalize(entity.getName()));
        dto.setTag(convertTag(entity.getName(), entity.getTag()));
        return dto;
    }

    public static List<UserLightDTO> entityToLightDto(List<User> entity) {
        if (entity == null) {
            return null;
        }

        List<UserLightDTO> dto = new ArrayList<>();
        entity.forEach(elt -> dto.add(entityToLightDto(elt)));
        return dto;
    }

    public static String convertTag(String name, String tag) {
        if (name == null || tag == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(name.toLowerCase().replaceAll(" ", "_").replaceAll("#", ""))
                .append("#")
                .append(tag);
        return builder.toString();
    }

    public static String convertTag(String dtoTag) {
        if (dtoTag == null) {
            return null;
        }
        return dtoTag.split("#")[1];
    }

}
