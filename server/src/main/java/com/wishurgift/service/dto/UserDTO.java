package com.wishurgift.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wishurgift.utils.MD5Utils;
import lombok.Data;

@Data
public class UserDTO {

    private long id;

    private String name;

    private String email;

    private String avatar;

    @JsonProperty("public")
    private boolean isPublic;

    private String tag;

    public void setEmail(String email) {
        this.email = email;
        if (email == null) {
            this.avatar = null;
        } else {
            this.avatar = "https://www.gravatar.com/avatar/" + MD5Utils.hash(email) + "?d=identicon";
        }
    }
}
