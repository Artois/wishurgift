package com.wishurgift.service.dto.request;

import lombok.Data;

@Data
public class TagRequestDTO {

    private String tag;

}
