package com.wishurgift.service.dto;

import lombok.Data;

@Data
public class WishlistHeaderDTO {

    private long id;

    private String name;

    private boolean isPublic;

    private WishlistSizeDTO size;

    private UserLightDTO user;

}
