package com.wishurgift.service.dto.transformer;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.service.dto.ElementDTO;

import java.util.ArrayList;
import java.util.List;

public class ElementTransformer {

    public static ElementDTO entityToDto(Element entity) {
        if (entity == null) {
            return null;
        }

        ElementDTO dto = new ElementDTO();
        dto.setId(entity.getId());
        dto.setStatus(entity.getStatus());
        dto.setName(entity.getName());
        dto.setLink(entity.getLink());
        dto.setAddress(entity.getAddress());
        dto.setDescription(entity.getDescription());
        dto.setPrice(entity.getPrice());
        dto.setImage(ImageTransformer.entityToDto(entity.getImages()));
        return dto;
    }

    public static List<ElementDTO> entityToDto(List<Element> entity) {
        if (entity == null) {
            return null;
        }

        List<ElementDTO> dto = new ArrayList<>();
        entity.forEach(elt -> dto.add(entityToDto(elt)));
        return dto;
    }

}
