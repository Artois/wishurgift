package com.wishurgift.service.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class ElementRequestDTO {

    private String name;

    private String link;

    private List<ImageRequestDTO> image;

    private String address;

    private Double price;

    private String description;

}
