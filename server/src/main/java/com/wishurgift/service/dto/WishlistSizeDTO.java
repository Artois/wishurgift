package com.wishurgift.service.dto;

import lombok.Data;

@Data
public class WishlistSizeDTO {

    private long total;

    private long unbuy;

}
