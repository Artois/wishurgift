package com.wishurgift.service.dto;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class CommentDTO {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private long id;

    private String text;

    private String date;

    private UserLightDTO user;

    public void setDate(Date date) {
        this.date = simpleDateFormat.format(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

}
