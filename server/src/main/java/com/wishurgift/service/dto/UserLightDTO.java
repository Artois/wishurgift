package com.wishurgift.service.dto;

import lombok.Data;

@Data
public class UserLightDTO {

    private long id;

    private String name;

    private String tag;

}
