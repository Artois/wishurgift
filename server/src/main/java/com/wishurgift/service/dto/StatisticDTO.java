package com.wishurgift.service.dto;

import com.wishurgift.persistence.data.StatsStatus;
import com.wishurgift.persistence.data.StatsWishlist;
import lombok.Data;
import org.decimal4j.util.DoubleRounder;

@Data
public class StatisticDTO {

    private long wishlist;

    private long element;

    private long contact;

    private long idea;

    private long crush;

    private long reserved;

    private long purchased;

    private double averagePrice;

    private double lowestPrice;

    private double highestPrice;

    public StatisticDTO() {
        // Empty constructor POJO
    }

    public StatisticDTO(StatsWishlist statsWishlist, StatsStatus statsStatus, long nbContact) {
        this.wishlist = statsWishlist.getNbWishlist();
        this.element = statsWishlist.getNbElement();
        this.contact = nbContact;
        this.idea = statsStatus.getNbIdea();
        this.crush = statsStatus.getNbCrush();
        this.reserved = statsStatus.getNbReserved();
        this.purchased = statsStatus.getNbPurchased();
        this.averagePrice = DoubleRounder.round(statsWishlist.getAvgPrice(), 2);
        this.lowestPrice = statsWishlist.getMinPrice();
        this.highestPrice = statsWishlist.getMaxPrice();
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = DoubleRounder.round(averagePrice, 2);
    }
}
