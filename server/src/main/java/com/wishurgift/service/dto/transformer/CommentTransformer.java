package com.wishurgift.service.dto.transformer;

import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.service.dto.CommentDTO;

import java.util.ArrayList;
import java.util.List;

public class CommentTransformer {

    public static CommentDTO entityToDto(Comment entity) {
        if (entity == null) {
            return null;
        }

        CommentDTO dto = new CommentDTO();
        dto.setDate(entity.getDate());
        dto.setId(entity.getId());
        dto.setText(entity.getText());
        dto.setUser(UserTransformer.entityToLightDto(entity.getUser()));
        return dto;
    }

    public static List<CommentDTO> entityToDto(List<Comment> entity) {
        if (entity == null) {
            return null;
        }
        List<CommentDTO> dto = new ArrayList<>();
        entity.forEach(elt -> dto.add(entityToDto(elt)));

        return dto;
    }
}
