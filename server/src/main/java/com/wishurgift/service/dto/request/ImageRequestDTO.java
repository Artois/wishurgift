package com.wishurgift.service.dto.request;

import lombok.Data;

@Data
public class ImageRequestDTO {

    private String ext;

    private String data;

}
