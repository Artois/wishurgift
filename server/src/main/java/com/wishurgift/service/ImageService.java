package com.wishurgift.service;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.service.dto.ImageDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.request.ImageRequestDTO;

public interface ImageService {

    /**
     * Ajoute une image à un élement
     *
     * @param element L'element de l'image
     * @param request Les infos de l'image à ajouter
     * @return L'image ajoutée
     */
    public ImageDTO add(ElementDTO element, ImageRequestDTO request);

    /**
     * Ajoute une image à un élement
     *
     * @param element L'element de l'image
     * @param request Les infos de l'image à ajouter
     * @return L'image ajoutée
     */
    public ImageDTO add(Element element, ImageRequestDTO request);

    /**
     * Supprime une image
     *
     * @param principal L'utilisateur qui demande la suppression
     * @param id        L'id de l'image
     * @return true si l'image est bien supprimé, false sinon
     */
    public boolean remove(UserDTO principal, long id);

    /**
     * Récupère une image
     *
     * @param principal L'utilisateur qui récupère l'image (peut être null si on ne le connait pas)
     * @param id        L'id de l'image
     * @return L'image ou null en cas d'erreur
     */
    public ImageDTO getImage(UserDTO principal, long id);

}
