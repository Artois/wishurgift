package com.wishurgift.service;

import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.request.TagRequestDTO;
import com.wishurgift.wrapper.Page;

public interface ContactService {

    /**
     * Ajoute une requete de contact
     *
     * @param principal L'utilisateur qui fait la requete
     * @param request   Le tag de l'utilisateur qui reçoit la requete
     * @return L'utilisateur ajouter
     */
    public UserDTO add(UserDTO principal, TagRequestDTO request);

    /**
     * Supprime une requete de contact entre deux utilisateur
     *
     * @param principal
     * @param contact
     */
    public void remove(UserDTO principal, UserDTO contact);

    /**
     * Indique si les deux utilisateurs sont en contact
     *
     * @param principal
     * @param contact
     * @return
     */
    public boolean isContact(UserDTO principal, UserDTO contact);

    /**
     * Récupere tous les utilisateurs qui ont reçu une requete et ne l'ont pas encore acceptée
     *
     * @param principal L'utilisateur qui à envoyé les requetes
     * @return
     */
    public Page<UserDTO> getRequest(UserDTO principal);

    /**
     * Récupere une liste paginée de tous les utilisateurs qui ont reçu une requete et ne l'ont pas encore acceptée
     *
     * @param page      Le numero de la page
     * @param size      La taille de la page (le nombre d'element)
     * @param principal L'utilisateur qui à envoyer les requetes
     * @return
     */
    public Page<UserDTO> getRequest(int page, int size, UserDTO principal);

    /**
     * Récupere tous les utilisateurs qui ont envoyée une requete et qui n'a pas encore été acceptée
     *
     * @param principal L'utilisateur qui à reçu les requetes
     * @return
     */
    public Page<UserDTO> getPending(UserDTO principal);

    /**
     * Récupere une liste paginée de tous les utilisateurs qui ont envoyée une requete et qui n'a pas encore été acceptée
     *
     * @param page      Le numero de la page
     * @param size      La taille de la page (le nombre d'element)
     * @param principal L'utilisateur qui à reçu les requetes
     * @return
     */
    public Page<UserDTO> getPending(int page, int size, UserDTO principal);

    /**
     * Récupere tous les utilisateurs qui sont en conatc
     *
     * @param principal L'utilisateur pour la recherche
     * @return
     */
    public Page<UserDTO> getContact(UserDTO principal);

    /**
     * Récupere une liste paginée de tous les utilisateurs qui sont en conatc
     *
     * @param page      Le numero de la page
     * @param size      La taille de la page (le nombre d'element)
     * @param principal L'utilisateur pour la recherche
     * @return
     */
    public Page<UserDTO> getContact(int page, int size, UserDTO principal);

}
