package com.wishurgift.service;

import com.wishurgift.service.dto.CommentDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.request.CommentRequestDTO;
import com.wishurgift.wrapper.Page;

public interface CommentService {


    /**
     * Permet d'obtenir un commentaire par l'id
     *
     * @param principal l'utilisateur connecté
     * @param id        son id
     * @return le commentaire
     */
    public CommentDTO getById(UserDTO principal, long id);

    /**
     * Renvoie les commentaires d'un element sous la forme d'un objet page
     *
     * @param principal l'utilisateur connecté
     * @param idElement l'id de l'element
     * @return les commentaires d'un element sous la forme d'un objet page
     */
    public Page<CommentDTO> getByElement(UserDTO principal, long idElement);

    /**
     * Renvoie les commentaires d'un element sous la forme d'un objet page
     *
     * @param principal l'utilisateur connecté
     * @param page      le nombre de page
     * @param size      le nombre de commentaire par page
     * @param idElement l'id de l'element
     * @return les commentaires d'un element sous la forme d'un objet page
     */
    public Page<CommentDTO> getByElement(UserDTO principal, int page, int size, long idElement);


    /**
     * Permet de modifier un commentaire
     *
     * @param principal l'utilisateur connecté
     * @param id        l'id
     * @param text      le nouveau text
     * @return le commentaire modifié
     */
    public CommentDTO put(UserDTO principal, long id, CommentRequestDTO text);

    /**
     * Permet de supprimer un commentaire
     *
     * @param principal l'utilisateur connecté
     * @param id        l'id
     * @return true if ok else false
     */
    public boolean delete(UserDTO principal, long id);

    /**
     * Permet d'inserer un commentaire
     *
     * @param principal l'utilisateur qui crée le commentaire
     * @param id_elt    l'id de l'element
     * @param request   le commentaire
     * @return le commentaire inserer
     */
    public CommentDTO insert(UserDTO principal, long id_elt, CommentRequestDTO request);
}
