package com.wishurgift.service;

import com.wishurgift.service.dto.StatisticDTO;
import com.wishurgift.service.dto.UserDTO;

public interface StatisticService {

    /**
     * Récupère des statistiques d'un utilisateur par un utilisateur
     *
     * @param principal L'utilisateur qui veut récupèrer les données
     * @param id        L'id de l'utilisateur
     * @return Les statistiques ou null en cas d'erreur
     */
    public StatisticDTO getStatistic(UserDTO principal, long id);

}
