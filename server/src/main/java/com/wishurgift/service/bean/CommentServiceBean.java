package com.wishurgift.service.bean;

import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.entity.User;
import com.wishurgift.persistence.repository.CommentRepository;
import com.wishurgift.persistence.repository.ElementRepository;
import com.wishurgift.persistence.repository.UserRepository;
import com.wishurgift.service.CommentService;
import com.wishurgift.service.WishlistService;
import com.wishurgift.service.dto.CommentDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.WishlistDTO;
import com.wishurgift.service.dto.request.CommentRequestDTO;
import com.wishurgift.service.dto.transformer.CommentTransformer;
import com.wishurgift.wrapper.Page;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Stateless
public class CommentServiceBean implements CommentService {

    @Inject
    private CommentRepository repository;

    @Inject
    private WishlistService wishlistService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ElementRepository elementRepository;

    @Override
    public CommentDTO getById(UserDTO principal, long id) {
        //Récupération du commentaire
        Optional<Comment> optionalComment = repository.findById(id);
        if (!optionalComment.isPresent()) {
            return null;
        }

        Comment comment = optionalComment.get();
        //vérification accés utilisateur
        WishlistDTO dto = wishlistService.getWishlist(comment.getElement().getWishlist().getId(), principal);
        return dto == null ? null : CommentTransformer.entityToDto(comment);
    }

    @Override
    public Page<CommentDTO> getByElement(UserDTO principal, long idElement) {
        // Recuperation element
        Optional<Element> optionalElement = elementRepository.findById(idElement);
        if (!optionalElement.isPresent()) {
            return null;
        }
        Element elt = optionalElement.get();
        //vérification accés utilisateur
        WishlistDTO dto = wishlistService.getWishlist(elt.getWishlist().getId(), principal);
        if (dto == null) {
            return null;
        }

        List<Comment> commentList = repository.getCommentByElement(elt);
        List<CommentDTO> dtos = CommentTransformer.entityToDto(commentList);
        return Page.unpaged(dtos);
    }

    @Override
    public Page<CommentDTO> getByElement(UserDTO principal, int page, int size, long idElement) {
        // Recuperation element
        Optional<Element> optionalElement = elementRepository.findById(idElement);
        if (!optionalElement.isPresent()) {
            return null;
        }
        Element elt = optionalElement.get();
        //vérification accés utilisateur
        WishlistDTO dto = wishlistService.getWishlist(elt.getWishlist().getId(), principal);
        if (dto == null) {
            return null;
        }

        List<Comment> commentList = repository.getCommentByElement(page, size, elt);
        List<CommentDTO> dtos = CommentTransformer.entityToDto(commentList);
        return Page.paged(page, repository.countCommentByElement(elt), dtos);
    }

    @Override
    public CommentDTO put(UserDTO principal, long id, CommentRequestDTO request) {
        //récupération du commentaire à modifier
        Optional<Comment> optionalComment = repository.findById(id);
        if (!optionalComment.isPresent()) {
            return null;
        }

        Comment comment = optionalComment.get();
        comment.setText(request.getText());
        //vérification utilisateur
        if (comment.getUser().getId() == principal.getId()) {
            repository.update(comment);
            return CommentTransformer.entityToDto(comment);
        }
        return null;
    }

    @Override
    public boolean delete(UserDTO principal, long id) {
        Optional<Comment> optionalComment = repository.findById(id);
        if (!optionalComment.isPresent()) {
            return false;
        }

        Comment comment = optionalComment.get();
        if (comment.getUser().getId() == principal.getId()) {
            repository.delete(optionalComment.get());
            return true;
        }
        return false;
    }

    @Override
    public CommentDTO insert(UserDTO principal, long id, CommentRequestDTO request) {
        //verification utilisateur
        Optional<User> optionalUser = userRepository.findById(principal.getId());
        if (!optionalUser.isPresent()) {
            return null;
        }

        //Verification element
        Optional<Element> optionalElement = elementRepository.findById(id);
        if (!optionalElement.isPresent()) {
            return null;
        }

        //Creation du commentaire
        Comment comment = new Comment();
        comment.setText(request.getText());
        comment.setUser(optionalUser.get());
        comment.setDate(new Date());
        comment.setElement(optionalElement.get());

        //insertion
        repository.insert(comment);

        return CommentTransformer.entityToDto(comment);
    }

}
