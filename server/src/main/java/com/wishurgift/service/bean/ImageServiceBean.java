package com.wishurgift.service.bean;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.entity.Image;
import com.wishurgift.persistence.repository.ElementRepository;
import com.wishurgift.persistence.repository.ImageRepository;
import com.wishurgift.service.ImageService;
import com.wishurgift.service.WishlistService;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.service.dto.ImageDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.WishlistDTO;
import com.wishurgift.service.dto.request.ImageRequestDTO;
import com.wishurgift.service.dto.transformer.ImageTransformer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@Stateless
public class ImageServiceBean implements ImageService {

    @Inject
    WishlistService wishlistService;

    @Inject
    ElementRepository elementRepository;

    @Inject
    ImageRepository repository;

    @Override
    public ImageDTO add(ElementDTO element, ImageRequestDTO request) {
        Optional<Element> optionalElement = elementRepository.findById(element.getId());
        return optionalElement.map(value -> add(value, request)).orElse(null);
    }

    @Override
    public ImageDTO add(Element element, ImageRequestDTO request) {
        Image image = new Image();
        image.setExt(request.getExt());
        image.setData(request.getData());
        image.setElement(element);
        element.getImages().add(image);
        elementRepository.update(element, true);
        return ImageTransformer.entityToDto(element.getImages().get(element.getImages().size() - 1));
    }

    @Override
    public boolean remove(UserDTO principal, long id) {
        // Recuperation de l'image
        Optional<Image> optionalImage = repository.findById(id);
        if (!optionalImage.isPresent()) {
            return false;
        }
        Image image = optionalImage.get();
        // Verfication que l'utilisateur est bien proprietaire de liste ou se trouve l'image
        if (principal.getId() != image.getElement().getWishlist().getUser().getId()) {
            return false;
        }
        // Supprime l'image
        List<Image> images = image.getElement().getImages();
        if (!images.remove(image)) {
            return false;
        }
        elementRepository.update(image.getElement());
        repository.delete(image);
        return true;
    }

    @Override
    public ImageDTO getImage(UserDTO principal, long id) {
        // Recuperation de l'image
        Optional<Image> optionalImage = repository.findById(id);
        if (!optionalImage.isPresent()) {
            return null;
        }
        Image image = optionalImage.get();
        // Si on ne connait pas l'utilisateur
        if (principal == null) {
            // Regarde si la liste de l'image est publique
            if (!image.getElement().getWishlist().isPublic()) {
                return null;
            }
        }
        // Si on connait l'utilisateur
        else {
            // Regarde si l'utilisateur à accès à l'image
            WishlistDTO wishlistDTO = wishlistService.getWishlist(image.getElement().getWishlist().getId(), principal);
            if (wishlistDTO == null) {
                return null;
            }
        }
        return ImageTransformer.entityToDto(image);
    }
}
