package com.wishurgift.service;

import com.wishurgift.persistence.entity.Wishlist;
import com.wishurgift.service.dto.ElementDTO;
import com.wishurgift.service.dto.ImageDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.WishlistDTO;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import com.wishurgift.service.dto.request.ImageRequestDTO;
import com.wishurgift.utils.Constant;

public interface ElementService {

    /**
     * Ajoute un élement sur une liste de souhait
     *
     * @param wishlist La liste de souhait
     * @param request  Les infos de l'élément à ajouter
     * @return L'élement ajouté ou null
     */
    public ElementDTO add(WishlistDTO wishlist, ElementRequestDTO request);

    /**
     * Ajoute un élement sur une liste de souhait
     *
     * @param wishlist La liste de souhait
     * @param request  Les infos de l'élément à ajouter
     * @return L'élement ajouté ou null
     */
    public ElementDTO add(Wishlist wishlist, ElementRequestDTO request);

    /**
     * Modifie un element
     *
     * @param principal L'utilisateur qui effectue la modification
     * @param id        L'id de l'element
     * @param request   Les modifications à faire sur l'element
     * @return L'element modifié ou null
     */
    public ElementDTO update(UserDTO principal, long id, ElementRequestDTO request);

    /**
     * Supprime un élement
     *
     * @param principal L'utilisateur qui supprime
     * @param id        L'id de l'element à supprimer
     */
    public boolean remove(UserDTO principal, long id);

    /**
     * Récupère un élement pour un utilisateur
     *
     * @param principal L'utilisateur qui récupère l'element
     * @param id        L'id de l'element
     * @return L'element ou null
     */
    public ElementDTO getElement(UserDTO principal, long id);

    /**
     * Modifie le status d'un élement
     *
     * @param principal L'utilisateur qui modifie l'element
     * @param id        L'id de l'element
     * @param status    Le nouveau status
     * @return L'element modifié
     */
    public ElementDTO setStatus(UserDTO principal, long id, Constant.Status status);

    /**
     * Ajoute une image à un élement
     *
     * @param principal L'utilisateur qui ajoute l'image
     * @param id        L'id de l'element de l'image
     * @param request   Les infos de l'image à ajouter
     * @return L'image ajoutée ou null en cas d'erreur
     */
    public ImageDTO addImage(UserDTO principal, long id, ImageRequestDTO request);

}
