package com.wishurgift.service;

import com.wishurgift.service.dto.*;
import com.wishurgift.service.dto.request.ElementRequestDTO;
import com.wishurgift.service.dto.request.WishlistRequestDTO;
import com.wishurgift.wrapper.Page;
import com.wishurgift.wrapper.Result;

public interface WishlistService {

    /**
     * Ajoute une liste de souhait
     *
     * @param principal L'utilisateur qui possède la liste
     * @param request   Les infos de la liste
     * @return
     */
    public WishlistHeaderDTO add(UserDTO principal, WishlistRequestDTO request);

    /**
     * Met à jour une liste de souhait
     *
     * @param principal L'utilisateur qui possède la liste
     * @param id        L'id de la liste
     * @param request   Les nouvelles infos de la liste
     * @return Le résultat, en cas de succes il contient un WishlistHeaderDTO, sinon il contient une ErrorDTO
     */
    public Result update(UserDTO principal, long id, WishlistRequestDTO request);

    /**
     * Supprime une liste de souhait
     *
     * @param principal L'utilisateur qui possède la liste
     * @param id        L'id de la liste
     * @return Le résultat, en cas d'erreur il contient une ErrorDTO
     */
    public Result remove(UserDTO principal, long id);

    /**
     * Recupere une liste de souhait par son id
     *
     * @param id
     * @return
     */
    public WishlistDTO getById(long id);

    /**
     * Recupere les informations d'une liste de souhait par son id
     *
     * @param id
     * @return
     */
    public WishlistHeaderDTO getHeaderById(long id);

    /**
     * Récupère une wishlist pour un utilisateur par son id
     *
     * @param id      L'id de la liste
     * @param userDTO L'utilisateur qui accède dans la liste
     * @return
     */
    public WishlistDTO getWishlist(long id, UserDTO userDTO);

    /**
     * Partage la liste de souhait d'un utilisateur à un autre
     *
     * @param principal L'utilisateur qui possède la liste
     * @param id        L'id de la liste
     * @param idUser    L'id de l'utilisateur à qui l'on partage la liste
     * @return true si le partage réussis, false sinon
     */
    public boolean share(UserDTO principal, long id, long idUser);

    /**
     * Supprime le partage la liste de souhait d'un utilisateur à un autre
     *
     * @param principal L'utilisateur qui possède la liste
     * @param id        L'id de la liste
     * @param idUser    L'id de l'utilisateur à qui l'on partage la liste
     * @return true si la suppression du partage réussis, false sinon
     */
    public boolean unshare(UserDTO principal, long id, long idUser);

    /**
     * Ajoute un élément à une liste
     *
     * @param principal L'utilisateur qui ajoute l'élement
     * @param id        L'id de la liste qui accueil le nouvel element
     * @param request   Les infos de l'element
     * @return L'element ajoute ou null
     */
    public ElementDTO addElement(UserDTO principal, long id, ElementRequestDTO request);

    /**
     * Récupère les utilisateurs qui ont accès à une liste
     *
     * @param principal L'utilisateur qui fait la demande (doit être le créateur)
     * @param id        L'id de la liste
     * @return Liste d'utilisateur ou null en cas de probleme
     */
    public Page<UserLightDTO> sharedWith(UserDTO principal, long id);

}
