package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.UpdateUserRequestDTO;
import com.wishurgift.utils.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.ejb.Stateless;

@Stateless
public class UpdateUserValidator extends Validator<UpdateUserRequestDTO> {

    public static final String EMAIL_INVALID = "Email is invalid";
    public static final String ONE_UPDATED_FIELD_REQUIRED = "At least one field must be updated";

    @Override
    protected void validate(UpdateUserRequestDTO data) {
        int nb = 0;
        if (StringUtils.isNotBlank(data.getName())) {
            nb++;
        }
        if (StringUtils.isNotBlank(data.getEmail())) {
            assertTrue(EmailValidator.getInstance().isValid(data.getEmail()), EMAIL_INVALID);
            nb++;
        }
        if (StringUtils.isNotBlank(data.getPassword())) {
            nb++;
        }
        if (data.isPublic() != null) {
            nb++;
        }
        // Au moins 1 champ modifié
        assertTrue(nb > 0, ONE_UPDATED_FIELD_REQUIRED);
    }

}
