package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.LoginRequestDTO;
import com.wishurgift.utils.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.ejb.Stateless;

@Stateless
public class LoginValidator extends Validator<LoginRequestDTO> {

    public static final String LOGIN_REQUIRED = "Login is required";
    public static final String LOGIN_EMAIL_INVALID = "Email is invalid";
    public static final String PASSWORD_REQUIRED = "Password is required";

    @Override
    public void validate(LoginRequestDTO data) {
        if (assertTrue(StringUtils.isNotBlank(data.getLogin()), LOGIN_REQUIRED)) {
            assertTrue(EmailValidator.getInstance().isValid(data.getLogin()), LOGIN_EMAIL_INVALID);
        }
        assertTrue(StringUtils.isNotBlank(data.getPassword()), PASSWORD_REQUIRED);
    }
}
