package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.CommentRequestDTO;
import com.wishurgift.utils.StringUtils;

import javax.ejb.Stateless;

@Stateless
public class CommentValidator extends Validator<CommentRequestDTO> {

    public static final String COMMENT_REQUIRED = "Comment is required";


    @Override
    protected void validate(CommentRequestDTO data) {
        assertTrue(StringUtils.isNotBlank(data.getText()), COMMENT_REQUIRED);
    }
}
