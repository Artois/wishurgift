package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.WishlistRequestDTO;
import com.wishurgift.utils.StringUtils;

import javax.ejb.Stateless;

@Stateless
public class WishlistAddValidator extends Validator<WishlistRequestDTO> {

    public static final String NAME_REQUIRED = "Name is required";
    public static final String PUBLIC_REQUIRED = "Public is required";

    @Override
    protected void validate(WishlistRequestDTO data) {
        assertTrue(StringUtils.isNotBlank(data.getName()), NAME_REQUIRED);
        assertTrue(data.isPublic() != null, PUBLIC_REQUIRED);
    }
}
