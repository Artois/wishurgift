package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.RegisterRequestDTO;
import com.wishurgift.utils.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.ejb.Stateless;

@Stateless
public class RegisterValidator extends Validator<RegisterRequestDTO> {

    public static final String EMAIL_REQUIRED = "Email is required";
    public static final String EMAIL_INVALID = "Email is invalid";
    public static final String NAME_REQUIRED = "Names is required";
    public static final String PASSWORD_REQUIRED = "Password is required";

    @Override
    public void validate(RegisterRequestDTO data) {
        if (assertTrue(StringUtils.isNotBlank(data.getEmail()), EMAIL_REQUIRED)) {
            assertTrue(EmailValidator.getInstance().isValid(data.getEmail()), EMAIL_INVALID);
        }
        assertTrue(StringUtils.isNotBlank(data.getName()), NAME_REQUIRED);
        assertTrue(StringUtils.isNotBlank(data.getPassword()), PASSWORD_REQUIRED);
    }

}
