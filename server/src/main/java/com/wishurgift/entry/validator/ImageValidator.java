package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.ImageRequestDTO;
import com.wishurgift.utils.StringUtils;

import javax.ejb.Stateless;

@Stateless
public class ImageValidator extends Validator<ImageRequestDTO> {

    public static final String EXT_REQUIRED = "Extension is required";
    public static final String DATA_REQUIRED = "Data is required";
    public static final String DATA_INVALID = "Data format is invalid, data must be a base64 encoded image";

    @Override
    protected void validate(ImageRequestDTO data) {
        boolean valid = assertTrue(StringUtils.isNotBlank(data.getExt()), EXT_REQUIRED);
        valid &= assertTrue(StringUtils.isNotBlank(data.getData()), DATA_REQUIRED);
        if (valid) {
            assertTrue(data.getData().startsWith("data:image/" + data.getExt() + ";base64,"), DATA_INVALID);
        }
    }

}
