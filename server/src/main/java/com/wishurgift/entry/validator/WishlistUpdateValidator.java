package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.WishlistRequestDTO;
import com.wishurgift.utils.StringUtils;

import javax.ejb.Stateless;

@Stateless
public class WishlistUpdateValidator extends Validator<WishlistRequestDTO> {

    public static final String ONE_UPDATED_FIELD_REQUIRED = "At least one field must be updated";

    @Override
    protected void validate(WishlistRequestDTO data) {
        int nb = 0;
        if (StringUtils.isNotBlank(data.getName())) {
            nb++;
        }
        if (data.isPublic() != null) {
            nb++;
        }
        // Au moins 1 champ modifié
        assertTrue(nb > 0, ONE_UPDATED_FIELD_REQUIRED);
    }
}
