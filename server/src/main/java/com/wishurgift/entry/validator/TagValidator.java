package com.wishurgift.entry.validator;

import com.wishurgift.service.dto.request.TagRequestDTO;
import com.wishurgift.utils.Constant;
import com.wishurgift.utils.RegexUtils;
import com.wishurgift.utils.StringUtils;

import javax.ejb.Stateless;

@Stateless
public class TagValidator extends Validator<TagRequestDTO> {

    public static final String TAG_REQUIRED = "Tag is required";
    public static final String TAG_INVALID = "Tag is invalid";
    public static final String TAG_USERNAME_INVALID = "Username in tag is invalid";
    public static final String TAG_ID_INVALID = "Identifier in tag is invalid";

    @Override
    public void validate(TagRequestDTO data) {
        if (assertTrue(StringUtils.isNotBlank(data.getTag()), TAG_REQUIRED)) {
            String[] split = data.getTag().split("#");
            if (assertTrue(split.length == 2, TAG_INVALID)) {
                assertTrue(StringUtils.isNotBlank(split[0]), TAG_USERNAME_INVALID);
                assertTrue(StringUtils.isNotBlank(split[1]), TAG_ID_INVALID);
                assertTrue(RegexUtils.match(Constant.REGEX_TAG_ID, split[1]), TAG_ID_INVALID);
            }
        }
    }
}
