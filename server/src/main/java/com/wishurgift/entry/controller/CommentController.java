package com.wishurgift.entry.controller;

import com.wishurgift.entry.security.Connected;
import com.wishurgift.entry.security.Secured;
import com.wishurgift.entry.validator.CommentValidator;
import com.wishurgift.service.CommentService;
import com.wishurgift.service.dto.CommentDTO;
import com.wishurgift.service.dto.UserDTO;
import com.wishurgift.service.dto.request.CommentRequestDTO;
import com.wishurgift.utils.Constant;
import com.wishurgift.utils.JwtUtils;
import com.wishurgift.wrapper.Result;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Slf4j
@Path("/comments")
@Produces(MediaType.APPLICATION_JSON)
public class CommentController {

    @Inject
    private CommentService service;

    @Inject
    private CommentValidator validator;

    @GET
    @Connected
    @Path("/{id}")
    public Response getComment(@PathParam("id") long id, @Context SecurityContext securityContext) {
        log.info("Begin Comment Controller get, id={} ", id);
        //verifie que l'id est correct
        if (id < 1) {
            log.info("End Comment Controller get, id incorrect");
            return Result.error().data(Result.Error.INVALID_REQUEST).trace(Constant.INVALID_ID).response();
        }
        //Récupère le commentaire
        UserDTO user = JwtUtils.getUser(securityContext.getUserPrincipal());
        CommentDTO dto = service.getById(user, id);
        if (dto == null) {
            log.info("End CommentController get, unable to get data");
            return Result.error().data(Result.Error.UNABLE_TO_GET_DATA).response();
        }
        log.info("End Comment Controller get");
        return Result.success().data(dto).response();
    }

    @PUT
    @Secured
    @Path("/{id}")
    public Response update(@PathParam("id") long id, CommentRequestDTO request, @Context SecurityContext securityContext) {
        log.info("Begin Comment Controller update, id={}", id);
        //verifie que l'id est correct
        if (id < 1) {
            log.info("End Comment Controller update, id incorrect");
            return Result.error().data(Result.Error.INVALID_REQUEST).trace(Constant.INVALID_ID).response();
        }

        //verifie le commentaire
        if (validator.isNotValid(request)) {
            log.info("End Comment Controller update, id incorrect");
            return Result.error().data(Result.Error.INVALID_REQUEST).trace(validator.getAllErrors()).response();
        }

        UserDTO user = JwtUtils.getUser(securityContext.getUserPrincipal());
        if (user == null) {
            log.info("End Comment Controller update, unable to connect");
            return Result.error().data(Result.Error.UNABLE_TO_CONNECT).trace(Constant.USER_NOT_FOUND).response();
        }
        //modification
        CommentDTO dto = service.put(user, id, request);
        if (dto == null) {
            log.info("End Comment Controller update, unauthorized");
            return Result.error().data(Result.Error.UNAUTHORIZED).response();
        }
        log.info("End Comment Controller update");
        return Result.success().data(dto).response();
    }

    @DELETE
    @Secured
    @Path("/{id}")
    public Response delete(@PathParam("id") long id, @Context SecurityContext securityContext) {
        log.info("Begin Comment Controller delete, id={}", id);
        //verifie que l'id est correct
        if (id < 1) {
            log.info("End Comment Controller delete, id incorrect");
            return Result.error().data(Result.Error.INVALID_REQUEST).trace(Constant.INVALID_ID).response();
        }

        UserDTO user = JwtUtils.getUser(securityContext.getUserPrincipal());
        if (user == null) {
            log.info("End Comment Controller delete, unable to connect");
            return Result.error().data(Result.Error.UNABLE_TO_CONNECT).trace(Constant.USER_NOT_FOUND).response();
        }
        //suppresion
        if (service.delete(user, id)) {
            log.info("End Comment Controller update");
            return Result.success().response();
        } else {
            log.info("End Comment Controller delete, unable to delete");
            return Result.error().data(Result.Error.UNAUTHORIZED).response();
        }
    }
}
