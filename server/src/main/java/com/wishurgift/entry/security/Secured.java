package com.wishurgift.entry.security;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Indique qu'une méthode n'est accessible que si l'utilisateur est authentifié
 * Sinon l'acces est refusé
 */
@NameBinding
@Retention(RUNTIME)
@Target({METHOD, TYPE})
public @interface Secured {
}
