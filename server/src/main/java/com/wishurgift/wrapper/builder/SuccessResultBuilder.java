package com.wishurgift.wrapper.builder;

import java.lang.reflect.InvocationTargetException;

public class SuccessResultBuilder<T> extends AbstractResultBuilder<T> {

    private Class<T> clazz;

    public SuccessResultBuilder(Class<T> clazz) {
        this.result.setSuccess(true);
        this.clazz = clazz;
    }

    @Override
    public SuccessResultBuilder<T> data() {
        try {
            T obj = clazz.getConstructor().newInstance();
            return this.data(obj);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalArgumentException("Object in result need an empty constructor", e);
        }
    }

    public SuccessResultBuilder<T> data(T obj) {
        result.setData(obj);
        return this;
    }

}
