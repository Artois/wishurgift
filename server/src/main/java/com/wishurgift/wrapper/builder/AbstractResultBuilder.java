package com.wishurgift.wrapper.builder;

import com.wishurgift.utils.ResponseUtils;
import com.wishurgift.wrapper.Result;

import javax.ws.rs.core.Response;

public abstract class AbstractResultBuilder<T> implements ResultBuilder {

    protected Result<T> result = new Result<>();

    @Override
    public Result<T> build() {
        if (result.getData() == null) {
            this.data();
        }
        return result;
    }

    @Override
    public Response response() {
        return ResponseUtils.make(build());
    }

}
