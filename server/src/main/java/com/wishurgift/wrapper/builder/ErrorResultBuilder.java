package com.wishurgift.wrapper.builder;

import com.wishurgift.service.dto.ErrorDTO;
import com.wishurgift.wrapper.Result;

import java.util.Collections;
import java.util.List;

public class ErrorResultBuilder extends AbstractResultBuilder<ErrorDTO> {

    public ErrorResultBuilder() {
        this.result.setSuccess(false);
    }

    @Override
    public ErrorResultBuilder data() {
        this.data(Result.Error.UNKNOWN);
        return this;
    }

    public ErrorResultBuilder data(Result.Error err) {
        return this.data(err.getCode(), err.getMessage());
    }

    public ErrorResultBuilder data(String errorMessage) {
        return this.data(0, errorMessage);
    }

    public ErrorResultBuilder data(int errorCode, String errorMessage) {
        ErrorDTO err = new ErrorDTO();
        err.setCode(errorCode);
        err.setMessage(errorMessage);
        return this.data(err);
    }

    public ErrorResultBuilder data(ErrorDTO err) {
        result.setData(err);
        return this;
    }

    public ErrorResultBuilder trace(String trace) {
        return this.trace(Collections.singletonList(trace));
    }

    public ErrorResultBuilder trace(List<String> trace) {
        if (result.getData() == null) {
            throw new IllegalStateException("Can't add trace without data");
        }
        ErrorDTO err = result.getData();
        err.setTrace(trace);
        return this;
    }

}
