package com.wishurgift.wrapper.builder;

import com.wishurgift.wrapper.Result;

import javax.ws.rs.core.Response;

public interface ResultBuilder {

    public ResultBuilder data();

    public Result build();

    public Response response();

}
