package com.wishurgift.wrapper;

import com.wishurgift.wrapper.builder.ErrorResultBuilder;
import com.wishurgift.wrapper.builder.SuccessResultBuilder;
import lombok.Data;

@Data
public class Result<T> {

    private boolean success;

    private T data;

    public Result() {
        // Empty constructor to generate object
    }

    public Result(boolean success, T data) {
        this.success = success;
        this.data = data;
    }

    public static SuccessResultBuilder<Object> success() {
        return new SuccessResultBuilder<>(Object.class);
    }

    public static <T> SuccessResultBuilder<T> success(Class<T> clazz) {
        return new SuccessResultBuilder<>(clazz);
    }

    public static ErrorResultBuilder error() {
        return new ErrorResultBuilder();
    }

    public enum Error {
        UNKNOWN(0, "Unknown error"),
        UNABLE_TO_CONNECT(1, "Unable to Connect"),
        USER_ALREADY_EXIST(2, "User Already Exist"),
        UNABLE_TO_GET_DATA(3, "Unable to get Data"),
        INVALID_REQUEST(400, "Invalid Request"),
        UNAUTHORIZED(401, "Unauthorized"),
        FORBIDDEN(403, "Forbidden"),
        NOT_FOUND(404, "Resource not found");

        private final int code;
        private final String message;

        Error(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

    }

}
