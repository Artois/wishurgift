package com.wishurgift.wrapper;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Page<T> {

    private long total;
    private Integer page;
    private Integer size;
    private List<T> list;

    /**
     * Création d'une liste non paginée
     *
     * @param elt La liste
     * @param <T> Le type de la liste
     * @return
     */
    public static <T> Page<T> unpaged(List<T> elt) {
        Page<T> unpaged = new Page<>();
        unpaged.setTotal(elt.size());
        unpaged.setList(elt);
        return unpaged;
    }

    /**
     * Création d'une liste pagninée
     *
     * @param page  Le numéro de la page
     * @param total Le nombre total d'element
     * @param elt   La liste
     * @param <T>   Le type de la liste
     * @return
     */
    public static <T> Page<T> paged(int page, long total, List<T> elt) {
        Page<T> paged = new Page<>();
        paged.setTotal(total);
        paged.setPage(page);
        paged.setSize(elt.size());
        paged.setList(elt);
        return paged;
    }

    public void setList(List<T> list) {
        this.list = new ArrayList<>(list);
    }

}
