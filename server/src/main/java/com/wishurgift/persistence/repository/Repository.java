package com.wishurgift.persistence.repository;

import java.util.Optional;

public interface Repository<E> {

    /**
     * Ajoute une entité en base
     *
     * @param entity L'entité à ajouter
     */
    public void insert(E entity);

    /**
     * Met à jour une entité en base
     *
     * @param entity L'entité modifiée à mettre à jour
     */
    public void update(E entity);

    /**
     * Met à jour une entité en base
     *
     * @param entity L'entité modifiée à mettre à jour
     * @param flush  Si on flush ou non apres l'update
     */
    public void update(E entity, boolean flush);

    /**
     * Supprime un entité en base
     *
     * @param entity L'entité à supprimer
     */
    public void delete(E entity);

    /**
     * Récupère une entité par son id
     *
     * @param id L'id de l'entité
     * @return L'entité ou null si l'id ne correspond pas
     */
    public E getById(long id);

    /**
     * Recherche une entité par son id
     *
     * @param id L'id de l'entité
     * @return Optional de l'entité
     */
    public Optional<E> findById(long id);

}
