package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.Contact;
import com.wishurgift.persistence.entity.User;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends Repository<Contact> {

    /**
     * Récupère une requete de contact
     *
     * @param requester L'utilisateur qui à fait la demande
     * @param contact   L'utilisateur qui à reçu la demande
     * @return Le contact ou null
     */
    public Contact getRequest(User requester, User contact);

    /**
     * Récupère une requete de contact
     *
     * @param requester L'utilisateur qui à fait la demande
     * @param contact   L'utilisateur qui à reçu la demande
     * @return Le contact
     */
    public Optional<Contact> findRequest(User requester, User contact);

    /**
     * Récupère toutes les utilisateurs des requetes de contact émise et qui ne sont pas accepté
     *
     * @param requester L'utilisateur qui à fait les requetes
     * @return Liste des utilisateur qui ont reçu la requete et qui ne l'ont pas encore accepté
     */
    public List<User> getAllRequest(User requester);

    /**
     * Récupère une liste paginée des utilisateurs des requetes de contact émise et qui ne sont pas accepté
     *
     * @param page      Le numéro de la page
     * @param size      La taille de la page
     * @param requester L'utilisateur qui à fait les requetes
     * @return Liste des utilisateur qui ont reçu la requete et qui ne l'ont pas encore accepté
     */
    public List<User> getAllRequest(int page, int size, User requester);

    /**
     * Compte le nombre total des utilisateurs des requetes de contact émise et qui ne sont pas accepté
     *
     * @param requester L'utilisateur qui à fait la requete
     * @return
     */
    public long countAllRequest(User requester);

    /**
     * Recupere toutes les utilisateurs qui ont envoyé une réquete de contact
     *
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public List<User> getAllPending(User user);

    /**
     * Recupere une liste paginée des utilisateurs qui ont envoyé une requete de contact
     *
     * @param page Le numero de la page
     * @param size Le nombre d'element sur la page
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public List<User> getAllPending(int page, int size, User user);

    /**
     * Compte le nombre d'utilisateur s qui ont envoyé une requete de contact
     *
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public long countAllPending(User user);

    /**
     * Récupere tous les utilisateurs en contacts d'un utilisateur
     *
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public List<User> getAllContact(User user);

    /**
     * Récupére une liste paginée de tous les utilisateurs en contact d'un utilisateur
     *
     * @param page Le numero de la oage
     * @param size Le nombre d'élement par page
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public List<User> getAllContact(int page, int size, User user);

    /**
     * Compte tous les utilisateurs en contact d'un utilisateur
     *
     * @param user L'utilisateur pour la recherche
     * @return
     */
    public long countAllContact(User user);

}
