package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.persistence.entity.Element;

import java.util.List;

public interface CommentRepository extends Repository<Comment> {

    public List<Comment> getCommentByElement(Element elt);

    public List<Comment> getCommentByElement(int page, int size, Element elt);

    public long countCommentByElement(Element elt);

}
