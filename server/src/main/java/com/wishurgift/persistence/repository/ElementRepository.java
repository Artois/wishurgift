package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.Element;

public interface ElementRepository extends Repository<Element> {

}
