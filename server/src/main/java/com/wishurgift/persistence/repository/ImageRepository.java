package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.Image;

public interface ImageRepository extends Repository<Image> {

}
