package com.wishurgift.persistence.repository.bean;

import com.wishurgift.persistence.entity.Comment;
import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.repository.CommentRepository;
import com.wishurgift.persistence.repository.PersistableRepository;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class CommentRepositoryBean extends PersistableRepository<Comment> implements CommentRepository {

    @Override
    protected Class<Comment> getEntityClass() {
        return Comment.class;
    }

    @Override
    public List<Comment> getCommentByElement(Element elt) {
        return getCommentByElement(0, 0, elt);
    }

    @Override
    public List<Comment> getCommentByElement(int page, int size, Element elt) {
        TypedQuery<Comment> query = entityManager.createNamedQuery(Comment.GETBYELEMENT_REQUEST, Comment.class);
        query.setParameter("elt", elt);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countCommentByElement(Element elt) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Comment.COUNTBYELEMENT_REQUEST, Long.class);
        query.setParameter("elt", elt);
        return query.getSingleResult();
    }
}
