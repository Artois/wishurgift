package com.wishurgift.persistence.repository.bean;

import com.wishurgift.persistence.entity.User;
import com.wishurgift.persistence.entity.Wishlist;
import com.wishurgift.persistence.repository.CommentRepository;
import com.wishurgift.persistence.repository.PersistableRepository;
import com.wishurgift.persistence.repository.SharedRepository;
import com.wishurgift.persistence.repository.WishlistRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class WishlistRepositoryBean extends PersistableRepository<Wishlist> implements WishlistRepository {

    @Inject
    private SharedRepository sharedRepository;

    @Inject
    private CommentRepository commentRepository;

    @Override
    public void delete(Wishlist entity) {
        // Remove shared
        sharedRepository.getAllFromWishlist(entity).forEach(shared -> sharedRepository.delete(shared));
        // Remove comments
        entity.getElements().forEach(elt -> {
            commentRepository.getCommentByElement(elt).forEach(com -> commentRepository.delete(com));
        });
        // Remove entity
        super.delete(entity);
    }

    @Override
    public List<Wishlist> getAllByUser(User owner, User user) {
        return getAllByUser(0, 0, owner, user);
    }

    @Override
    public List<Wishlist> getAllByUser(int page, int size, User owner, User user) {
        TypedQuery<Wishlist> query = entityManager.createNamedQuery(Wishlist.FIND_BY_USER, Wishlist.class);
        query.setParameter("owner", owner);
        query.setParameter("user", user);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countAllByUser(User owner, User user) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Wishlist.COUNT_BY_USER, Long.class);
        query.setParameter("owner", owner);
        query.setParameter("user", user);
        return query.getSingleResult();
    }

    @Override
    public Wishlist getWishlist(long id, User user) {
        TypedQuery<Wishlist> query = entityManager.createNamedQuery(Wishlist.GET, Wishlist.class);
        query.setParameter("id", id);
        query.setParameter("user", user);
        List<Wishlist> result = query.getResultList();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    @Override
    public Optional<Wishlist> findWishlist(long id, User user) {
        Wishlist result = getWishlist(id, user);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public List<Wishlist> getShared(User user) {
        return getShared(0, 0, user);
    }

    @Override
    public List<Wishlist> getShared(int page, int size, User user) {
        TypedQuery<Wishlist> query = entityManager.createNamedQuery(Wishlist.GET_SHARED_BY_USER, Wishlist.class);
        query.setParameter("user", user);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countShared(User user) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Wishlist.COUNT_SHARED_BY_USER, Long.class);
        query.setParameter("user", user);
        return query.getSingleResult();
    }

    @Override
    public List<User> sharedWith(Wishlist wishlist) {
        TypedQuery<User> query = entityManager.createNamedQuery(Wishlist.SHARED_WITH, User.class);
        query.setParameter("wishlist", wishlist);
        return query.getResultList();
    }

    @Override
    protected Class<Wishlist> getEntityClass() {
        return Wishlist.class;
    }

}
