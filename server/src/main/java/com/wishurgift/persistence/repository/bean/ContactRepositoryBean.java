package com.wishurgift.persistence.repository.bean;

import com.wishurgift.persistence.entity.Contact;
import com.wishurgift.persistence.entity.User;
import com.wishurgift.persistence.repository.ContactRepository;
import com.wishurgift.persistence.repository.PersistableRepository;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class ContactRepositoryBean extends PersistableRepository<Contact> implements ContactRepository {

    @Override
    public Contact getRequest(User requester, User contact) {
        TypedQuery<Contact> query = entityManager.createNamedQuery(Contact.FIND_REQUEST, Contact.class);
        query.setParameter("requester", requester);
        query.setParameter("contact", contact);
        List<Contact> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @Override
    public Optional<Contact> findRequest(User requester, User contact) {
        Contact result = getRequest(requester, contact);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public List<User> getAllRequest(User requester) {
        return getAllRequest(0, 0, requester);
    }

    @Override
    public List<User> getAllRequest(int page, int size, User requester) {
        TypedQuery<User> query = entityManager.createNamedQuery(Contact.GET_ALL_REQUEST, User.class);
        query.setParameter("requester", requester);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countAllRequest(User requester) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Contact.COUNT_ALL_REQUEST, Long.class);
        query.setParameter("requester", requester);
        return query.getSingleResult();
    }

    @Override
    public List<User> getAllPending(User user) {
        return getAllPending(0, 0, user);
    }

    @Override
    public List<User> getAllPending(int page, int size, User user) {
        TypedQuery<User> query = entityManager.createNamedQuery(Contact.GET_ALL_PENDING, User.class);
        query.setParameter("user", user);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countAllPending(User user) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Contact.COUNT_ALL_PENDING, Long.class);
        query.setParameter("user", user);
        return query.getSingleResult();
    }

    @Override
    public List<User> getAllContact(User user) {
        return getAllContact(0, 0, user);
    }

    @Override
    public List<User> getAllContact(int page, int size, User user) {
        TypedQuery<User> query = entityManager.createNamedQuery(Contact.GET_ALL, User.class);
        query.setParameter("user", user);
        if (page > 0) {
            query.setFirstResult(size * (page - 1));
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public long countAllContact(User user) {
        TypedQuery<Long> query = entityManager.createNamedQuery(Contact.COUNT_ALL, Long.class);
        query.setParameter("user", user);
        return query.getSingleResult();
    }

    @Override
    protected Class<Contact> getEntityClass() {
        return Contact.class;
    }
}
