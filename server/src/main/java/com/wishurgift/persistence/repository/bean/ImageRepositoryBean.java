package com.wishurgift.persistence.repository.bean;

import com.wishurgift.persistence.entity.Image;
import com.wishurgift.persistence.repository.ImageRepository;
import com.wishurgift.persistence.repository.PersistableRepository;

import javax.ejb.Stateless;

@Stateless
public class ImageRepositoryBean extends PersistableRepository<Image> implements ImageRepository {

    @Override
    protected Class<Image> getEntityClass() {
        return Image.class;
    }

}
