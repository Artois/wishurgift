package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.User;
import com.wishurgift.persistence.entity.Wishlist;

import java.util.List;
import java.util.Optional;

public interface WishlistRepository extends Repository<Wishlist> {

    /**
     * Recupere toutes les listes d'un utilisateur pour un autre utilisateur
     *
     * @param owner L'utilisateur pour la recherche
     * @param user  L'utilisateur qui effectue la recherche
     * @return
     */
    public List<Wishlist> getAllByUser(User owner, User user);

    /**
     * Recupere une liste paginée des listes d'un utilisateur pour un autre utilisateur
     *
     * @param page  Le numéro de la page
     * @param size  La taille de la page
     * @param owner L'utilisateur pour la recherche
     * @param user  L'utilisateur qui effectue la recherche
     * @return
     */
    public List<Wishlist> getAllByUser(int page, int size, User owner, User user);

    /**
     * Compte le nombres de listes réxupèrable d'un utilisateur pour un autre utilisateur
     *
     * @param owner L'utilisateur pour la recherche
     * @param user  L'utilisateur qui effectue la recherche
     * @return
     */
    public long countAllByUser(User owner, User user);

    /**
     * Récupère une liste de souhait pour un utilisateur par son id
     *
     * @param id   L'id de la liste
     * @param user L'utilisateur qui veut récupèrer la liste
     * @return La liste ou null
     */
    public Wishlist getWishlist(long id, User user);

    /**
     * Recherche une liste de souhait pour un utilisateur par son id
     *
     * @param id   L'id de la liste
     * @param user L'utilisateur qui veut récupèrer la liste
     * @return
     */
    public Optional<Wishlist> findWishlist(long id, User user);

    /**
     * Recupère toutes les listes de souhait partagée avec un utilisateur
     *
     * @param user L'utilisateur
     * @return La liste de liste de souhait
     */
    public List<Wishlist> getShared(User user);

    /**
     * Recupère une liste paginée des listes de souhait partagée avec un utilisateur
     *
     * @param page Le numéro de la page
     * @param size La taille de la page
     * @param user L'utilisateur
     * @return La liste de liste de souhait
     */
    public List<Wishlist> getShared(int page, int size, User user);

    /**
     * Compte le nombre total de liste de souhait partagée avec un utilisateur
     *
     * @param user L'utilisateur
     * @return Le nombre de liste de souhait partagée avec l'utilisateur
     */
    public long countShared(User user);

    /**
     * Récupère les utilisateurs qui ont accès à une liste
     *
     * @param wishlist
     * @return
     */
    public List<User> sharedWith(Wishlist wishlist);

}
