package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends Repository<User> {

    /**
     * Récupère un utilisateur par son email
     *
     * @param email
     * @return
     */
    public User getByEmail(String email);

    /**
     * Recherche un utilisateur par son email
     *
     * @param email
     * @return
     */
    public Optional<User> findByEmail(String email);

    /**
     * Récupère un utilisateur par son tag
     *
     * @param tag
     * @return
     */
    public User getByTag(String tag);

    /**
     * Recherche un utilisateur par son tag
     *
     * @param tag
     * @return
     */
    public Optional<User> findByTag(String tag);

    /**
     * Recherche des utilisateurs par leur email, leur nom ou leur tag
     *
     * @param request La recherche
     * @param limit   Le nombre maximum de résultat (0 = pas de limite)
     * @return Une liste d'utilisateur correspondant à la recherche
     */
    public List<User> search(String request, int limit);

}
