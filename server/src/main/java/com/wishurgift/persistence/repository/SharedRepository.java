package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.entity.Shared;
import com.wishurgift.persistence.entity.User;
import com.wishurgift.persistence.entity.Wishlist;

import java.util.List;
import java.util.Optional;

public interface SharedRepository extends Repository<Shared> {

    /**
     * Recupère un partage d'accès à partir de la liste et de l'utilisateur
     *
     * @param w La liste de souhait partagé
     * @param u L'utilisateur qui y à accès
     * @return Le partage d'accès
     */
    public Shared getShared(Wishlist w, User u);

    /**
     * Recupère un partage d'accès à partir de la liste et de l'utilisateur
     *
     * @param w La liste de souhait partagé
     * @param u L'utilisateur qui y à accès
     * @return L'optional du partage d'accès
     */
    public Optional<Shared> findShared(Wishlist w, User u);

    /**
     * Récupère tous les droits d'accès d'une liste de souhait
     *
     * @param w La liste de souhaite
     * @return Les droits d'accès de la liste
     */
    public List<Shared> getAllFromWishlist(Wishlist w);

}
