package com.wishurgift.persistence.repository.bean;

import com.wishurgift.persistence.entity.Element;
import com.wishurgift.persistence.repository.CommentRepository;
import com.wishurgift.persistence.repository.ElementRepository;
import com.wishurgift.persistence.repository.PersistableRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ElementRepositoryBean extends PersistableRepository<Element> implements ElementRepository {

    @Inject
    private CommentRepository commentRepository;

    @Override
    public void delete(Element entity) {
        commentRepository.getCommentByElement(entity).forEach(com -> commentRepository.delete(com));
        super.delete(entity);
    }

    @Override
    protected Class<Element> getEntityClass() {
        return Element.class;
    }

}
