package com.wishurgift.persistence.repository;

import com.wishurgift.persistence.data.StatsStatus;
import com.wishurgift.persistence.data.StatsWishlist;
import com.wishurgift.persistence.entity.User;

public interface StatisticRepository {

    /**
     * Récupère des statistiques sur les listes de souhaits d'un utilisateur
     *
     * @param user L'utilisateur pour lequel on récupère les statistiques
     * @return Les statisques
     */
    public StatsWishlist wishlistStats(User user);

    /**
     * Récupère des statistiques sur les status des elements des listes de souhait d'un utilisateur
     *
     * @param user L'utilisateur pour lequel on récupère les statistiques
     * @return Les statisques
     */
    public StatsStatus statusStats(User user);

}
