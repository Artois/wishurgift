package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "images")
public class Image implements Persistable {

    @Id
    @Column(name = "im_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String ext;

    private String data;

    @ManyToOne
    @JoinColumn(name = "el_id")
    private Element element;

}
