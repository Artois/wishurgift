package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "comments")
@NamedQueries({
        @NamedQuery(name = "comment.getByElement", query = "select distinct c from Comment c where c.element = :elt"),
        @NamedQuery(name = "comment.countByElement", query = "select count(distinct c) from Comment c where c.element = :elt")
})
public class Comment implements Persistable {

    public static final String GETBYELEMENT_REQUEST = "comment.getByElement";
    public static final String COUNTBYELEMENT_REQUEST = "comment.countByElement";

    @Id
    @Column(name = "co_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String text;

    private Date date;

    @ManyToOne
    @JoinColumn(name = "us_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "el_id")
    private Element element;
}
