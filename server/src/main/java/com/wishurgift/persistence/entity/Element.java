package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "elements")
public class Element implements Persistable {

    @Id
    @Column(name = "el_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String status;

    private String name;

    private String link;

    private String address;

    private double price;

    private String description;

    @ManyToOne
    @JoinColumn(name = "wi_id")
    private Wishlist wishlist;

    @ManyToOne
    @JoinColumn(name = "us_id")
    private User buyer;

    @OneToMany(mappedBy = "element", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Image> images;

}
