package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contact")
@NamedQueries({
        @NamedQuery(name = "contact.findRequest", query = "select c from Contact c where c.requester = :requester and c.contact = :contact"),
        @NamedQuery(name = "contact.countAllRequest", query = "select count(distinct c.contact) from Contact c where c.requester = :requester and c.contact not in (select co.requester from Contact co where co.contact = :requester)"),
        @NamedQuery(name = "contact.getAllRequest", query = "select distinct c.contact from Contact c where c.requester = :requester and c.contact not in (select co.requester from Contact co where co.contact = :requester)"),
        @NamedQuery(name = "contact.countAllPending", query = "select count(distinct c.requester) from Contact c where c.contact = :user and c.requester not in (select co.contact from Contact co where co.requester = :user)"),
        @NamedQuery(name = "contact.getAllPending", query = "select distinct c.requester from Contact c where c.contact = :user and c.requester not in (select co.contact from Contact co where co.requester = :user)"),
        @NamedQuery(name = "contact.countAll", query = "select count(distinct c.contact) from Contact c where c.requester = :user and c.contact in (select co.requester from Contact co where co.contact = :user)"),
        @NamedQuery(name = "contact.getAll", query = "select distinct c.contact from Contact c where c.requester = :user and c.contact in (select co.requester from Contact co where co.contact = :user)")
})
public class Contact implements Persistable {

    public static final String FIND_REQUEST = "contact.findRequest";
    public static final String COUNT_ALL_REQUEST = "contact.countAllRequest";
    public static final String GET_ALL_REQUEST = "contact.getAllRequest";
    public static final String COUNT_ALL_PENDING = "contact.countAllPending";
    public static final String GET_ALL_PENDING = "contact.getAllPending";
    public static final String COUNT_ALL = "contact.countAll";
    public static final String GET_ALL = "contact.getAll";

    @Id
    @Column(name = "co_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "us_id")
    private User contact;

    @ManyToOne
    @JoinColumn(name = "us_id_demandeur")
    private User requester;

}
