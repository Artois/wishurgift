package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "wishlists")
@NamedQueries({
        @NamedQuery(name = "wishlist.get", query = "select w from Wishlist w where w.id = :id and (w.isPublic = true or w.user = :user or w.id in (select s.wishlist.id from Shared s where s.user = :user))"),
        @NamedQuery(name = "wishlist.findByUser", query = "select distinct w from Wishlist w where w.user = :owner and (w.user = :user or w.isPublic = true or w.id in (select s.wishlist.id from Shared s where s.user = :user))"),
        @NamedQuery(name = "wishlist.countByUser", query = "select count(distinct w) from Wishlist w where w.user = :owner and (w.user = :user or w.isPublic = true or w.id in (select s.wishlist.id from Shared s where s.user = :user))"),
        @NamedQuery(name = "wishlist.getSharedByUser", query = "select distinct w from Wishlist w where w.id in (select s.wishlist.id from Shared s where s.user = :user)"),
        @NamedQuery(name = "wishlist.countSharedByUser", query = "select count(distinct w) from Wishlist w where w.id in (select s.wishlist.id from Shared s where s.user = :user)"),
        @NamedQuery(name = "wishlist.sharedWith", query = "select distinct u from Shared s inner join s.user u where s.wishlist = :wishlist")
})
public class Wishlist implements Persistable {

    public static final String GET = "wishlist.get";
    public static final String FIND_BY_USER = "wishlist.findByUser";
    public static final String COUNT_BY_USER = "wishlist.countByUser";
    public static final String GET_SHARED_BY_USER = "wishlist.getSharedByUser";
    public static final String COUNT_SHARED_BY_USER = "wishlist.countSharedByUser";
    public static final String SHARED_WITH = "wishlist.sharedWith";

    @Id
    @Column(name = "wi_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @Column(name = "public")
    private boolean isPublic;

    @ManyToOne
    @JoinColumn(name = "us_id")
    private User user;

    @OneToMany(mappedBy = "wishlist", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Element> elements;

}
