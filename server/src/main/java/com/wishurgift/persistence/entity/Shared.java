package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "shared")
@NamedQueries({
        @NamedQuery(name = "shared.getAll", query = "select s from Shared s where s.wishlist = :wishlist"),
        @NamedQuery(name = "shared.find", query = "select s from Shared s where  s.wishlist = :wishlist and s.user = :user")
})
public class Shared implements Persistable {

    public static final String GET_ALL = "shared.getAll";
    public static final String FIND = "shared.find";

    @Id
    @Column(name = "sh_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "wi_id")
    private Wishlist wishlist;

    @ManyToOne
    @JoinColumn(name = "us_id")
    private User user;

}
