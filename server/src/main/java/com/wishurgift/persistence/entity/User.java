package com.wishurgift.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "user.findByTag", query = "select u from User u where u.tag = :tag"),
        @NamedQuery(name = "user.findByEmail", query = "select u from User u where lower(u.email) = lower(:email)"),
        @NamedQuery(name = "user.search", query = "select u from User u where (u.email like :search or u.tag like :search or u.name like :search) and u.isPublic = true")
})
public class User implements Persistable {

    public static final String FIND_BY_TAG = "user.findByTag";
    public static final String FIND_BY_EMAIL = "user.findByEmail";
    public static final String SEARCH = "user.search";

    @Id
    @Column(name = "us_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String email;

    private String password;

    @Column(name = "public")
    private boolean isPublic;

    private String tag;

}
