package com.wishurgift.persistence.entity;

public interface Persistable {

    public long getId();

}
