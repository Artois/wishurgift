package com.wishurgift.persistence.data;

import lombok.Data;

@Data
public class StatsStatus {

    private long nbIdea;

    private long nbCrush;

    private long nbReserved;

    private long nbPurchased;

}
