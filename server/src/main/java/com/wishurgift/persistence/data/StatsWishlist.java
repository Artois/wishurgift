package com.wishurgift.persistence.data;

import lombok.Data;

@Data
public class StatsWishlist {

    private long nbWishlist;

    private long nbElement;

    private double avgPrice;

    private double minPrice;

    private double maxPrice;

    public StatsWishlist() {
        // Empty constructor POJO
    }

    public StatsWishlist(Long nbWishlist, Long nbElement, Double avgPrice, Double minPrice, Double maxPrice) {
        this.nbWishlist = nbWishlist == null ? 0 : nbWishlist;
        this.nbElement = nbElement == null ? 0 : nbElement;
        this.avgPrice = avgPrice == null ? 0 : avgPrice;
        this.minPrice = minPrice == null ? 0 : minPrice;
        this.maxPrice = maxPrice == null ? 0 : maxPrice;
    }

}
