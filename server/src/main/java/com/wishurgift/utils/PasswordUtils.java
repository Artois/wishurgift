package com.wishurgift.utils;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;

public class PasswordUtils {

    /**
     * Hash un mot de passe
     *
     * @param password Le mot de passe a hasher
     * @return Le mot de passe hasher
     */
    public static String hash(String password) {
        return BCrypt.with(BCrypt.Version.VERSION_2B, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2B)).hashToString(8, password.toCharArray());
    }

    /**
     * Verifie qu'un mot de passe est valide
     *
     * @param password     Le mot de passe à verifier
     * @param passwordHash Le mot de passe hasher pour verifier
     * @return Valide ou non
     */
    public static boolean verify(String password, String passwordHash) {
        if (password == null || passwordHash == null) {
            return false;
        }
        BCrypt.Result result = BCrypt.verifyer(BCrypt.Version.VERSION_2B, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2B)).verify(password.toCharArray(), passwordHash);
        return result.verified;
    }

}
