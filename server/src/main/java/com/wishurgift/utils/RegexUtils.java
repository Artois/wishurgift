package com.wishurgift.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    /**
     * Test un regex sur une chaine de caracteres
     *
     * @param regex Le regex
     * @param test  La chaine a tester
     * @return Si la chaine correspond au Regex
     */
    public static boolean match(String regex, String test) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(test);
        return matcher.find();
    }
}
