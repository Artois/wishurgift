package com.wishurgift.utils;

public class Constant {

    // Clef token JWT
    public static final String JWT_KEY = "xzAYHNxgX88eeWsv021kglVfvpOOI2Kk36ROKGPB";

    // Authentification
    public static final String AUTHENTICATION_REALM = "wishurgift";
    public static final String AUTHENTICATION_SCHEME = "Bearer";

    // Trace d'erreur controller
    public static final String PAGE_NUMBER_INVALID = "Page number is invalid";
    public static final String INVALID_ID = "ID is invalid";
    public static final String CONTACT_REQUEST_ALREADY_EXIST = "Contact request already exist";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String LOGIN_FAIL = "Invalid login or password";
    public static final String SEARCH_REQUEST_EMPTY = "Search request cannot be empty";
    public static final String LIMIT_INVALID = "Limit is invalid";
    public static final String EMAIL_ALREADY_USED = "Email is already used";

    // Regex
    public static final String REGEX_TAG_ID = "^[0-9]{5}$";

    // Statut d'un element
    public static enum Status {

        IDEA(1, "idea"),
        CRUSH(1, "crush"),
        RESERVED(2, "reserved"),
        PURCHASED(3, "purchased");

        /**
         * Position de l'etat par rappor aux autre
         */
        private final int pos;

        /**
         * Nom de l'etat
         */
        private final String name;

        Status(int pos, String name) {
            this.pos = pos;
            this.name = name;
        }

        public static Status fromName(String name) {
            Status[] status = values();
            for (int i = 0; i < status.length; i++) {
                Status s = status[i];
                if (s.getName().equals(name)) {
                    return s;
                }
            }
            return null;
        }

        public int getPos() {
            return pos;
        }

        public String getName() {
            return name;
        }

    }

}
