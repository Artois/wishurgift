package com.wishurgift.utils;

public class StringUtils {

    public static boolean isNull(String str) {
        return str == null;
    }

    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }
        return str.isEmpty();
    }

    public static boolean isBlank(String str) {
        if (str == null) {
            return true;
        }
        return str.trim().isEmpty();
    }

    public static boolean isNotNull(String str) {
        return !isNull(str);
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static String capitalize(String str) {
        if (isBlank(str)) {
            return str;
        } else if (str.length() == 1) {
            return str.toUpperCase();
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }

    public static String lower(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase();
    }

}
