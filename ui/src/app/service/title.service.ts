import {Injectable} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})
export class TitleService {

    private static BASE_TITLE = 'Wishurgift';

    // tslint:disable-next-line:variable-name
    private _title: string;
    private baseTitle: string = null;

    constructor(private titleAngular: Title) {
        this.resetTitle();
    }

    public resetTitle(): void {
        this._title = TitleService.BASE_TITLE;
        this.baseTitle = null;
        this.setTitle();
    }

    get title(): string {
        return this._title;
    }

    set title(val: string) {
        this._title = TitleService.BASE_TITLE + ' - ' + val;
        this.setTitle();
    }

    public add(val: string): void {
        if (this.baseTitle == null) {
            this.baseTitle = this._title;
        }
        this._title = this.baseTitle + ' ' + val;
        this.setTitle();
    }

    private setTitle(): void {
        this.titleAngular.setTitle(this._title);
    }

}
