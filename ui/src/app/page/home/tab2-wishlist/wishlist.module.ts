import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {WishlistComponent} from './wishlist.component';
import {ExploreContainerComponentModule} from '../../explore-container/explore-container.module';
import {HeaderModule} from '../../../component/header/header.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ExploreContainerComponentModule,
        HeaderModule,
        RouterModule.forChild([{path: '', component: WishlistComponent}])
    ],
    declarations: [WishlistComponent]
})
export class WishlistComponentModule {
}
