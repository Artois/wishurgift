import {Component} from '@angular/core';
import {TitleService} from '../../../service/title.service';

@Component({
    selector: 'wishurgift-wishlist',
    templateUrl: 'wishlist.component.html',
    styleUrls: ['wishlist.component.scss']
})
export class WishlistComponent {

    constructor(private titleService: TitleService) {
        this.titleService.add('Wishlist');
    }

    segmentChanged(ev: any) {
        console.log('Segment changed', ev.detail.value);
    }
}
