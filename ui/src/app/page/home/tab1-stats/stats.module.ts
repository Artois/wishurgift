import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {StatsComponent} from './stats.component';
import {ExploreContainerComponentModule} from '../../explore-container/explore-container.module';
import {HeaderModule} from '../../../component/header/header.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ExploreContainerComponentModule,
        HeaderModule,
        RouterModule.forChild([{path: '', component: StatsComponent}])
    ],
    declarations: [StatsComponent]
})
export class StatsComponentModule {
}
