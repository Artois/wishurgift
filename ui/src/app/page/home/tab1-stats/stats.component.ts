import {Component} from '@angular/core';
import {TitleService} from '../../../service/title.service';

@Component({
    selector: 'wishurgift-stats',
    templateUrl: 'stats.component.html',
    styleUrls: ['stats.component.scss']
})
export class StatsComponent {

    constructor(private titleService: TitleService) {
        this.titleService.add('Stats');
    }

}
