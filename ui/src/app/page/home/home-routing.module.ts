import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: 'stats',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('./tab1-stats/stats.module').then(m => m.StatsComponentModule)
                    }
                ]
            },
            {
                path: 'wishlist',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('./tab2-wishlist/wishlist.module').then(m => m.WishlistComponentModule)
                    }
                ]
            },
            {
                path: 'contact',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('./tab3-contact/contact.module').then(m => m.ContactComponentModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/home/stats',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/home/stats',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeComponentRoutingModule {
}
