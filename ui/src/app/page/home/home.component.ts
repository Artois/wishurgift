import {Component} from '@angular/core';
import {TitleService} from '../../service/title.service';

@Component({
    selector: 'wishurgift-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})
export class HomeComponent {

    constructor(private titleService: TitleService) {
        this.titleService.title = 'Home:';
    }

}
