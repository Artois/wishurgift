import {Component} from '@angular/core';
import {TitleService} from '../../../service/title.service';

@Component({
    selector: 'wishurgift-contact',
    templateUrl: 'contact.component.html',
    styleUrls: ['contact.component.scss']
})
export class ContactComponent {

    constructor(private titleService: TitleService) {
        this.titleService.add('Contact');
    }

}
