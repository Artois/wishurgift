import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './login.component';
import {HeaderModule} from '../../component/header/header.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HeaderModule,
        RouterModule.forChild([{path: '', component: LoginComponent}])
    ],
    declarations: [LoginComponent]
})
export class LoginComponentModule {
}
