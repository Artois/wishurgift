import {Component, OnInit} from '@angular/core';
import {TitleService} from '../../service/title.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    constructor(private titleService: TitleService) {
        this.titleService.title = 'Login';
    }

    ngOnInit() {
    }

}
