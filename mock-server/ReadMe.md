# Wishurgift Mock Server

Serveur simulant le serveur de l'application Wishurgift dans le cadre du développement ou du test.

La description de l'API est disponible sur le [git du serveur](https://gitlab.univ-artois.fr/arthur_brandao/wishurgift-server).

Pour installer l'application il suffit de télécharger le projet et d'installer nodeJS, puis d'éxécuter la commande `npm install`.

Pour lancer le serveur il suffit d'utiliser l'une des commandes suivantes :

```bash
# Lancement de l'application
npm start

# Lancement de l'application en mode verbose
npm run dev
```

Le mode verbose permet d'afficher les URL chargées par le serveur, et de voir les réquetes et leur parametres reçu par le serveur.

L'explication du fonctionnement du serveur est trouvable [ici](https://github.com/Loquicom/Mock-Server)