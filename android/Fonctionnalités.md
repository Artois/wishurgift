Gestion des utilisateurs: 

	- Création de compte utilisateur
	- Connexion sécurisée à l'application
	- Gestion de profil
	- Ajout et gestion d'amis
	- Gestion des listes
	- Choix des personnes avec qui  partager une liste ou plusieurs
	- Consulter les listes partager avec lui
	- Réserver un jeux

	
Gestion Des Cadeaux :

	- Enregistrement des cadeaux
	- Collecte des meta informations sur des sites de vente ou ajouter une adresse de magasin physique
	- Ajout de meta informations  sur le cadeau
	- Ajout d'un champ de commentaire sur chaque cadeau
	- Evaluation (interet) sur le cadeau 
	
Gestion de la liste: 

	
	- Création
	- modifications
	- consultation 
	- Suppression
	

Notification 

	- Notifications quand des cadeaux sont ajoutés 
	
	
