# Wishurgift Android

L'application Android Wishurgift, dans le cadre du projet du second semestre de l'année 2019-2020 du M2 ILI à l'université d'Artois.

Les membres du groupe sont :
- Arthur Brandao
- Maxence Bacquet
- Ouahab Fenniche
- Aymeric Vandewoorde
 
## Le serveur

Pour fonctionner l'application à besoins d'une API mise à disposition par un serveur. [Le serveur de l'application est disponoble ici](https://gitlab.univ-artois.fr/arthur_brandao/wishurgift-server).
L'API est décrite dans le documentation du serveur. Il existe aussi un faux serveur ([disponible ici](https://gitlab.univ-artois.fr/arthur_brandao/wishurgift-mock-server)) dans le but de ne pas bloquer le développement de l'application. 
Il implemente la même API mais retourne des données factices.

## L'application

Wishurgift est une application de liste de souhait communautaire. Elle permet de créer des listes de souhaits et de les partager à ces contacts et de réserver des cadeaux dans les listes des autres.
Elle dispose de fonction communautaire comme un système de commentaire et permet la création de liste privée (partageable à ces certains contacts) ou plublique (visible par tous le monde).

Pour faciliter le développement le serveur a été mis en ligne à l'adresse http://wishurgift.loquico.me, cependant pour éviter de payer le serveur, il a été coupé le 29 avril. 

## Fonctionnalité

Description de chaque fonctionnalité majeure de l'application.

### Gestion des listes

- [x] Écran affichant toutes les listes de l'utilisateurs
- [x] Ajouter une nouvelle liste
- [x] Modifier une liste existante
- [x] Supprimer une liste existante
- [x] Partager une liste existante via l'application aux différents contacts de l'utilisateur
- [x] Gérer les droits d'une liste (qui peut voir ma liste) pour pouvoir supprimer ou ajouter des utilisateurs

### Création / Modification d'une liste

- [x] Nommer / Renommer la liste
- [x] Ajouter des éléments
- [x] Retirer des éléments
- [x] Modifier des éléments
- [x] Possibilité de choisir état par défaut de la liste (publique: tous mes contacts peuvent la voir, privé : uniquement les contacts choisit peuvent la voir)

### Édition d'un élément

- [x] Nom de l'élément
- [x] Possibilité d'ajouter un lien
- [x] Possibilité d'ajouter des coordonnées GPS (via une adresse) pour indiquer ou trouver le produit
- [x] Prix
- [x] État de l'élément (idea, crush, reserved, purchased)
- [x] Description / Notes

### Connexion à un compte 

- [x] Création compte
- [x] Connexion à un compte existant 
- [x] Déconnexion si connecté

### Profil

- [x] Nom de l'utilisateur
- [x] Consultation de l'email
- [x] modification de l'email
- [x] modification du mot de passe
- [x] modification du nom d'utilisateur 
- [x] Consultation avatar (Gravatar)
- [x] Identifiant unique (soit le nom d'utilisateur qui doit être unique, soit un code, soit un tag après le pseudo comme discord ou battle.net)
- [x] Profil publique ou privée pour savoir si il apparait dans les recherches de contact

### Liste de contact

- [x] Ajouter un contact : via son identifiant unique ou  système de recherche
- [x] Supprimer un contact
- [x] Décliner une demande d'un contact
- [x] Voir les détails d'un contact et ses statistiques
- [x] Voir les listes d'un contacts

### Liste appartenant a un contact

- [x] Changer d'état un élément (pour indiquer que l'on va ou que l'a acheté)
- [x] Commenter (un élément ou la liste)

### Paramètre

- [x] Changer la couleur de l'appli
- [ ] Mode clair / sombre 
- [ ] Possibilité de verrouiller l'application au demarage

### Zone Notification

- [x] Demande de contact

## Technologie

Voici les principales technologies et framework utilisé dans le projet :
- Java 8 : Langage de programmation
- GSON : Parser JSON
- Volley : Requete  HTTP
- Glide : Gestion des images
- Saripaar : Validation données 

## Paramètrage du serveur

L'url du serveur doit être précisé dans la classe [Constants](https://gitlab.univ-artois.fr/arthur_brandao/wishurgift/-/blob/master/app/src/main/java/com/androiddev/project/ili/wishurgift/util/Constants.java), il suffit de modifier la constante API_URL

## Autre projet Wishurgift

Voici la liste des differents projets Wishurgift :
- [Le serveur](https://git.loquico.me/Artois/Wishurgift/src/master/server)
- [Le mock server](https://git.loquico.me/Artois/Wishurgift/src/master/mock-server)
