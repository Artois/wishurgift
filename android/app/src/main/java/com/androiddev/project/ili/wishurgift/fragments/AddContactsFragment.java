package com.androiddev.project.ili.wishurgift.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.ContactsApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.google.gson.Gson;

import org.json.JSONObject;

public class AddContactsFragment extends Fragment {


    Gson gson = new Gson();
    private ProgressDialog pg;
    TextView tag;
    private Button addContactButton, cancelButton;

    public AddContactsFragment() {

    }

    public static AddContactsFragment newInstance() {
        AddContactsFragment fragmentContact = new AddContactsFragment();
        return fragmentContact;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(Wishurgift.getSessionVariables().getUserConnected()==null || Wishurgift.getSessionVariables().getToken()==null){
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
        View view = inflater.inflate(R.layout.fragment_add_contact, container, false);
        tag = view.findViewById(R.id.tag_contact);
        addContactButton = view.findViewById(R.id.addContact);
        addContactButton.setOnClickListener(v -> AddContactBytag());
        cancelButton = view.findViewById(R.id.cancelAddContact);
        cancelButton.setOnClickListener(v -> exitAddContact());

        return view;
    }

    private void exitAddContact() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Confirm exit");
        alertDialogBuilder.setMessage("Are you sure you want to exit");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> {

            ((AppMainActivity) getActivity()).afficherListesContacts();
        });

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> dialog.cancel());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void onPreExecute() {
        pg = new ProgressDialog(getContext(), R.style.AppTheme_Dark_Dialog);
        pg.setTitle("Processing...");
        pg.setMessage("Please wait.");
        pg.setCancelable(false);
        pg.setIndeterminate(true);
        pg.show();
    }
    public void AddContactBytag() {
        onPreExecute();
        ContactsApi.addContactByTag(tag.getText().toString(), (Response.Listener<JSONObject>) response -> {
            pg.dismiss();
            ((AppMainActivity) getActivity()).afficherListesContacts();
        }, error -> {
            pg.dismiss();
            AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle("An error has occured");
            alertDialogBuilder.setMessage("contact does not exist").setCancelable(false).setNeutralButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((AppMainActivity) getActivity()).afficherListesContacts();
                }
            });

            AlertDialog alertDialog =alertDialogBuilder.create();
            alertDialog.show();
        });
    }


    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("User not found");
        alertDialogBuilder.setMessage("We were unable to find this user").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                /*Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getActivity()).finish();
                getActivity().startActivity(new Intent(getContext(), LoginActivity.class));*/
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }
}
