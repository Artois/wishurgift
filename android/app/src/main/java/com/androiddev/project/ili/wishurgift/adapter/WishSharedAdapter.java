package com.androiddev.project.ili.wishurgift.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.model.WishList;

import java.util.List;

public class WishSharedAdapter extends ArrayAdapter<WishList> {
    public WishSharedAdapter(Context context, List<WishList> listes) {
        super(context, 0, listes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_shared_list, parent, false);
        }
        final View listItem=convertView;
        final WishList wishList=getItem(position);
        TextView nom= listItem.findViewById(R.id.nomListeShared);
        TextView owner= listItem.findViewById(R.id.nomOwner);
        TextView stats= listItem.findViewById(R.id.statsWishlistShared);
        nom.setText(wishList.getName());
        owner.setText(wishList.getUser().getTag());
        stats.setText("Total : " + wishList.getStats().getTotal() + " Non acheté : " + wishList.getStats().getUnbuy());
        return convertView;
    }
}
