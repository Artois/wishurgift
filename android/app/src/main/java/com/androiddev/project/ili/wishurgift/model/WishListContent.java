package com.androiddev.project.ili.wishurgift.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishListContent {
    private int id;
    private String name;
    @SerializedName("public")
    private Boolean is_public;
    @SerializedName("size")
    private WishStats stats;
    @SerializedName("element")
    private List<Element> elements;

    public WishListContent(int id, String name, Boolean is_public, WishStats stats, List<Element> elements) {
        this.id = id;
        this.name=name;
        this.is_public = is_public;
        this.stats = stats;
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getIs_public() {
        return is_public;
    }

    public void setIs_public(Boolean is_public) {
        this.is_public = is_public;
    }

    public WishStats getStats() {
        return stats;
    }

    public void setStats(WishStats stats) {
        this.stats = stats;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }
}
