package com.androiddev.project.ili.wishurgift.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.WishListApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddWishListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddWishListFragment extends Fragment {
    CheckBox checkPublic;
    EditText titleWishlist;
    Button addImageButton,cancelButton;
    private ProgressDialog pg;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment AddWishListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddWishListFragment newInstance() {
        AddWishListFragment fragment = new AddWishListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(Wishurgift.getSessionVariables().getUserConnected()==null || Wishurgift.getSessionVariables().getToken()==null){
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
        View view= inflater.inflate(R.layout.fragment_add_wish_list, container, false);
        // Inflate the layout for this fragment
        checkPublic=view.findViewById(R.id.chkPublic);
        titleWishlist=view.findViewById(R.id.titleWishlist);
        addImageButton=view.findViewById(R.id.addWishlist);
        addImageButton.setOnClickListener(v -> createWishList());
        cancelButton = view.findViewById(R.id.cancelAddwishlist);
        cancelButton.setOnClickListener(v -> exitButton());
        return view;
    }

    protected void onPreExecute() {
        pg = new ProgressDialog(getContext(), R.style.AppTheme_Dark_Dialog);
        pg.setTitle("Processing...");
        pg.setMessage("Please wait.");
        pg.setCancelable(false);
        pg.setIndeterminate(true);
        pg.show();
    }

    /**
     * creation of a wishlist
     */
    public void createWishList(){
        onPreExecute();
        WishListApi.createWishList(titleWishlist.getText().toString(), checkPublic.isChecked(), (Response.Listener<JSONObject>) response -> {
            //response
            pg.dismiss();
            returnToList();
        }, error -> {
            onError();
        });
    }

    public void returnToList(){
        ((AppMainActivity) getActivity()).afficherWishListList();
    }


    public void exitButton(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Confirm exit");
        alertDialogBuilder.setMessage("Are you sure you want to exit");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> returnToList());

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> dialog.cancel());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getActivity()).finish();
                getActivity().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }
}
