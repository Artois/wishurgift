package com.androiddev.project.ili.wishurgift.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.ContactsApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.model.User;
import com.bumptech.glide.Glide;

import java.util.List;

public class ContactPendingAdapter extends ArrayAdapter<User> {
    public ContactPendingAdapter(Context context, List<User> listes) {
        super(context, 0, listes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_contact_request, parent, false);
        }
        final View listItem = convertView;
        final User contact = getItem(position);
        TextView nom = listItem.findViewById(R.id.nomContactRequest);
        TextView tag = listItem.findViewById(R.id.contactRequest_tag);
        ImageView img = listItem.findViewById(R.id.imageContactRequest);
        Glide.with(getContext()).load(contact.getAvatar()).into(img);
        nom.setText(contact.getUserName());
        tag.setText(contact.getTag());
        listItem.findViewById(R.id.addButtonContact).setOnClickListener(v -> {
            ContactsApi.addContactByTag(contact.getTag(), response -> {
                ((AppMainActivity) getContext()).afficherListesContacts();

            }, error -> onError());
        });

        listItem.findViewById(R.id.declineRequest).setOnClickListener(v -> ContactsApi.deleteContact(contact.getId(), response -> {
            Toast.makeText(getContext(), "friend request rejected", Toast.LENGTH_SHORT).show();
            ((AppMainActivity) getContext()).afficherListesContacts();
        }, error -> onError()));
        return convertView;
    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getContext()).finish();
                getContext().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }
}
