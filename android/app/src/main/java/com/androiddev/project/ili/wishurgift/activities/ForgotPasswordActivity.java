package com.androiddev.project.ili.wishurgift.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androiddev.project.ili.wishurgift.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    Toolbar toolbar;
    ProgressBar progressBar;
    EditText userEmail;
    Button userPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progressBar);
        userEmail = findViewById(R.id.userEmail);
        userPass = findViewById(R.id.btnForgotPass);

        toolbar.setTitle("Mot de passe oublié");
        userPass.setOnClickListener(v -> Toast.makeText(ForgotPasswordActivity.this, "Pas encore implémenté coté back ...", Toast.LENGTH_SHORT).show());
    }
}
