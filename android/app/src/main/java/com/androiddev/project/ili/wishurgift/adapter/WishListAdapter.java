package com.androiddev.project.ili.wishurgift.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.WishListApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.model.WishList;

import java.util.List;

public class WishListAdapter extends ArrayAdapter<WishList> {
    private View popupInputDialogView = null;

    private Button saveNewWishlist, cancelEditWishlist;
    private TextView newName;
    private CheckBox newWishlistVisibility;

    public WishListAdapter(Context context, List<WishList> listes) {
        super(context, 0, listes);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_user, parent, false);
        }
        final View listItem = convertView;
        final WishList wishList = getItem(position);
        TextView nom = listItem.findViewById(R.id.nomListe);
        TextView stats = listItem.findViewById(R.id.statsWishlist);
        nom.setText(wishList.getName());
        stats.setText("Total : " + wishList.getStats().getTotal() + " Non acheté : " + wishList.getStats().getUnbuy());
        listItem.findViewById(R.id.deleteButtonWishlist).setOnClickListener(v -> {
            WishListApi.deleteWishList(wishList.getId(), response -> {
                ((AppMainActivity) getContext()).afficherWishListList();
            }, error -> onError());
        });

        listItem.findViewById(R.id.updateButtonWishlist).setOnClickListener(v -> editWishlistPopup(wishList.getId()));
        return convertView;
    }


    private void editWishlistPopup(int id) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Edit Wishlist ");
        alertDialogBuilder.setIcon(R.drawable.ic_launcher_background);
        alertDialogBuilder.setCancelable(false);
        initPopupViewControls();
        alertDialogBuilder.setView(popupInputDialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        saveNewWishlist.setOnClickListener(view -> {
            saveChangeWishlist(id);
            alertDialog.cancel();
        });

        cancelEditWishlist.setOnClickListener(view -> alertDialog.cancel());


    }

    private void saveChangeWishlist(int id) {
        WishListApi.updateWishList(id, newName.getText().toString(), newWishlistVisibility.isChecked(), response -> afficherDashboard(id,true), error -> onError());
    }

    private void afficherDashboard(int id, boolean appartient) {
        ((AppMainActivity) getContext()).afficherWishListList();

    }

    private void initPopupViewControls() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        popupInputDialogView = layoutInflater.inflate(R.layout.popup_edit_wishlist_dialog, null);
        newName = popupInputDialogView.findViewById(R.id.newname_popup);
        newWishlistVisibility = popupInputDialogView.findViewById(R.id.newwishlist_chkpublic);
        saveNewWishlist = popupInputDialogView.findViewById(R.id.button_save_newWishlist);
        cancelEditWishlist = popupInputDialogView.findViewById(R.id.button_cancel_editWishlist);

    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getContext()).finish();
                getContext().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }

}
