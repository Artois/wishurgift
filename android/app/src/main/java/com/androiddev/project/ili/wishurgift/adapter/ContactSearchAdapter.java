package com.androiddev.project.ili.wishurgift.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.ContactsApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.model.User;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Ouahab FENNICHE on 26/04/2020
 */
public class ContactSearchAdapter extends ArrayAdapter<User> {
    public ContactSearchAdapter(Context context, List<User> listes) {
        super(context, 0, listes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_contact_search, parent, false);
        }
        final View listItem = convertView;
        final User contact = getItem(position);
        TextView nom = listItem.findViewById(R.id.nomContactFound);
        TextView email = listItem.findViewById(R.id.contact_emailFound);
        TextView tag = listItem.findViewById(R.id.contact_tagFound);
        ImageView image = listItem.findViewById(R.id.imageContactFound);
        Glide.with(getContext()).load(contact.getAvatar()).into(image);
        nom.setText(contact.getUserName());
        email.setText(contact.getEmail());
        tag.setText(contact.getTag());
        listItem.findViewById(R.id.addButtonContactFound).setOnClickListener(v -> ContactsApi.addContactByTag(contact.getTag(), response -> {
            ((AppMainActivity) getContext()).afficherListesContacts();

        }, error -> onError()));


        return convertView;
    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getContext()).finish();
                getContext().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }
}
