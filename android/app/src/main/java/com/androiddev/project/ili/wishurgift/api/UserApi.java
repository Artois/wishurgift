package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UserApi {
    /*
     * @param id Id utilisateur
     * @param listener listener
     * @param errorListener listener d'erreur
     * Appel à l'api pour l'utilisateur passé en paramètre.
     * En cas de succès, la méthode onSuccess du listener est executé.
     * En cas d'erreur methode onError de l'errorListener est executé
     */
    public static void getWishListsFromUser(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.USER_LIST_WISHLIST, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void getUserProfile(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.USER_PROFILE, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }


    public static void getUsersBySearchWithLimit(String searching, int capacity, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.USER_SEARCH_WITH_LIMIT, searching, capacity), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void getWishListsFromUserPaginable(int id, int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.USER_LIST_WISHLIST_PAGINABLE, id, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void login(String login, String password, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("login", login);
            params.put("password", password);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + Constants.LOGIN, new HashMap<String, String>(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void signIn(String name, String email, String password, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("name", name);
            params.put("password", password);
            params.put("email", email);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + Constants.SIGN_IN, new HashMap<String, String>(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void refreshToken(Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.PUT, Constants.API_URL + Constants.REFRESH_TOKEN, AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void updateUser(String name, String email, String password, Boolean isPublic, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            if (name != null && !name.isEmpty()) {
                params.put("name", name);
            }
            if (email != null && !email.isEmpty()) {
                params.put("email", email);
            }
            if (password != null && !password.isEmpty()) {
                params.put("password", password);
            }
            if (isPublic != null) {
                params.put("public", isPublic);
            }
            AppelApi.getJSONObjectWithParams(Request.Method.PUT, Constants.API_URL + String.format(Constants.UPDATE_USER), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getSharedListFromUser(Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + Constants.USER_SHARED_LIST, AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    // Returns a paginated list of shared wish lists
    public static void getWishListsSharedFromUserPaginable(int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.USER_SHARED_LIST_PAGINABLE, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }


}
