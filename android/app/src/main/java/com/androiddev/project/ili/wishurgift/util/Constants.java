package com.androiddev.project.ili.wishurgift.util;

/**
 * Created by Ouahab FENNICHE on 2020-01-25
 */


public class Constants {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "register.db";
    public static final String TABLE_NAME = "registerUser";


    //Table Columns
    public static final String KEY_ID = "id";
    public static final String KEY_USERNAME = "userName";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";

    //API
    public static final String API_URL = "http://wishurgift.loquico.me/";

    //Users
    //GET
    public static final String USER_LIST_WISHLIST = "users/%d/wishlists";
    public static final String USER_PROFILE = "users/%d";
    public static final String USER_SEARCH_WITH_LIMIT = "users/search/%s/%d";
    public static final String USER_LIST_WISHLIST_PAGINABLE = "users/%d/wishlists/page/%d/%d";
    public static final String USER_SHARED_LIST = "users/wishlists/shared";
    public static final String USER_SHARED_LIST_PAGINABLE = "users/%d/shared/page/%d/%d";
    //POST
    public static final String LOGIN = "users/login";
    public static final String SIGN_IN = "users";
    //PUT
    public static final String REFRESH_TOKEN = "users/refresh";
    public static final String UPDATE_USER = "users";

    //Wishlist
    //GET
    public static final String WISHLIST = "wishlists/%d";
    //POST
    public static final String ADD_WISHLIST = "wishlists";
    public static final String ADD_ITEM_WISHLIST = "wishlists/%d/element";
    //PUT
    public static final String UPDATE_WISHLIST = "wishlists/%d";
    public static final String SHARE_WISHLIST = "wishlists/%d/share/%d";
    //DELETE
    public static final String DELETE_WISHLIST = "wishlists/%d";
    public static final String UNDOSHARE_WISHLIST = "wishlists/%d/share/%d";

    //Elements
    //GET
    public static final String ELEMENT = "elements/%d";
    public static final String ELEMENT_COMMENTS = "elements/%d/comments";
    public static final String ELEMENT_COMMENTS_PAGINABLE = "elements/%d/comments/page/%d/%d";
    //POST
    public static final String ADD_IMG = "elements/%d/image";
    public static final String ADD_COMMENT = "elements/%d/comment";
    //PUT
    public static final String UPDATE_ELEMENT = "elements/%d";
    public static final String UPDATE_STATUS = "elements/%d/status/%s";
    //DELETE
    public static final String DELETE_ELEMENT = "elements/%d";

    //Contacts
    //GET
    public static final String CONTACT_LIST = "/contacts";
    public static final String CONTACT_LIST_PAGINABLE = "/contacts/page/%d/%d";
    public static final String CONTACTS_PENDING = "/contacts/pending";
    public static final String CONTACTS_PENDING_PAGINABLE = "/contacts/pending/page/%d/%d";
    public static final String CONTACTS_REQUESTED = "contacts/request";
    public static final String CONTACTS_REQUESTED_PAGINABLE = "/contacts/request/page/%d/%d";
    //POST
    public static final String ADD_CONTACT = "contacts";
    //DELETE
    public static final String DELETE_CONTACTS = "contacts/%d";

    // Commentaire
    //GET
    public static final String COMMENT = "comments/%d";
    //PUT
    public static final String UPDATE_COMMENT = "comments/%d";
    //DELETE
    public static final String DELETE_COMMENT = "comments/%d";


    // Stat
    //GET
    public static final String STATS = "stats/user/%d";

}