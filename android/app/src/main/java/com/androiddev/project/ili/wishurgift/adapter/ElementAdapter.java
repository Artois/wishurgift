package com.androiddev.project.ili.wishurgift.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.ElementsApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.model.Element;

import java.util.List;

public class ElementAdapter extends ArrayAdapter<Element> {
    private boolean appartient;
    private int idParent;

    public ElementAdapter(Context context, List<Element> listes, boolean appartient, int idParent) {
        super(context, 0, listes);
        this.appartient = appartient;
        this.idParent = idParent;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_content, parent, false);
        }
        View listItem = convertView;
        final Element elt = getItem(position);
        TextView nom = listItem.findViewById(R.id.nomElement);
        TextView status = listItem.findViewById(R.id.statusElement);
        listItem.findViewById(R.id.deleteButtonElement).setOnClickListener(v -> ElementsApi.deleteElement(elt.getId(), response -> showWishlistAfterDeletingElement(elt.getId(), true), error -> onError()));
        nom.setText(elt.getName());
        status.setText(elt.getStatus());


        return convertView;
    }

    private void showWishlistAfterDeletingElement(int id, boolean appartient) {
        ((AppMainActivity) getContext()).afficherContenuWishList(idParent, appartient);
    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getContext()).finish();
                getContext().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }

}
