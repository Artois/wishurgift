package com.androiddev.project.ili.wishurgift.model;

import com.google.gson.annotations.SerializedName;

public class WishList {
    private int id;
    private String name;
    @SerializedName("public")
    private Boolean isPublic;
    private User user;
    @SerializedName("size")
    private WishStats stats;

    public WishList(int id, String name, Boolean isPublic, WishStats stats, User user) {
        this.id = id;
        this.name = name;
        this.isPublic = isPublic;
        this.stats = stats;
        this.user=user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public WishStats getStats() {
        return stats;
    }

    public void setStats(WishStats stats) {
        this.stats = stats;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
