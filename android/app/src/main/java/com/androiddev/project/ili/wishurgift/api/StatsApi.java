package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

/**
 * Created by Ouahab FENNICHE on 22/03/2020
 */
public class StatsApi {

    // GET
    //Statistique sur un utilisateur (l'utilisateur connecté peut uniquement accèder au statistique de ses contacts ou de lui même)
    public static void getStatsById(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.STATS, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }
}
