package com.androiddev.project.ili.wishurgift.model;

public class WishStats {

    private int total;
    private int unbuy;
    public WishStats(int total,int unbuy){
        this.total=total;
        this.unbuy=unbuy;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUnbuy() {
        return unbuy;
    }

    public void setUnbuy(int unbuy) {
        this.unbuy = unbuy;
    }
}
