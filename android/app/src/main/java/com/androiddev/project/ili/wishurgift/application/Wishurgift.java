package com.androiddev.project.ili.wishurgift.application;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.androiddev.project.ili.wishurgift.util.SessionVariables;

public class Wishurgift extends Application {
    private static Wishurgift mInstance;
    private ActivityLifecycleCallbacks mLifeCycle;
    private RequestQueue mRequestQueue;
    private static final SessionVariables sessionVariables=new SessionVariables();

    @Override
    public void onCreate(){
        super.onCreate();

        mInstance=this;
        mRequestQueue= Volley.newRequestQueue(getApplicationContext());
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {

        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? "log" : tag);
        mInstance.mRequestQueue.add(req);
    }

    public static Wishurgift get(){
        return mInstance;
    }
    public static SessionVariables getSessionVariables(){
        return sessionVariables;
    }
}
