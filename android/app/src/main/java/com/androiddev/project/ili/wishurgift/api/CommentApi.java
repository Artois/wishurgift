package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ouahab FENNICHE on 19/04/2020
 */
public class CommentApi {

    //GET
    //Récupère les données d'un commentaire
    public static void getCommentsByID(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.COMMENT, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    //PUT
    //Modifie un commentaire
    public static void updateComment(int idComment,  String text,  Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            if(text!= null && !text.isEmpty()) {
                params.put("text", text);
            }


            AppelApi.getJSONObjectWithParams(Request.Method.PUT, Constants.API_URL + String.format(Constants.UPDATE_COMMENT, idComment), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //DELETE
    //Supprime un commentaire
    public static void deleteComment(int idComment, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.DELETE, Constants.API_URL + String.format(Constants.DELETE_COMMENT, idComment), AppelApi.getHeaderToken(), listener, errorListener, false);
    }
}
