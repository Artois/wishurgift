package com.androiddev.project.ili.wishurgift.util;

import com.androiddev.project.ili.wishurgift.model.User;

public class SessionVariables {
    // @Todo sauvegarder ici l'utilisateur de la session
    private User userConnected = new User(1, "toto", "mail@mail.mail", "avatar", true, "toto#1234", null);

    private String token;

    public User getUserConnected() {
        return userConnected;
    }

    public void setUserConnected(User userConnected) {
        this.userConnected = userConnected;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
