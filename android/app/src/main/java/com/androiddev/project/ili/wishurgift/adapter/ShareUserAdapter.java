package com.androiddev.project.ili.wishurgift.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.model.User;
import com.bumptech.glide.Glide;

import java.util.List;

public class ShareUserAdapter extends ArrayAdapter<User> {
    public ShareUserAdapter(Context context, List<User> listes) {
        super(context, 0, listes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_sharable, parent, false);
        }
        final View listItem=convertView;
        final User contact=getItem(position);
        TextView nom= listItem.findViewById(R.id.nomContact);
        TextView tag= listItem.findViewById(R.id.tagContact);
        ImageView img= listItem.findViewById(R.id.imageContactShare);
        nom.setText(contact.getUserName());
        tag.setText(contact.getTag());
        Glide.with(getContext()).load(contact.getAvatar()).into(img);
        return convertView;
    }
}
