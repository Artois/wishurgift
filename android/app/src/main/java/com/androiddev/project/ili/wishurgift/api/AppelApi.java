package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class AppelApi {

    private static int REQUEST_TIMEOUT_MS = 10000;

    /**
     * Fait un appel HTTP à l'url indiqué, selon le resultat execute le listener ou l'errorListener associé
     * @param method Méthode HTTP
     * @param url URL associé
     * @param headers Le header rattaché à la requête http
     * @param listener Le listener associé
     * @param errorListener Le listener d'erreur
     * @param putInCache Permet de sauvegarder en cache la réponse de la requète
     */
    public static void getString(
            int method,
            String url,
            final Map<String, String> headers,
            Response.Listener<String> listener,
            Response.ErrorListener errorListener,
            boolean putInCache)
    {
        StringRequest jsonObjReq = new StringRequest(method, url, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headers == null) {
                    return new LinkedHashMap<>(0);
                } else {
                    return headers;
                }
            }
        };

        jsonObjReq.setShouldCache(putInCache);
        addRequestPolicy(jsonObjReq);

        // Adding request to request queue
        Wishurgift.get().addToRequestQueue(jsonObjReq, "req_object_" + url.hashCode());
    }

    public static void addRequestPolicy(Request request)
    {
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        REQUEST_TIMEOUT_MS, // 10 000
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
    }

    /**
     * @return le token de connexion
     */
    public static HashMap<String,String> getHeaderToken(){
        HashMap<String,String> header = new HashMap<>();
        header.put("Authorization","Bearer " + Wishurgift.getSessionVariables().getToken());
        return header;
    }

    /**
     * Fait un appel HTTP à l'url indiqué, selon le resultat execute le listener ou l'errorListener associé
     * @param method Méthode HTTP
     * @param url URL associé
     * @param headers Le header rattaché à la requête http
     * @param params les paramètres associés à l'url
     * @param listener Le listener associé
     * @param errorListener Le listener d'erreur
     * @param putInCache Permet de sauvegarder en cache la réponse de la requète
     */
    public static void getJSONObjectWithParams(int method,
                                               String url,
                                               final Map<String, String> headers,
                                               final JSONObject params,
                                               Response.Listener<JSONObject> listener,
                                               Response.ErrorListener errorListener,
                                               boolean putInCache) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method, url, params, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headers == null) {
                    return new LinkedHashMap<>(0);
                }
                return headers;
            }
        };

        jsonObjReq.setShouldCache(putInCache);
        addRequestPolicy(jsonObjReq);

        // Adding request to request queue
        Wishurgift.get().addToRequestQueue(jsonObjReq, "req_object_" + url.hashCode());
    }
}
