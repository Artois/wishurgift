package com.androiddev.project.ili.wishurgift.activities;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.fragments.AddContactsFragment;
import com.androiddev.project.ili.wishurgift.fragments.AddElementFragment;
import com.androiddev.project.ili.wishurgift.fragments.AddWishListFragment;
import com.androiddev.project.ili.wishurgift.fragments.EditElementFragment;
import com.androiddev.project.ili.wishurgift.fragments.EditProfilFragment;
import com.androiddev.project.ili.wishurgift.fragments.ElementFragment;
import com.androiddev.project.ili.wishurgift.fragments.ListeContactsFragment;
import com.androiddev.project.ili.wishurgift.fragments.MyListFragment;
import com.androiddev.project.ili.wishurgift.fragments.ProfilFragment;
import com.androiddev.project.ili.wishurgift.fragments.SettingsFragment;
import com.androiddev.project.ili.wishurgift.fragments.ShareListFragment;
import com.androiddev.project.ili.wishurgift.fragments.ShowContactDetails;
import com.androiddev.project.ili.wishurgift.fragments.WishListContentFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * Activité regroupant l'affichage de toutes les actions utilisateur possibles une fois connecté
 */
public class AppMainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    private ProgressDialog pg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_list_test);
        Fragment fragment = MyListFragment.newInstance();
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainerList, fragment).commit();
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            Fragment fragment1 = null;

            switch (menuItem.getItemId()) {
                case R.id.dashboardfragment:
                    fragment1 = MyListFragment.newInstance();
                    break;

                case R.id.profilesfragment:
                    fragment1 = ProfilFragment.newInstance(Wishurgift.getSessionVariables().getUserConnected().getId());
                    break;
                case R.id.contactsfragment:
                    fragment1 = ListeContactsFragment.newInstance();
                    break;


                case R.id.settingsfragment:
                    fragment1 = SettingsFragment.newInstance();
                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerList, fragment1).commit();
            return true;
        });
    }

    /**
     * Permet d'afficher le fragment correspondant à la liste de contact de l'utilisateur connecté
     */
    public void afficherListesContacts(){
        Fragment fragment=null;
        try {
            fragment= ListeContactsFragment.newInstance();
        } catch (Exception e){
            e.printStackTrace();
        }
        FragmentManager frM= getSupportFragmentManager();
        frM.beginTransaction().replace(R.id.fragmentContainerList,fragment).commit();
    }

    /**
     * Affiche le fragment correspondant au contenu d'une wishlist
     * @param idWishlist l'identifiant de la wishlist à afficher
     * @param appartient indique si la wishlist appartient à l'utilisateur connecté
     */
    public void afficherContenuWishList(int idWishlist, boolean appartient) {
        Fragment fragment = null;
        try {
            fragment = WishListContentFragment.newInstance(idWishlist, appartient);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment correspondant au panneau d'affichage des wishlists
     */
    public void afficherWishListList() {
        Fragment fragment = null;
        try {
            fragment = MyListFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment permettant de créer une wishlist
     */
    public void afficherAjouterWishList() {
        Fragment fragment = null;
        try {
            fragment = AddWishListFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment permettant l'ajout d'un élément dans une wishlist
     * @param idWishlist la wishlist à laquelle on ajoute un élément
     */
    public void afficherAjouterElement(int idWishlist) {
        Fragment fragment = null;
        try {
            fragment = AddElementFragment.newInstance(idWishlist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment correspondant au détail d'un élément
     * @param wishlistId La wishlist auquel l'élément est rattaché
     * @param elementId L'élément à afficher
     * @param appartient Indique si la wishlist appartient à l'utilisateur connecté
     */
    public void afficherElement(int wishlistId, int elementId, boolean appartient) {
        Fragment fragment = null;
        try {
            fragment = ElementFragment.newInstance(wishlistId, elementId, appartient);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }


    /**
     * Affiche le fragment correspondant à l'ajout d'un contact
     */
    public void afficherAjoutContact() {
        Fragment fragment = null;
        try {
            fragment = AddContactsFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment correspondant au profil de l'utilisateur
     * @param idUtilisateur L'utilisateur à afficher
     */
    public void afficherProfil(int idUtilisateur) {
        Fragment fragment = null;
        try {
            fragment = ProfilFragment.newInstance(idUtilisateur);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment correspondant à la mise a jour du profil
     * @param idUtilisateur L'id de l'utilisateur à mettre à jour
     */
    public void updateProfil(int idUtilisateur) {
        Fragment fragment = null;
        try {
            fragment = EditProfilFragment.newInstance(idUtilisateur);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment permettant le partage d'une liste à un utilisateur
     * @param idWishlist l'id de la wishlist à partager
     */
    public void afficherShareList(int idWishlist) {
        Fragment fragment = null;
        try {
            fragment = ShareListFragment.newInstance(idWishlist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment permettant la mise à jour d'un élément
     * @param wishlistParentId l'id de la wishlist rattachée à l'élément
     * @param idElement l'id de l'élément à mettre à jour
     */
    public void updateElement(int wishlistParentId, int idElement) {

        Fragment fragment = null;
        try {
            fragment = EditElementFragment.newInstance(wishlistParentId, idElement);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }

    /**
     * Affiche le fragment correspondant au profil d'un contact
     * @param idContact L'identifiant du contact à afficher
     */
    public void afficherDétailsContact(int idContact) {
        Fragment fragment = null;
        try {
            fragment = ShowContactDetails.newInstance(idContact);
        } catch (Exception e){
            e.printStackTrace();
        }
        FragmentManager fr = getSupportFragmentManager();
        fr.beginTransaction().replace(R.id.fragmentContainerList, fragment).commit();
    }
}
