package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class WishListApi {
    public static void getWishListById(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.WISHLIST, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void createWishList(String name, Boolean is_public, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("name", name);
            params.put("public", is_public);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + String.format(Constants.ADD_WISHLIST), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Supprime une liste
    public static void deleteWishList(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.DELETE, Constants.API_URL + String.format(Constants.DELETE_WISHLIST, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    // UNDOSHARE_WISHLIST
    public static void undoSharingWishList(int id, int idUser, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.DELETE, Constants.API_URL + String.format(Constants.UNDOSHARE_WISHLIST, id, idUser), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void addElementToWishList(int id, String name, String link, String address, Double price, String description, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("name", name);

            if (link != null && !link.isEmpty()) {
                params.put("link", link);
            }
            if (address != null && !address.isEmpty()) {
                params.put("address", address);
            }
            if (price != null) {
                params.put("price", price);
            }
            if (description != null && !description.isEmpty()) {
                params.put("description", description);
            }
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + String.format(Constants.ADD_ITEM_WISHLIST, id), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateWishList(int id, String name, Boolean is_public, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            if (name != null && !name.isEmpty()) {
                params.put("name", name);
            }
            if (is_public != null) {
                params.put("public", is_public);
            }


            AppelApi.getJSONObjectWithParams(Request.Method.PUT, Constants.API_URL + String.format(Constants.UPDATE_WISHLIST, id), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void shareWishList(int idWishList, int idUser, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.PUT, Constants.API_URL + String.format(Constants.SHARE_WISHLIST, idWishList, idUser), AppelApi.getHeaderToken(), listener, errorListener, false);
    }
}
