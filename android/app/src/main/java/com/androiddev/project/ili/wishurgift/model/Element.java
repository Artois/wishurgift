package com.androiddev.project.ili.wishurgift.model;

import java.util.List;

public class Element {
    private int id;
    private String name;
    private String status;
    private String link;
    private List<Image> image;
    private String address;
    private float price;
    private String description;

    public Element(int id, String name, String status, String link, List<Image> image, String address, float price, String description) {
        this.id = id;
        this.name=name;
        this.status = status;
        this.link = link;
        this.image = image;
        this.address = address;
        this.price = price;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }
}
