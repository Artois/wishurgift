package com.androiddev.project.ili.wishurgift.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ouahab FENNICHE on 28/04/2020
 */
public class ContactStats {
    private int id;
    @SerializedName("wishlist")
    private int nbWishlist;
    @SerializedName("element")
    private int nbElement;
    @SerializedName("contact")
    private int nbContact;
    @SerializedName("idea")
    private int nbIdea;
    @SerializedName("crush")
    private int nbCrush;
    @SerializedName("reserved")
    private int nbReserved;
    @SerializedName("purchased")
    private int nbPurchased;
    @SerializedName("averagePrice")
    private double nbAveragePrice;
    @SerializedName("lowestPrice")
    private int nbLowestPrice;
    @SerializedName("highestPrice")
    private int nbHighestPrice;


    public ContactStats(int id, int nbWishlist, int nbElement, int nbContact, int nbIdea, int nbCrush, int nbReserved, int nbPurchased, double nbAveragePrice, int nbLowestPrice, int nbHighestPrice) {
        this.id = id;
        this.nbWishlist = nbWishlist;
        this.nbElement = nbElement;
        this.nbContact = nbContact;
        this.nbIdea = nbIdea;
        this.nbCrush = nbCrush;
        this.nbReserved = nbReserved;
        this.nbPurchased = nbPurchased;
        this.nbAveragePrice = nbAveragePrice;
        this.nbLowestPrice = nbLowestPrice;
        this.nbHighestPrice = nbHighestPrice;
    }

    public int getNbContact() {
        return nbContact;
    }

    public void setNbContact(int nbContact) {
        this.nbContact = nbContact;
    }

    public int getNbElement() {
        return nbElement;
    }

    public void setNbElement(int nbElement) {
        this.nbElement = nbElement;
    }

    public int getNbWishlist() {
        return nbWishlist;
    }

    public void setNbWishlist(int nbWishlist) {
        this.nbWishlist = nbWishlist;
    }

    public int getNbIdea() {
        return nbIdea;
    }

    public void setNbIdea(int nbIdea) {
        this.nbIdea = nbIdea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNbCrush() {
        return nbCrush;
    }

    public void setNbCrush(int nbCrush) {
        this.nbCrush = nbCrush;
    }

    public int getNbReserved() {
        return nbReserved;
    }

    public void setNbReserved(int nbReserved) {
        this.nbReserved = nbReserved;
    }

    public int getNbPurchased() {
        return nbPurchased;
    }

    public void setNbPurchased(int nbPurchased) {
        this.nbPurchased = nbPurchased;
    }

    public double getNbAveragePrice() {
        return nbAveragePrice;
    }

    public void setNbAveragePrice(double nbAveragePrice) {
        this.nbAveragePrice = nbAveragePrice;
    }

    public int getNbLowestPrice() {
        return nbLowestPrice;
    }

    public void setNbLowestPrice(int nbLowestPrice) {
        this.nbLowestPrice = nbLowestPrice;
    }

    public int getNbHighestPrice() {
        return nbHighestPrice;
    }

    public void setNbHighestPrice(int nbHighestPrice) {
        this.nbHighestPrice = nbHighestPrice;
    }
}
