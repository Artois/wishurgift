package com.androiddev.project.ili.wishurgift.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.androiddev.project.ili.wishurgift.R;

public class MainActivity extends AppCompatActivity {


    private static int SPLASH_TIME_OUT = 5000;
    // Hooks
    View first, second, third, fourth, fifth, sixth, first_bottom, second_bottom, third_bottom, fourth_bottom, fifth_bottom, sixth_bottom;
    TextView w, slogan, bienvenue;

    // Animations
    Animation topAnimantion, middleAnimantion, bottomAnimantion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        topAnimantion = AnimationUtils.loadAnimation(this, R.anim.top_animantion);
        middleAnimantion = AnimationUtils.loadAnimation(this, R.anim.middle_animantion);
        bottomAnimantion = AnimationUtils.loadAnimation(this, R.anim.bottom_animantion);

        //Hooks
        first = findViewById(R.id.first_line);
        second = findViewById(R.id.second_line);
        third = findViewById(R.id.third_line);
        fourth = findViewById(R.id.fourth_line);
        fifth = findViewById(R.id.fifth_line);
        sixth = findViewById(R.id.sixth_line);

        //Hooks
        first_bottom = findViewById(R.id.first_line_bottom);
        second_bottom = findViewById(R.id.second_line_bottom);
        third_bottom = findViewById(R.id.third_line_bottom);
        fourth_bottom = findViewById(R.id.fourth_line_bottom);
        fifth_bottom = findViewById(R.id.fifth_line_bottom);
        sixth_bottom = findViewById(R.id.sixth_line_bottom);

        w = findViewById(R.id.w);
        slogan = findViewById(R.id.tagLine);
        bienvenue = findViewById(R.id.bienvenue);

        // Setting Animations
        first.setAnimation(topAnimantion);
        second.setAnimation(topAnimantion);
        third.setAnimation(topAnimantion);
        fourth.setAnimation(topAnimantion);
        fifth.setAnimation(topAnimantion);
        sixth.setAnimation(topAnimantion);

        first_bottom.setAnimation(bottomAnimantion);
        second_bottom.setAnimation(bottomAnimantion);
        third_bottom.setAnimation(bottomAnimantion);
        fourth_bottom.setAnimation(bottomAnimantion);
        fifth_bottom.setAnimation(bottomAnimantion);
        sixth_bottom.setAnimation(bottomAnimantion);

        w.setAnimation(middleAnimantion);
        slogan.setAnimation(topAnimantion);
        bienvenue.setAnimation(bottomAnimantion);


        // Splash Screen

        new Handler().postDelayed(() -> {

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading the application...");
            progressDialog.show();
            startActivity(intent);
            progressDialog.dismiss();
            finish();
        }, SPLASH_TIME_OUT);


    }

    private void chargementAppliDialog() {

    }
}
