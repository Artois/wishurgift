package com.androiddev.project.ili.wishurgift.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class ElementsApi {
    public static void getElementByID(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.ELEMENT, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void getElementCommentByID(int id, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.ELEMENT_COMMENTS, id), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void getElementCommentByIDPaginable(int id, int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.ELEMENT_COMMENTS_PAGINABLE, id, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void addImageToElement(int id, String encodedImg, String extension, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("ext", extension);
            params.put("data", encodedImg);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + String.format(Constants.ADD_IMG, id), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void addComment(int idElt, String comment, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("text", comment);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + String.format(Constants.ADD_COMMENT, idElt), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateElement(int idElt, String name, String link, String address, Double price, String description, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            if(name!= null && !name.isEmpty()) {
                params.put("name", name);
            }
            if(link!= null && !link.isEmpty()) {
                params.put("link", link);
            }
            if(address!= null && !address.isEmpty()) {
                params.put("address", address);
            }
            if (price != null  ) {
                params.put("price", price);
            }
            if(description!= null && !description.isEmpty()) {
                params.put("description", description);
            }

            AppelApi.getJSONObjectWithParams(Request.Method.PUT, Constants.API_URL + String.format(Constants.UPDATE_ELEMENT, idElt), AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateStatus(int idElt, String newStatus, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.PUT, Constants.API_URL + String.format(Constants.UPDATE_STATUS, idElt, newStatus), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    public static void deleteElement(int idElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.DELETE, Constants.API_URL + String.format(Constants.DELETE_ELEMENT, idElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }
}
