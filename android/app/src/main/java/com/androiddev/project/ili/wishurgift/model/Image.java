package com.androiddev.project.ili.wishurgift.model;

import com.google.gson.annotations.SerializedName;

public class Image {
    private int id;
    @SerializedName("ext")
    private String extension;
    @SerializedName("data")
    private String dataBase64;

    public Image(int id, String extension, String dataBase64) {
        this.id = id;
        this.extension = extension;
        this.dataBase64 = dataBase64;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDataBase64() {
        return dataBase64;
    }

    public void setDataBase64(String dataBase64) {
        this.dataBase64 = dataBase64;
    }
}
