package com.androiddev.project.ili.wishurgift.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ouahab FENNICHE on 2020-01-25
 */
public class User {
    private int id;
    @SerializedName("name")
    private String userName;
    private String email ;
    private String avatar;
    @SerializedName("public")
    private Boolean isPublic;
    private String tag;
    private transient String password;

    @SerializedName("size")
    private ContactStats stats;

    public User(int id, String userName, String email, String avatar, Boolean isPublic, String tag, ContactStats stats) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.avatar = avatar;
        this.isPublic = isPublic;
        this.tag = tag;
        this.stats= stats;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", tag='" + tag + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public ContactStats getStats() {
        return stats;
    }

    public void setStats(ContactStats stats) {
        this.stats = stats;
    }
}
