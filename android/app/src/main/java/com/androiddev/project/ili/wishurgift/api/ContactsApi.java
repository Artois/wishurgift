package com.androiddev.project.ili.wishurgift.api;


import com.android.volley.Request;
import com.android.volley.Response;
import com.androiddev.project.ili.wishurgift.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactsApi {

    // GET
    //Retourne tous les contacts de l'utilisateur connecté
    public static void getContacts(Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + Constants.CONTACT_LIST, AppelApi.getHeaderToken(), listener, errorListener, false);

    }

    //Retourne les contacts de l'utilisateur connecté sous forme paginée
    public static void getContactsFromUserPaginable(int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.CONTACT_LIST_PAGINABLE, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    //Retourne la liste des utilisateurs ayant ajouté l'utilisateur connecté mais qui ne sont pas dans ses contacts.
    public static void getContactsPending(Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.CONTACTS_PENDING), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    //Retourne la liste paginée des utilisateurs ayant ajouté l'utilisateur connecté mais qui ne sont pas dans ses contacts.
    public static void getContactsNotInListFriendPaginable(int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.CONTACTS_PENDING_PAGINABLE, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    //Retourne la liste des utilisateurs ajoutés par l'utilisateur connecté mais qui ne sont pas dans ses contacts.
    public static void getContactsRequested(Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + Constants.CONTACTS_REQUESTED, AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    //Retourne la liste paginée des utilisateurs ajoutés par l'utilisateur connecté mais qui ne sont pas dans ses contacts.
    public static void GetContactsRequestedPaginable(int numPage, int nbElt, Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.GET, Constants.API_URL + String.format(Constants.CONTACTS_REQUESTED_PAGINABLE, numPage, nbElt), AppelApi.getHeaderToken(), listener, errorListener, false);
    }

    // POST
    public static void addContactByTag(String tag, Response.Listener listener, Response.ErrorListener errorListener) {
        JSONObject params = new JSONObject();
        try {
            params.put("tag", tag);
            AppelApi.getJSONObjectWithParams(Request.Method.POST, Constants.API_URL + Constants.ADD_CONTACT, AppelApi.getHeaderToken(), params, listener, errorListener, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // DELETE
    public static void deleteContact(int user , Response.Listener listener, Response.ErrorListener errorListener) {
        AppelApi.getString(Request.Method.DELETE, Constants.API_URL + String.format(Constants.DELETE_CONTACTS, user), AppelApi.getHeaderToken(), listener, errorListener, false);
    }
}
