package com.androiddev.project.ili.wishurgift.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.androiddev.project.ili.wishurgift.R;
import com.androiddev.project.ili.wishurgift.activities.AppMainActivity;
import com.androiddev.project.ili.wishurgift.activities.LoginActivity;
import com.androiddev.project.ili.wishurgift.api.ContactsApi;
import com.androiddev.project.ili.wishurgift.application.Wishurgift;
import com.androiddev.project.ili.wishurgift.model.User;
import com.bumptech.glide.Glide;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<User> {

    public ContactAdapter(Context context, List<User> listes) {
        super(context, 0, listes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_contact, parent, false);
        }
        final View listItem=convertView;
        final User contact=getItem(position);
        TextView nom= listItem.findViewById(R.id.nomContact);
        TextView tagContact= listItem.findViewById(R.id.contact_tag);
        ImageView imgContact = listItem.findViewById(R.id.imageContact);
        Glide.with(getContext()).load(contact.getAvatar()).into(imgContact);
        nom.setText(contact.getUserName());

        tagContact.setText(contact.getTag());
        listItem.findViewById(R.id.deleteButtonContact).setOnClickListener(v -> {
            ContactsApi.deleteContact(contact.getId(), response -> {
                ((AppMainActivity) getContext()).afficherListesContacts();
            }, error -> onError());
        });

        listItem.findViewById(R.id.afficherDetailsContact).setOnClickListener(v -> ((AppMainActivity) getContext()).afficherDétailsContact(contact.getId()));
        return convertView;
    }

    public void onError(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("An error has occured");
        alertDialogBuilder.setMessage("You will be redirected to the login page").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Wishurgift.getSessionVariables().setUserConnected(null);
                Wishurgift.getSessionVariables().setToken(null);
                ((AppMainActivity)getContext()).finish();
                getContext().startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        AlertDialog alertDialog =alertDialogBuilder.create();
        alertDialog.show();
    }
}
